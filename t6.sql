USE [PR-dev]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 9/13/2019 5:34:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppraisalModes]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppraisalModes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_AppraisalModes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppraisalParticipants]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppraisalParticipants](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[GeneralComment] [nvarchar](max) NULL,
	[IsMain] [bit] NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[AppraisalId] [int] NOT NULL,
	[DueDate] [datetime2](7) NULL,
	[IsInvited] [bit] NOT NULL,
 CONSTRAINT [PK_AppraisalParticipants] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Appraisals]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Appraisals](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DueDate] [datetime2](7) NOT NULL,
	[OwnerId] [uniqueidentifier] NOT NULL,
	[PerformanceReviewCycleId] [int] NOT NULL,
 CONSTRAINT [PK_Appraisals] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BusinessUnits]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusinessUnits](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Abbre] [nvarchar](max) NULL,
	[DeliveryManagerId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_BusinessUnits] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContractTypes]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_ContractTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CriteriaGroups]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CriteriaGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_CriteriaGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Criterias]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Criterias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Scale] [int] NOT NULL,
	[CriteriaGroupId] [int] NOT NULL,
 CONSTRAINT [PK_Criterias] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CriteriaWeights]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CriteriaWeights](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Weight] [decimal](18, 2) NOT NULL,
	[LevelCriteriaGroupId] [int] NOT NULL,
	[CriteriaId] [int] NOT NULL,
 CONSTRAINT [PK_CriteriaWeights] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cycles]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cycles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Date] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Cycles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeProjects]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeProjects](
	[ProjectId] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[JoiningDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_EmployeeProjects] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[ProjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Goals]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Goals](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[NextGoals] [nvarchar](max) NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[LastGoals] [nvarchar](max) NULL,
 CONSTRAINT [PK_Goals] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LevelCriteriaGroups]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LevelCriteriaGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Weight] [decimal](18, 2) NOT NULL,
	[LevelId] [int] NOT NULL,
	[CriteriaGroupId] [int] NOT NULL,
 CONSTRAINT [PK_LevelCriteriaGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Levels]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Levels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Abbre] [nvarchar](max) NULL,
 CONSTRAINT [PK_Levels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookups]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lookups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Objectives]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Objectives](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[NextObjectives] [nvarchar](max) NULL,
	[Plan] [nvarchar](max) NULL,
	[TlComment] [nvarchar](max) NULL,
	[PmComment] [nvarchar](max) NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[LastObjectives] [nvarchar](max) NULL,
 CONSTRAINT [PK_Objectives] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PerformanceReviewCycles]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PerformanceReviewCycles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Year] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[CycleId] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[AppraisalModeId] [int] NOT NULL,
 CONSTRAINT [PK_PerformanceReviewCycles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Projects]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Abbre] [nvarchar](max) NULL,
	[TeamId] [int] NOT NULL,
 CONSTRAINT [PK_Projects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ratings]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ratings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Point] [float] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[CriteriaWeightId] [int] NOT NULL,
	[AppraisalParticipantId] [int] NOT NULL,
 CONSTRAINT [PK_Ratings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReviewTimelines]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReviewTimelines](
	[AppraisalId] [int] NOT NULL,
	[StageId] [int] NOT NULL,
	[FromDate] [datetime2](7) NOT NULL,
	[ToDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_ReviewTimelines] PRIMARY KEY CLUSTERED 
(
	[AppraisalId] ASC,
	[StageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Stages]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stages](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Days] [int] NOT NULL,
 CONSTRAINT [PK_Stages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Teams]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teams](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Abbre] [nvarchar](max) NULL,
	[BusinessUnitId] [int] NOT NULL,
 CONSTRAINT [PK_Teams] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserLevels]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLevels](
	[UserId] [uniqueidentifier] NOT NULL,
	[LevelId] [int] NOT NULL,
	[PromotionDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_UserLevels] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 9/13/2019 5:34:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [uniqueidentifier] NOT NULL,
	[EmployeeId] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[FullName] [nvarchar](max) NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[isMale] [bit] NOT NULL,
	[Photo] [varbinary](max) NULL,
	[BirthDate] [datetime2](7) NOT NULL,
	[StartDate] [datetime2](7) NOT NULL,
	[ContractTypeId] [int] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190829170239_InitiateDatabase', N'3.0.0-preview6.19304.10')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190904080048_UpdateRatingDataTypeToDouble', N'3.0.0-preview6.19304.10')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190904092758_AddColumnIn_Goal_Objective', N'3.0.0-preview6.19304.10')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190904184939_UpdateDataTypeInCycleTable', N'3.0.0-preview6.19304.10')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190913044531_AddColumnIn_AppraisalParticipant', N'3.0.0-preview6.19304.10')
SET IDENTITY_INSERT [dbo].[AppraisalModes] ON 

INSERT [dbo].[AppraisalModes] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Special')
INSERT [dbo].[AppraisalModes] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Probation ')
INSERT [dbo].[AppraisalModes] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Annual Review')
SET IDENTITY_INSERT [dbo].[AppraisalModes] OFF
SET IDENTITY_INSERT [dbo].[AppraisalParticipants] ON 

INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (1, CAST(N'2019-09-12T17:13:32.1151201' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.1151206' AS DateTime2), NULL, 0, NULL, 1, 4, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 1, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (2, CAST(N'2019-09-12T17:13:32.1158838' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.1158844' AS DateTime2), NULL, 0, NULL, 1, 5, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 1, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (3, CAST(N'2019-09-12T17:13:32.1158855' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.1158856' AS DateTime2), NULL, 0, NULL, 1, 6, N'2eb36971-47e4-4ec5-bcee-21283b2388c7', 1, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (4, CAST(N'2019-09-12T17:13:32.2058606' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2058606' AS DateTime2), NULL, 0, NULL, 1, 6, N'2eb36971-47e4-4ec5-bcee-21283b2388c7', 2, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (5, CAST(N'2019-09-12T17:13:32.2058601' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2058602' AS DateTime2), NULL, 0, NULL, 1, 5, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 2, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (6, CAST(N'2019-09-12T17:13:32.2058584' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2058585' AS DateTime2), NULL, 0, NULL, 1, 4, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 2, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (7, CAST(N'2019-09-12T17:13:32.2132286' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2132287' AS DateTime2), NULL, 0, NULL, 1, 6, N'2eb36971-47e4-4ec5-bcee-21283b2388c7', 3, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (8, CAST(N'2019-09-12T17:13:32.2132275' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2132276' AS DateTime2), NULL, 0, NULL, 1, 5, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 3, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (9, CAST(N'2019-09-12T17:13:32.2132260' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2132261' AS DateTime2), NULL, 0, NULL, 1, 4, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 3, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (10, CAST(N'2019-09-12T17:13:32.2180470' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2180471' AS DateTime2), NULL, 0, NULL, 1, 6, N'2eb36971-47e4-4ec5-bcee-21283b2388c7', 4, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (11, CAST(N'2019-09-12T17:13:32.2180465' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2180466' AS DateTime2), NULL, 0, NULL, 1, 5, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 4, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (12, CAST(N'2019-09-12T17:13:32.2180449' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2180451' AS DateTime2), NULL, 0, NULL, 1, 4, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 4, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (13, CAST(N'2019-09-12T17:13:32.2216660' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2216661' AS DateTime2), NULL, 0, NULL, 1, 6, N'2eb36971-47e4-4ec5-bcee-21283b2388c7', 5, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (14, CAST(N'2019-09-12T17:13:32.2216642' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2216643' AS DateTime2), NULL, 0, NULL, 1, 5, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 5, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (15, CAST(N'2019-09-12T17:13:32.2216631' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2216632' AS DateTime2), NULL, 0, NULL, 1, 4, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 5, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (16, CAST(N'2019-09-12T17:13:32.2251713' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2251714' AS DateTime2), NULL, 0, NULL, 1, 6, N'2eb36971-47e4-4ec5-bcee-21283b2388c7', 6, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (17, CAST(N'2019-09-12T17:13:32.2251706' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2251707' AS DateTime2), NULL, 0, NULL, 1, 5, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 6, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (18, CAST(N'2019-09-12T17:13:32.2251616' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2251619' AS DateTime2), NULL, 0, NULL, 1, 4, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 6, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (19, CAST(N'2019-09-12T17:13:32.2312368' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2312369' AS DateTime2), NULL, 0, NULL, 1, 6, N'2eb36971-47e4-4ec5-bcee-21283b2388c7', 7, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (20, CAST(N'2019-09-12T17:13:32.2312364' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2312365' AS DateTime2), NULL, 0, NULL, 1, 5, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 7, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (21, CAST(N'2019-09-12T17:13:32.2312347' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2312348' AS DateTime2), NULL, 0, NULL, 1, 4, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 7, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (22, CAST(N'2019-09-12T17:13:32.2371110' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2371111' AS DateTime2), NULL, 0, NULL, 1, 6, N'2eb36971-47e4-4ec5-bcee-21283b2388c7', 8, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (23, CAST(N'2019-09-12T17:13:32.2371106' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2371107' AS DateTime2), NULL, 0, NULL, 1, 5, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 8, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (24, CAST(N'2019-09-12T17:13:32.2371091' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2371092' AS DateTime2), NULL, 0, NULL, 1, 4, N'8913e9b2-0238-4ea3-73b5-08d737675a35', 8, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (25, CAST(N'2019-09-12T17:13:32.1158855' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.1158856' AS DateTime2), NULL, 0, NULL, 1, 7, N'ed4b51b0-a6ff-4647-7395-08d737675a35', 1, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (26, CAST(N'2019-09-12T17:13:32.2058584' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2058585' AS DateTime2), NULL, 0, NULL, 1, 4, N'47ad2f82-4abc-4e6e-73a9-08d737675a35', 2, NULL, 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (27, CAST(N'2019-09-12T17:13:32.2132260' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2132261' AS DateTime2), NULL, 0, NULL, 1, 4, N'4169389d-9ca4-46a2-73bf-08d737675a35', 3, NULL, 0)
SET IDENTITY_INSERT [dbo].[AppraisalParticipants] OFF
SET IDENTITY_INSERT [dbo].[Appraisals] ON 

INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (1, CAST(N'2019-09-12T17:13:32.1144135' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.1144141' AS DateTime2), NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'ed4b51b0-a6ff-4647-7395-08d737675a35', 2)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (2, CAST(N'2019-09-12T17:13:32.2058565' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2058567' AS DateTime2), NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'47ad2f82-4abc-4e6e-73a9-08d737675a35', 2)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (3, CAST(N'2019-09-12T17:13:32.2132241' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2132243' AS DateTime2), NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'4169389d-9ca4-46a2-73bf-08d737675a35', 2)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (4, CAST(N'2019-09-12T17:13:32.2180431' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2180433' AS DateTime2), NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'd7f9fba1-dd70-4c8f-73c2-08d737675a35', 2)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (5, CAST(N'2019-09-12T17:13:32.2216619' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2216621' AS DateTime2), NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'b2e046f4-ad06-4c7d-73d5-08d737675a35', 2)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (6, CAST(N'2019-09-12T17:13:32.2251562' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2251563' AS DateTime2), NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'e93afdee-9104-49b1-73d8-08d737675a35', 2)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (7, CAST(N'2019-09-12T17:13:32.2312327' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2312329' AS DateTime2), NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'6bd1c6c7-e4f8-43d7-73df-08d737675a35', 2)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (8, CAST(N'2019-09-12T17:13:32.2371067' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-12T17:13:32.2371069' AS DateTime2), NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'06657f8c-7ac7-4b74-73e0-08d737675a35', 2)
SET IDENTITY_INSERT [dbo].[Appraisals] OFF
SET IDENTITY_INSERT [dbo].[BusinessUnits] ON 

INSERT [dbo].[BusinessUnits] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [DeliveryManagerId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Supporting', N'SUP', N'00000000-0000-0000-0000-000000000000')
INSERT [dbo].[BusinessUnits] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [DeliveryManagerId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Operational Development', N'OD', N'2ebff174-b6aa-4d4f-73c6-08d737675a35')
INSERT [dbo].[BusinessUnits] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [DeliveryManagerId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Logistics', N'LOG', N'8913e9b2-0238-4ea3-73b5-08d737675a35')
INSERT [dbo].[BusinessUnits] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [DeliveryManagerId]) VALUES (4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'eCommerce', N'eCom', N'343dbb1d-db6a-4e6d-73aa-08d737675a35')
SET IDENTITY_INSERT [dbo].[BusinessUnits] OFF
SET IDENTITY_INSERT [dbo].[ContractTypes] ON 

INSERT [dbo].[ContractTypes] ([Id], [Name]) VALUES (1, N'OnGoing')
SET IDENTITY_INSERT [dbo].[ContractTypes] OFF
SET IDENTITY_INSERT [dbo].[CriteriaGroups] ON 

INSERT [dbo].[CriteriaGroups] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Management Criteria')
INSERT [dbo].[CriteriaGroups] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Performance Criteria')
SET IDENTITY_INSERT [dbo].[CriteriaGroups] OFF
SET IDENTITY_INSERT [dbo].[Criterias] ON 

INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Staff Development', N'- Ability to recognize subordinates'' strengths and weaknesses
- Capability and willingness to help staffs overcome their weaknesses
- Ability to instruct and guide subordinates to further develop their strong points.', 5, 1)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Leadership/Management skills', N'- Exhibit self-confidence
- Inspire trust and respect from others
- Ability to motivate people to perform well
- React under critical situations
- Ability to make decisions
- Ability to control and coordinate
- Ability to resolve conflicts
- Ability to delegate (assign) appropriate tasks to members', 5, 1)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Responsibility/Accountability', N'- Ability to receive workload and respond to problems
- Ability to complete tasks on time meet the deadlines with good output
- Ability to support others complete their tasks on time meet the deadlines with good output
- Go to work on time, Go to meeting and other activities on time
- Participate in company activities', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Decision making and problem solving', N'- Ability to adapt to unexpected changes
- Capable of foreseeing potential problems / issues
- Ability to give solutions to dev in task development and responsible for accuracy.
- Ability to make decision on most of tasks / defects in reviewing.
- Ask / Confirm with ATL for complicated technical issues to make final decision', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Decision making', N'- Ability to identify the problem
- Ask/Confirm with ATL for complicated technical issues to make final decision
- Ability to analysis and select the best solution on most of tasks/defects in reviewing.', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Problem solving', N'- Ability to adapt to unexpected changes/Ability to handle unexpected changes as well as complex situations
- Capable of foreseeing potential problems / issues
- Ability to give solutions to dev in task development and responsible for accuracy.', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (7, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Teamworks', N'- Ability to cooperate with other team members
- Ability to reach an outcome of mutually acceptable compromise
- Frequency of causing conflict in the team
- Ability to actively participate in team meetings, brain storming and review sessionseview sessions', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Time management and planning skills', N'- Ability to prioritize assigned tasks
- Ability to handle multi-tasking
- Willingness to work late or over weekend in order to complete the urgent tasks
- Ability to take over the task rather than development: testing, maintenance, bug fixing
- Ability to actively report status or raise issue on the assignment', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (9, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'English level', N'Refer to skill matrix for each level', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (10, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Communication skills', N'- Ability to expresses thoughts effectively in individual and group situations
- Ability to effectively transfer knowledge and know-how to others
- Documentation skill
- Ability to express ideas in writing / email', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (11, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Technical skills', N'- Level of domain knowledge of specific team/as per specific customer
- Ability to quickly learn, adjust and adapt to change technical request in project
- Technology Watch: ability to  stay current with new technologies, methods, and processes in software development industry?', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (12, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Job knowledge', N'- Ability to know how to use correct method and procedure to fulfill assigned task
- Ability to understand client''s request and requirement
- Ability to understand client''s bug report
- Abitity to actively propose feedback or solution on customer''s requirement / request', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (13, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Quality Of Work', N'- Productivity (Volume of work load vs. Time needed to finish task)
- Ablity to handle all tasks of a project with good quality and meet the deadlines
- Ability to get the job done in expected quality with no or little supervisory
- Smoothly follow the processes', 5, 2)
SET IDENTITY_INSERT [dbo].[Criterias] OFF
SET IDENTITY_INSERT [dbo].[CriteriaWeights] ON 

INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (1, CAST(0.10 AS Decimal(18, 2)), 5, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (2, CAST(0.10 AS Decimal(18, 2)), 34, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (3, CAST(0.10 AS Decimal(18, 2)), 34, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (4, CAST(0.20 AS Decimal(18, 2)), 34, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (5, CAST(0.10 AS Decimal(18, 2)), 34, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (6, CAST(0.50 AS Decimal(18, 2)), 33, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (7, CAST(0.25 AS Decimal(18, 2)), 32, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (8, CAST(0.10 AS Decimal(18, 2)), 32, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (9, CAST(0.15 AS Decimal(18, 2)), 32, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (10, CAST(0.05 AS Decimal(18, 2)), 32, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (11, CAST(0.05 AS Decimal(18, 2)), 32, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (12, CAST(0.05 AS Decimal(18, 2)), 32, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (13, CAST(0.10 AS Decimal(18, 2)), 32, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (14, CAST(0.05 AS Decimal(18, 2)), 32, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (15, CAST(0.05 AS Decimal(18, 2)), 32, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (16, CAST(0.15 AS Decimal(18, 2)), 32, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (17, CAST(0.20 AS Decimal(18, 2)), 31, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (18, CAST(0.10 AS Decimal(18, 2)), 31, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (19, CAST(0.10 AS Decimal(18, 2)), 31, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (20, CAST(0.10 AS Decimal(18, 2)), 31, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (21, CAST(0.10 AS Decimal(18, 2)), 31, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (22, CAST(0.10 AS Decimal(18, 2)), 31, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (23, CAST(0.10 AS Decimal(18, 2)), 31, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (24, CAST(0.05 AS Decimal(18, 2)), 31, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (25, CAST(0.05 AS Decimal(18, 2)), 31, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (26, CAST(0.10 AS Decimal(18, 2)), 31, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (27, CAST(0.20 AS Decimal(18, 2)), 38, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (28, CAST(0.10 AS Decimal(18, 2)), 38, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (29, CAST(0.10 AS Decimal(18, 2)), 34, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (30, CAST(0.10 AS Decimal(18, 2)), 38, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (31, CAST(0.10 AS Decimal(18, 2)), 34, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (32, CAST(0.15 AS Decimal(18, 2)), 34, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (33, CAST(0.15 AS Decimal(18, 2)), 9, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (34, CAST(0.15 AS Decimal(18, 2)), 20, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (35, CAST(0.15 AS Decimal(18, 2)), 20, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (36, CAST(0.10 AS Decimal(18, 2)), 20, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (37, CAST(0.10 AS Decimal(18, 2)), 20, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (38, CAST(0.15 AS Decimal(18, 2)), 20, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (39, CAST(0.10 AS Decimal(18, 2)), 20, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (40, CAST(0.15 AS Decimal(18, 2)), 20, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (41, CAST(0.10 AS Decimal(18, 2)), 20, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (42, CAST(0.15 AS Decimal(18, 2)), 21, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (43, CAST(0.15 AS Decimal(18, 2)), 21, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (44, CAST(0.10 AS Decimal(18, 2)), 21, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (45, CAST(0.10 AS Decimal(18, 2)), 21, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (46, CAST(0.15 AS Decimal(18, 2)), 21, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (47, CAST(0.10 AS Decimal(18, 2)), 21, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (48, CAST(0.15 AS Decimal(18, 2)), 21, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (49, CAST(0.10 AS Decimal(18, 2)), 21, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (50, CAST(0.50 AS Decimal(18, 2)), 37, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (51, CAST(0.15 AS Decimal(18, 2)), 36, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (52, CAST(0.15 AS Decimal(18, 2)), 36, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (53, CAST(0.10 AS Decimal(18, 2)), 36, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (54, CAST(0.10 AS Decimal(18, 2)), 36, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (55, CAST(0.10 AS Decimal(18, 2)), 36, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (56, CAST(0.10 AS Decimal(18, 2)), 36, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (57, CAST(0.20 AS Decimal(18, 2)), 36, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (58, CAST(0.10 AS Decimal(18, 2)), 36, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (59, CAST(0.50 AS Decimal(18, 2)), 35, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (60, CAST(0.15 AS Decimal(18, 2)), 34, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (61, CAST(0.10 AS Decimal(18, 2)), 9, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (62, CAST(0.10 AS Decimal(18, 2)), 38, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (63, CAST(0.10 AS Decimal(18, 2)), 38, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (64, CAST(0.10 AS Decimal(18, 2)), 25, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (65, CAST(0.15 AS Decimal(18, 2)), 25, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (66, CAST(0.05 AS Decimal(18, 2)), 25, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (67, CAST(0.15 AS Decimal(18, 2)), 25, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (68, CAST(0.20 AS Decimal(18, 2)), 24, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (69, CAST(0.10 AS Decimal(18, 2)), 24, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (70, CAST(0.15 AS Decimal(18, 2)), 24, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (71, CAST(0.10 AS Decimal(18, 2)), 24, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (72, CAST(0.10 AS Decimal(18, 2)), 24, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (73, CAST(0.10 AS Decimal(18, 2)), 24, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (74, CAST(0.10 AS Decimal(18, 2)), 24, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (75, CAST(0.15 AS Decimal(18, 2)), 24, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (76, CAST(0.15 AS Decimal(18, 2)), 23, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (77, CAST(0.15 AS Decimal(18, 2)), 23, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (78, CAST(0.10 AS Decimal(18, 2)), 23, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (79, CAST(0.10 AS Decimal(18, 2)), 23, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (80, CAST(0.10 AS Decimal(18, 2)), 23, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (81, CAST(0.10 AS Decimal(18, 2)), 23, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (82, CAST(0.15 AS Decimal(18, 2)), 23, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (83, CAST(0.15 AS Decimal(18, 2)), 23, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (84, CAST(0.15 AS Decimal(18, 2)), 22, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (85, CAST(0.15 AS Decimal(18, 2)), 22, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (86, CAST(0.10 AS Decimal(18, 2)), 22, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (87, CAST(0.10 AS Decimal(18, 2)), 22, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (88, CAST(0.10 AS Decimal(18, 2)), 22, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (89, CAST(0.10 AS Decimal(18, 2)), 22, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (90, CAST(0.20 AS Decimal(18, 2)), 22, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (91, CAST(0.10 AS Decimal(18, 2)), 25, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (92, CAST(0.10 AS Decimal(18, 2)), 38, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (93, CAST(0.10 AS Decimal(18, 2)), 25, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (94, CAST(0.25 AS Decimal(18, 2)), 25, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (95, CAST(0.15 AS Decimal(18, 2)), 38, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (96, CAST(0.15 AS Decimal(18, 2)), 38, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (97, CAST(0.15 AS Decimal(18, 2)), 30, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (98, CAST(0.15 AS Decimal(18, 2)), 30, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (99, CAST(0.15 AS Decimal(18, 2)), 30, 10)
GO
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (100, CAST(0.10 AS Decimal(18, 2)), 30, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (101, CAST(0.10 AS Decimal(18, 2)), 30, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (102, CAST(0.10 AS Decimal(18, 2)), 30, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (103, CAST(0.10 AS Decimal(18, 2)), 30, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (104, CAST(0.15 AS Decimal(18, 2)), 30, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (105, CAST(0.15 AS Decimal(18, 2)), 28, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (106, CAST(0.15 AS Decimal(18, 2)), 28, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (107, CAST(0.10 AS Decimal(18, 2)), 28, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (108, CAST(0.10 AS Decimal(18, 2)), 28, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (109, CAST(0.10 AS Decimal(18, 2)), 28, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (110, CAST(0.10 AS Decimal(18, 2)), 28, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (111, CAST(0.20 AS Decimal(18, 2)), 28, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (112, CAST(0.10 AS Decimal(18, 2)), 28, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (113, CAST(0.40 AS Decimal(18, 2)), 27, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (114, CAST(0.20 AS Decimal(18, 2)), 26, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (115, CAST(0.10 AS Decimal(18, 2)), 26, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (116, CAST(0.10 AS Decimal(18, 2)), 26, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (117, CAST(0.10 AS Decimal(18, 2)), 26, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (118, CAST(0.10 AS Decimal(18, 2)), 26, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (119, CAST(0.10 AS Decimal(18, 2)), 26, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (120, CAST(0.15 AS Decimal(18, 2)), 26, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (121, CAST(0.15 AS Decimal(18, 2)), 26, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (122, CAST(0.10 AS Decimal(18, 2)), 25, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (123, CAST(0.10 AS Decimal(18, 2)), 22, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (124, CAST(0.10 AS Decimal(18, 2)), 9, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (125, CAST(0.10 AS Decimal(18, 2)), 9, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (126, CAST(0.10 AS Decimal(18, 2)), 18, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (127, CAST(0.15 AS Decimal(18, 2)), 18, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (128, CAST(0.10 AS Decimal(18, 2)), 18, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (129, CAST(0.10 AS Decimal(18, 2)), 18, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (130, CAST(0.10 AS Decimal(18, 2)), 18, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (131, CAST(0.10 AS Decimal(18, 2)), 18, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (132, CAST(0.10 AS Decimal(18, 2)), 18, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (133, CAST(0.10 AS Decimal(18, 2)), 18, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (134, CAST(0.15 AS Decimal(18, 2)), 18, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (135, CAST(0.40 AS Decimal(18, 2)), 11, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (136, CAST(0.10 AS Decimal(18, 2)), 12, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (137, CAST(0.15 AS Decimal(18, 2)), 12, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (138, CAST(0.10 AS Decimal(18, 2)), 12, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (139, CAST(0.10 AS Decimal(18, 2)), 12, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (140, CAST(0.10 AS Decimal(18, 2)), 12, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (141, CAST(0.10 AS Decimal(18, 2)), 12, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (142, CAST(0.10 AS Decimal(18, 2)), 12, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (143, CAST(0.10 AS Decimal(18, 2)), 12, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (144, CAST(0.15 AS Decimal(18, 2)), 12, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (145, CAST(0.40 AS Decimal(18, 2)), 13, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (146, CAST(0.10 AS Decimal(18, 2)), 14, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (147, CAST(0.15 AS Decimal(18, 2)), 14, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (148, CAST(0.10 AS Decimal(18, 2)), 14, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (149, CAST(0.10 AS Decimal(18, 2)), 14, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (150, CAST(0.10 AS Decimal(18, 2)), 14, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (151, CAST(0.10 AS Decimal(18, 2)), 14, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (152, CAST(0.10 AS Decimal(18, 2)), 14, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (153, CAST(0.40 AS Decimal(18, 2)), 10, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (154, CAST(0.10 AS Decimal(18, 2)), 14, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (155, CAST(0.25 AS Decimal(18, 2)), 8, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (156, CAST(0.10 AS Decimal(18, 2)), 8, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (157, CAST(0.10 AS Decimal(18, 2)), 5, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (158, CAST(0.10 AS Decimal(18, 2)), 6, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (159, CAST(0.10 AS Decimal(18, 2)), 6, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (160, CAST(0.05 AS Decimal(18, 2)), 6, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (161, CAST(0.10 AS Decimal(18, 2)), 6, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (162, CAST(0.10 AS Decimal(18, 2)), 6, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (163, CAST(0.10 AS Decimal(18, 2)), 6, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (164, CAST(0.10 AS Decimal(18, 2)), 6, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (165, CAST(0.10 AS Decimal(18, 2)), 6, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (166, CAST(0.10 AS Decimal(18, 2)), 6, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (167, CAST(0.15 AS Decimal(18, 2)), 6, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (168, CAST(0.15 AS Decimal(18, 2)), 7, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (169, CAST(0.05 AS Decimal(18, 2)), 7, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (170, CAST(0.05 AS Decimal(18, 2)), 7, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (171, CAST(0.10 AS Decimal(18, 2)), 7, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (172, CAST(0.05 AS Decimal(18, 2)), 7, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (173, CAST(0.05 AS Decimal(18, 2)), 7, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (174, CAST(0.05 AS Decimal(18, 2)), 7, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (175, CAST(0.15 AS Decimal(18, 2)), 7, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (176, CAST(0.10 AS Decimal(18, 2)), 7, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (177, CAST(0.25 AS Decimal(18, 2)), 7, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (178, CAST(0.15 AS Decimal(18, 2)), 8, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (179, CAST(0.05 AS Decimal(18, 2)), 8, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (180, CAST(0.15 AS Decimal(18, 2)), 8, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (181, CAST(0.05 AS Decimal(18, 2)), 8, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (182, CAST(0.05 AS Decimal(18, 2)), 8, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (183, CAST(0.10 AS Decimal(18, 2)), 8, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (184, CAST(0.10 AS Decimal(18, 2)), 8, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (185, CAST(0.10 AS Decimal(18, 2)), 9, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (186, CAST(0.15 AS Decimal(18, 2)), 14, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (187, CAST(0.15 AS Decimal(18, 2)), 16, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (188, CAST(0.05 AS Decimal(18, 2)), 39, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (189, CAST(0.05 AS Decimal(18, 2)), 39, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (190, CAST(0.10 AS Decimal(18, 2)), 39, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (191, CAST(0.10 AS Decimal(18, 2)), 39, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (192, CAST(0.10 AS Decimal(18, 2)), 39, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (193, CAST(0.25 AS Decimal(18, 2)), 39, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (194, CAST(0.10 AS Decimal(18, 2)), 5, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (195, CAST(0.10 AS Decimal(18, 2)), 5, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (196, CAST(0.10 AS Decimal(18, 2)), 5, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (197, CAST(0.10 AS Decimal(18, 2)), 5, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (198, CAST(0.10 AS Decimal(18, 2)), 5, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (199, CAST(0.15 AS Decimal(18, 2)), 5, 4)
GO
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (200, CAST(0.15 AS Decimal(18, 2)), 5, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (201, CAST(0.40 AS Decimal(18, 2)), 4, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (202, CAST(0.10 AS Decimal(18, 2)), 3, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (203, CAST(0.10 AS Decimal(18, 2)), 3, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (204, CAST(0.10 AS Decimal(18, 2)), 3, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (205, CAST(0.10 AS Decimal(18, 2)), 3, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (206, CAST(0.10 AS Decimal(18, 2)), 3, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (207, CAST(0.10 AS Decimal(18, 2)), 3, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (208, CAST(0.10 AS Decimal(18, 2)), 3, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (209, CAST(0.15 AS Decimal(18, 2)), 3, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (210, CAST(0.15 AS Decimal(18, 2)), 3, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (211, CAST(0.40 AS Decimal(18, 2)), 2, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (212, CAST(0.15 AS Decimal(18, 2)), 9, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (213, CAST(0.15 AS Decimal(18, 2)), 9, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (214, CAST(0.15 AS Decimal(18, 2)), 9, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (215, CAST(0.15 AS Decimal(18, 2)), 39, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (216, CAST(0.40 AS Decimal(18, 2)), 15, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (217, CAST(0.05 AS Decimal(18, 2)), 39, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (218, CAST(0.25 AS Decimal(18, 2)), 19, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (219, CAST(0.15 AS Decimal(18, 2)), 16, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (220, CAST(0.10 AS Decimal(18, 2)), 16, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (221, CAST(0.10 AS Decimal(18, 2)), 16, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (222, CAST(0.10 AS Decimal(18, 2)), 16, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (223, CAST(0.10 AS Decimal(18, 2)), 16, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (224, CAST(0.10 AS Decimal(18, 2)), 16, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (225, CAST(0.10 AS Decimal(18, 2)), 16, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (226, CAST(0.10 AS Decimal(18, 2)), 16, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (227, CAST(0.10 AS Decimal(18, 2)), 17, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (228, CAST(0.10 AS Decimal(18, 2)), 17, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (229, CAST(0.05 AS Decimal(18, 2)), 17, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (230, CAST(0.10 AS Decimal(18, 2)), 17, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (231, CAST(0.10 AS Decimal(18, 2)), 17, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (232, CAST(0.10 AS Decimal(18, 2)), 17, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (233, CAST(0.10 AS Decimal(18, 2)), 17, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (234, CAST(0.10 AS Decimal(18, 2)), 17, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (235, CAST(0.10 AS Decimal(18, 2)), 17, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (236, CAST(0.15 AS Decimal(18, 2)), 17, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (237, CAST(0.15 AS Decimal(18, 2)), 19, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (238, CAST(0.05 AS Decimal(18, 2)), 19, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (239, CAST(0.05 AS Decimal(18, 2)), 19, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (240, CAST(0.10 AS Decimal(18, 2)), 19, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (241, CAST(0.05 AS Decimal(18, 2)), 19, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (242, CAST(0.05 AS Decimal(18, 2)), 19, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (243, CAST(0.05 AS Decimal(18, 2)), 19, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (244, CAST(0.15 AS Decimal(18, 2)), 19, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (245, CAST(0.10 AS Decimal(18, 2)), 19, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (246, CAST(0.15 AS Decimal(18, 2)), 39, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (247, CAST(0.40 AS Decimal(18, 2)), 1, 2)
SET IDENTITY_INSERT [dbo].[CriteriaWeights] OFF
SET IDENTITY_INSERT [dbo].[Cycles] ON 

INSERT [dbo].[Cycles] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Date], [Month], [Description]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'H2', 1, 7, NULL)
INSERT [dbo].[Cycles] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Date], [Month], [Description]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'H1', 7, 1, NULL)
SET IDENTITY_INSERT [dbo].[Cycles] OFF
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'91502f09-e02e-43dd-738d-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'da548f23-ee8b-4128-738e-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'b7c39efc-675f-494f-738f-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'f3f32d44-33e0-471a-7390-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'80a6001b-be1b-4209-7391-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'a03ab6a1-b528-4083-7392-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'ed300fe5-d016-47fd-7393-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'b9194558-66aa-4307-7394-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'ed4b51b0-a6ff-4647-7395-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'fdd1e876-20d0-42e3-7396-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'32712441-cf5b-4d88-7397-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'd7525d1f-3c95-4bcf-7398-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'592a4bb0-11da-49da-7399-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'97157224-c78d-4f97-739a-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'f15c9549-b2a4-49d2-739b-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'dcee4b20-505b-4da8-739c-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'0a26435c-1881-4840-739d-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'4dce4b22-34dc-4149-739e-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'3931623f-f03d-481e-739f-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'92d11cf5-8edb-41f5-73a0-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'dc75fae2-7ec6-494c-73a1-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'8ab14c85-80d2-4759-73a2-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'22baa715-43fa-429f-73a3-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'c497f964-6c82-4259-73a4-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'96f66a47-f879-4939-73a5-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'ef3799fa-3e78-4ef5-73a6-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'b1e99f7f-96e2-4a34-73a7-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'62babbf9-343a-47c9-73a8-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'47ad2f82-4abc-4e6e-73a9-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'343dbb1d-db6a-4e6d-73aa-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'a274c058-befa-49cb-73ab-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'd63240a9-6d45-4c69-73ac-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'49790a0b-4f67-45f6-73ad-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'2bc446ed-277c-43dc-73ae-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'33fd95b5-0612-45da-73af-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'b92f221d-89bc-4dc1-73b0-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'0df351be-417c-4b6b-73b1-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'bf6ccbd5-2132-4c28-73b2-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'f645fa18-9b70-4395-73b3-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'905c1d00-8664-4e6b-73b4-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'8913e9b2-0238-4ea3-73b5-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'444ef3b1-200e-4650-73b6-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'0f30ec33-8684-48b7-73b7-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'8da98199-08f7-418b-73b8-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'570cd560-9252-45a3-73b9-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'9bfc46bb-2484-4c4d-73ba-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'47cdb882-5848-48f9-73bb-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'da50cceb-cb04-4bf8-73bc-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'53a04f77-a6ab-44f0-73bd-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'59873592-554e-48eb-73be-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'4169389d-9ca4-46a2-73bf-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'a0f79b16-b304-4792-73c0-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'08ed8955-211b-411e-73c1-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'd7f9fba1-dd70-4c8f-73c2-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'34b9cd56-f1eb-433f-73c3-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'c9fcc43a-a9e0-4482-73c4-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'12a90d06-6419-4a6f-73c5-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'2ebff174-b6aa-4d4f-73c6-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'6a082577-42db-44bf-73c7-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'67110b67-26af-4c53-73c8-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'bc6de099-0a84-4129-73c9-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'483b31ac-f275-49cc-73ca-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'b6f8c502-aea8-4fa7-73cb-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'7f54a997-5139-4b33-73cc-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'd183ce3d-40a9-4e98-73cd-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'e2b58443-da54-4c85-73ce-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'59b0c65a-279a-47ab-73cf-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'f43ad146-61c4-44de-73d0-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'696054f7-20aa-491c-73d1-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'200f9ce5-5313-4df2-73d2-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'75b0e9cb-261e-4b73-73d3-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'c1f6660f-9570-41a9-73d4-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'b2e046f4-ad06-4c7d-73d5-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'4af4be05-c351-427c-73d6-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'4417eb5b-e9c8-4741-73d7-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'e93afdee-9104-49b1-73d8-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'b8fa96ee-e10e-4863-73d9-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'275ce4ae-07f8-4419-73da-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'367bf9c7-fe68-40d7-73db-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'1533b70e-34db-449b-73dc-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'3a3db162-8a9e-4345-73dd-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'8b169d0a-50bd-4337-73de-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'6bd1c6c7-e4f8-43d7-73df-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'06657f8c-7ac7-4b74-73e0-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'8aeb0af7-1791-4864-73e1-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'29e83c4c-ad9d-47d0-73e2-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'9ac017a3-4ea6-4402-73e3-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'57ced7af-b5d5-45cb-73e4-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'fc645229-e309-44bb-73e5-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'328dc22a-129c-48d5-73e6-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'1e715c62-89a0-472e-73e7-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'9f145e0e-6e0f-4e9b-73e8-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'd6f8c674-3998-46fb-73e9-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'57d7b635-0b4c-4a15-73ea-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'185a29f7-7f63-4310-73eb-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'ccb3c68e-5fa1-4614-73ec-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'a0fc21cf-6b5a-4c96-73ed-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'c13d063c-5cec-47f4-73ee-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'54499bc4-503e-4a72-73ef-08d737675a35', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[LevelCriteriaGroups] ON 

INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (1, CAST(0.30 AS Decimal(18, 2)), 26, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (2, CAST(0.30 AS Decimal(18, 2)), 11, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (3, CAST(0.70 AS Decimal(18, 2)), 11, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (4, CAST(0.30 AS Decimal(18, 2)), 10, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (5, CAST(0.70 AS Decimal(18, 2)), 10, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (6, CAST(1.00 AS Decimal(18, 2)), 9, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (7, CAST(1.00 AS Decimal(18, 2)), 8, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (8, CAST(1.00 AS Decimal(18, 2)), 7, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (9, CAST(1.00 AS Decimal(18, 2)), 12, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (10, CAST(0.30 AS Decimal(18, 2)), 6, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (11, CAST(0.30 AS Decimal(18, 2)), 5, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (12, CAST(0.70 AS Decimal(18, 2)), 5, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (13, CAST(0.30 AS Decimal(18, 2)), 4, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (14, CAST(0.70 AS Decimal(18, 2)), 4, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (15, CAST(0.30 AS Decimal(18, 2)), 3, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (16, CAST(0.70 AS Decimal(18, 2)), 3, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (17, CAST(1.00 AS Decimal(18, 2)), 2, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (18, CAST(0.70 AS Decimal(18, 2)), 6, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (19, CAST(1.00 AS Decimal(18, 2)), 27, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (20, CAST(0.70 AS Decimal(18, 2)), 13, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (21, CAST(0.70 AS Decimal(18, 2)), 14, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (22, CAST(0.70 AS Decimal(18, 2)), 26, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (23, CAST(1.00 AS Decimal(18, 2)), 25, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (24, CAST(1.00 AS Decimal(18, 2)), 24, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (25, CAST(1.00 AS Decimal(18, 2)), 23, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (26, CAST(1.00 AS Decimal(18, 2)), 22, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (27, CAST(0.30 AS Decimal(18, 2)), 21, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (28, CAST(0.70 AS Decimal(18, 2)), 21, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (29, CAST(0.30 AS Decimal(18, 2)), 13, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (30, CAST(1.00 AS Decimal(18, 2)), 20, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (31, CAST(1.00 AS Decimal(18, 2)), 18, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (32, CAST(1.00 AS Decimal(18, 2)), 17, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (33, CAST(0.30 AS Decimal(18, 2)), 16, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (34, CAST(0.70 AS Decimal(18, 2)), 16, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (35, CAST(0.30 AS Decimal(18, 2)), 15, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (36, CAST(0.70 AS Decimal(18, 2)), 15, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (37, CAST(0.30 AS Decimal(18, 2)), 14, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (38, CAST(1.00 AS Decimal(18, 2)), 19, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (39, CAST(1.00 AS Decimal(18, 2)), 28, 2)
SET IDENTITY_INSERT [dbo].[LevelCriteriaGroups] OFF
SET IDENTITY_INSERT [dbo].[Levels] ON 

INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (1, N'Chief Executive Officer', N'CEO')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (2, N'Senior Software Engineer', N'SSE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (3, N'Technical Leader', N'TL')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (4, N'Technical Manager', N'TM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (5, N'Software Manager', N'SM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (6, N'Solution Architecture', N'SA')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (7, N'Associate Test Engineer', N'ATS')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (8, N'Test Engineer', N'TS')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (9, N'Senior Testing Engineer', N'STS')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (10, N'QA Lead', N'QAL')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (11, N'Test Lead', N'TSL')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (12, N'Associate Project Assistant', N'APA')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (13, N'Associate Project Manager', N'APM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (14, N'Project Manager', N'PM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (15, N'Senior Project Manager', N'SPM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (16, N'Delivery Manager', N'DM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (17, N'UX-UI designer', N'UXD')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (18, N'Business Analyst ', N'BA')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (19, N'Office Manager', N'OM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (20, N'Accountant ', N'ACC')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (21, N'Accounting Manager', N'ACM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (22, N'IT Manager', N'ITM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (23, N'Associate HR Executive', N'AHE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (24, N'HR Executive', N'HE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (25, N'Senior HR Executive', N'SHE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (26, N'HR Manager', N'HMR')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (27, N'Software Engineer', N'SE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (28, N'Associate Software Engineer', N'ASE')
SET IDENTITY_INSERT [dbo].[Levels] OFF
SET IDENTITY_INSERT [dbo].[PerformanceReviewCycles] ON 

INSERT [dbo].[PerformanceReviewCycles] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Year], [Description], [CycleId], [Status], [AppraisalModeId]) VALUES (1, CAST(N'2019-09-12T17:00:36.9629121' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, N'Annual Review H1 2020', 2020, N' Performance Review Cycle
', 2, 20, 3)
INSERT [dbo].[PerformanceReviewCycles] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Year], [Description], [CycleId], [Status], [AppraisalModeId]) VALUES (2, CAST(N'2019-09-12T17:05:18.2959965' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, N'Annual Review H2 2019', 2019, NULL, 1, 20, 3)
SET IDENTITY_INSERT [dbo].[PerformanceReviewCycles] OFF
SET IDENTITY_INSERT [dbo].[Projects] ON 

INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'General - SUP', N'PRJSUP', 1)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'General - Adj', N'PRJADJ', 2)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'General - LOG', N'PRJLOG', 3)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'General - App', N'PRJAPP', 4)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'General - Amb', N'PRJAMB', 5)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'General - F4', N'PRJF4', 6)
SET IDENTITY_INSERT [dbo].[Projects] OFF
SET IDENTITY_INSERT [dbo].[Ratings] ON 

INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (1, CAST(N'2019-09-13T15:05:17.6233333' AS DateTime2), NULL, CAST(N'2019-09-13T15:19:57.8866667' AS DateTime2), NULL, 0, 1, N'a', 168, 25)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (2, CAST(N'2019-09-13T15:05:29.0066667' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.6966667' AS DateTime2), NULL, 0, 1, N'a', 171, 25)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (3, CAST(N'2019-09-13T15:05:29.0366667' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.6966667' AS DateTime2), NULL, 0, 1, N'a', 175, 25)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (4, CAST(N'2019-09-13T15:05:29.0766667' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.6966667' AS DateTime2), NULL, 0, 2, N'a', 176, 25)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (5, CAST(N'2019-09-13T15:05:29.1200000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.6966667' AS DateTime2), NULL, 0, 2, N'a', 174, 25)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (6, CAST(N'2019-09-13T15:05:29.1500000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.6966667' AS DateTime2), NULL, 0, 1, N'a', 177, 25)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (7, CAST(N'2019-09-13T15:05:29.1666667' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.6966667' AS DateTime2), NULL, 0, 2, N'a', 168, 1)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (8, CAST(N'2019-09-13T15:05:29.1833333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.6966667' AS DateTime2), NULL, 0, 1, N'a', 169, 1)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (9, CAST(N'2019-09-13T15:05:29.2466667' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.6966667' AS DateTime2), NULL, 0, 2, N'sd', 170, 1)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (10, CAST(N'2019-09-13T15:05:29.3033333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7000000' AS DateTime2), NULL, 0, 3, N'sfd', 171, 1)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (11, CAST(N'2019-09-13T15:05:29.3900000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7100000' AS DateTime2), NULL, 0, 2, N'', 172, 1)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (12, CAST(N'2019-09-13T15:05:29.4500000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7100000' AS DateTime2), NULL, 0, 1, N'sfd', 173, 1)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (13, CAST(N'2019-09-13T15:05:29.5166667' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7133333' AS DateTime2), NULL, 0, 3, N'e', 174, 1)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (14, CAST(N'2019-09-13T15:05:29.6000000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7133333' AS DateTime2), NULL, 0, 2, N'r', 175, 1)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (15, CAST(N'2019-09-13T15:05:29.6700000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7133333' AS DateTime2), NULL, 0, 4, N'te', 176, 1)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (16, CAST(N'2019-09-13T15:05:29.7066667' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7166667' AS DateTime2), NULL, 0, 1, N'h', 177, 1)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (17, CAST(N'2019-09-13T15:05:29.7233333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7200000' AS DateTime2), NULL, 0, 4, N'', 168, 2)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (18, CAST(N'2019-09-13T15:05:29.7633333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7266667' AS DateTime2), NULL, 0, 4, N'bv', 169, 2)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (19, CAST(N'2019-09-13T15:05:29.7633333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7266667' AS DateTime2), NULL, 0, 4, N'vc', 170, 2)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (20, CAST(N'2019-09-13T15:05:29.7633333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7266667' AS DateTime2), NULL, 0, 3, N'xzx', 171, 2)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (21, CAST(N'2019-09-13T15:05:29.7633333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7266667' AS DateTime2), NULL, 0, 2, N'cxz', 172, 2)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (22, CAST(N'2019-09-13T15:05:29.7633333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7266667' AS DateTime2), NULL, 0, 1, N'', 173, 2)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (23, CAST(N'2019-09-13T15:05:29.7666667' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7266667' AS DateTime2), NULL, 0, 4, N'Z', 174, 2)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (24, CAST(N'2019-09-13T15:05:29.7700000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7266667' AS DateTime2), NULL, 0, 5, N'sa', 175, 2)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (25, CAST(N'2019-09-13T15:05:29.7733333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7266667' AS DateTime2), NULL, 0, 2, N'ds', 176, 2)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (26, CAST(N'2019-09-13T15:05:29.7733333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7266667' AS DateTime2), NULL, 0, 1, N'dfd', 177, 2)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (27, CAST(N'2019-09-13T15:05:29.7866667' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7266667' AS DateTime2), NULL, 0, 2, N'trw', 168, 3)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (28, CAST(N'2019-09-13T15:05:29.8133333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7266667' AS DateTime2), NULL, 0, 2.5, N'd', 169, 3)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (29, CAST(N'2019-09-13T15:05:29.8300000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7300000' AS DateTime2), NULL, 0, 2.5, N'cx', 170, 3)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (30, CAST(N'2019-09-13T15:05:29.8333333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7300000' AS DateTime2), NULL, 0, 0, N'vx', 171, 3)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (31, CAST(N'2019-09-13T15:05:29.8400000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7300000' AS DateTime2), NULL, 0, 2.5, N'c', 172, 3)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (32, CAST(N'2019-09-13T15:05:29.8433333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7300000' AS DateTime2), NULL, 0, 3, N'gdfgd', 173, 3)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (33, CAST(N'2019-09-13T15:05:29.8433333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7300000' AS DateTime2), NULL, 0, 4, N'w', 174, 3)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (34, CAST(N'2019-09-13T15:05:29.8500000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7300000' AS DateTime2), NULL, 0, 5, N'w', 175, 3)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (35, CAST(N'2019-09-13T15:05:29.8500000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7300000' AS DateTime2), NULL, 0, 2, N'sadsa', 176, 3)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (36, CAST(N'2019-09-13T15:05:29.8500000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.7300000' AS DateTime2), NULL, 0, 3, N'', 177, 3)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (37, CAST(N'2019-09-13T15:18:38.1800000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.6966667' AS DateTime2), NULL, 0, 5, N'sad', 172, 25)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (38, CAST(N'2019-09-13T15:18:38.1800000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:04.6966667' AS DateTime2), NULL, 0, 2, N'sad', 173, 25)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (39, CAST(N'2019-09-13T15:18:38.1800000' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:02.7466667' AS DateTime2), NULL, 0, 1, N'sa', 170, 25)
INSERT [dbo].[Ratings] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Point], [Comment], [CriteriaWeightId], [AppraisalParticipantId]) VALUES (40, CAST(N'2019-09-13T15:18:38.1833333' AS DateTime2), NULL, CAST(N'2019-09-13T15:20:01.0633333' AS DateTime2), NULL, 0, 1, N'dsa', 169, 25)
SET IDENTITY_INSERT [dbo].[Ratings] OFF
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (1, 1, CAST(N'2019-09-10T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-12T09:24:59.6530000' AS DateTime2), 1)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (1, 2, CAST(N'2019-09-12T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-15T09:24:59.6530000' AS DateTime2), 1)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (1, 3, CAST(N'2019-09-15T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-16T09:24:59.6530000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (1, 4, CAST(N'2019-09-16T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-17T09:24:59.6530000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (1, 5, CAST(N'2019-09-17T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-18T09:24:59.6530000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (1, 6, CAST(N'2019-09-18T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-19T09:24:59.6530000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (1, 7, CAST(N'2019-09-19T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-20T09:24:59.6530000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (1, 8, CAST(N'2019-09-20T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-21T09:24:59.6530000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (2, 1, CAST(N'2019-09-13T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-14T09:24:59.6530000' AS DateTime2), 1)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (2, 2, CAST(N'2019-09-14T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-15T09:24:59.6530000' AS DateTime2), 1)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (2, 3, CAST(N'2019-09-15T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-16T09:24:59.6530000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (2, 4, CAST(N'2019-09-16T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-17T09:24:59.6530000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (2, 5, CAST(N'2019-09-17T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-18T09:24:59.6530000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (2, 6, CAST(N'2019-09-18T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-19T09:24:59.6530000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (2, 7, CAST(N'2019-09-19T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-20T09:24:59.6530000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (2, 8, CAST(N'2019-09-20T09:24:59.6530000' AS DateTime2), CAST(N'2019-09-21T09:24:59.6530000' AS DateTime2), 0)
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([Id], [Name]) VALUES (1, N'System Admin')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (2, N'HR')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (3, N'CEO')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (4, N'DM')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (5, N'PM')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (6, N'TL')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (7, N'Employee')
SET IDENTITY_INSERT [dbo].[Roles] OFF
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (1, N'Self rating', NULL, 3)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (2, N'TL review', NULL, 6)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (3, N'PM review', NULL, 6)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (4, N'DM review ', NULL, 3)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (5, N'HR consolidate', NULL, 7)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (6, N'CEO', NULL, 3)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (7, N'Sharing', NULL, 10)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (8, N'Employee Sign-off', NULL, 5)
SET IDENTITY_INSERT [dbo].[Teams] ON 

INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Supporting', N'SUP', 1)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Adjuno', N'ADJ', 2)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Logistics', N'LOG', 3)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'AppDev', N'APP', 4)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Amblique', N'AMB', 4)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'FactFour', N'F4', 4)
SET IDENTITY_INSERT [dbo].[Teams] OFF
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'fe0cf8b6-052e-475d-738c-08d737675a35', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'91502f09-e02e-43dd-738d-08d737675a35', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'da548f23-ee8b-4128-738e-08d737675a35', 4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b7c39efc-675f-494f-738f-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'f3f32d44-33e0-471a-7390-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'80a6001b-be1b-4209-7391-08d737675a35', 9, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'a03ab6a1-b528-4083-7392-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'ed300fe5-d016-47fd-7393-08d737675a35', 9, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b9194558-66aa-4307-7394-08d737675a35', 13, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'ed4b51b0-a6ff-4647-7395-08d737675a35', 9, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'fdd1e876-20d0-42e3-7396-08d737675a35', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'32712441-cf5b-4d88-7397-08d737675a35', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd7525d1f-3c95-4bcf-7398-08d737675a35', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'592a4bb0-11da-49da-7399-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'97157224-c78d-4f97-739a-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'f15c9549-b2a4-49d2-739b-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'dcee4b20-505b-4da8-739c-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'0a26435c-1881-4840-739d-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'4dce4b22-34dc-4149-739e-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'3931623f-f03d-481e-739f-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'92d11cf5-8edb-41f5-73a0-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'dc75fae2-7ec6-494c-73a1-08d737675a35', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'8ab14c85-80d2-4759-73a2-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'22baa715-43fa-429f-73a3-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c497f964-6c82-4259-73a4-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'96f66a47-f879-4939-73a5-08d737675a35', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'ef3799fa-3e78-4ef5-73a6-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b1e99f7f-96e2-4a34-73a7-08d737675a35', 4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'62babbf9-343a-47c9-73a8-08d737675a35', 10, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'47ad2f82-4abc-4e6e-73a9-08d737675a35', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'343dbb1d-db6a-4e6d-73aa-08d737675a35', 16, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'a274c058-befa-49cb-73ab-08d737675a35', 19, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd63240a9-6d45-4c69-73ac-08d737675a35', 26, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'49790a0b-4f67-45f6-73ad-08d737675a35', 21, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2bc446ed-277c-43dc-73ae-08d737675a35', 22, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'33fd95b5-0612-45da-73af-08d737675a35', 25, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b92f221d-89bc-4dc1-73b0-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'0df351be-417c-4b6b-73b1-08d737675a35', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'bf6ccbd5-2132-4c28-73b2-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'f645fa18-9b70-4395-73b3-08d737675a35', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'905c1d00-8664-4e6b-73b4-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'8913e9b2-0238-4ea3-73b5-08d737675a35', 16, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'444ef3b1-200e-4650-73b6-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'0f30ec33-8684-48b7-73b7-08d737675a35', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'8da98199-08f7-418b-73b8-08d737675a35', 5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'570cd560-9252-45a3-73b9-08d737675a35', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'9bfc46bb-2484-4c4d-73ba-08d737675a35', 11, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'47cdb882-5848-48f9-73bb-08d737675a35', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'da50cceb-cb04-4bf8-73bc-08d737675a35', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'53a04f77-a6ab-44f0-73bd-08d737675a35', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'59873592-554e-48eb-73be-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'4169389d-9ca4-46a2-73bf-08d737675a35', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'a0f79b16-b304-4792-73c0-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'08ed8955-211b-411e-73c1-08d737675a35', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd7f9fba1-dd70-4c8f-73c2-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'34b9cd56-f1eb-433f-73c3-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c9fcc43a-a9e0-4482-73c4-08d737675a35', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'12a90d06-6419-4a6f-73c5-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2ebff174-b6aa-4d4f-73c6-08d737675a35', 16, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'6a082577-42db-44bf-73c7-08d737675a35', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'67110b67-26af-4c53-73c8-08d737675a35', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'bc6de099-0a84-4129-73c9-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'483b31ac-f275-49cc-73ca-08d737675a35', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b6f8c502-aea8-4fa7-73cb-08d737675a35', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'7f54a997-5139-4b33-73cc-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd183ce3d-40a9-4e98-73cd-08d737675a35', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'e2b58443-da54-4c85-73ce-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'59b0c65a-279a-47ab-73cf-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'f43ad146-61c4-44de-73d0-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'696054f7-20aa-491c-73d1-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'200f9ce5-5313-4df2-73d2-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'75b0e9cb-261e-4b73-73d3-08d737675a35', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c1f6660f-9570-41a9-73d4-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b2e046f4-ad06-4c7d-73d5-08d737675a35', 17, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'4af4be05-c351-427c-73d6-08d737675a35', 20, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'4417eb5b-e9c8-4741-73d7-08d737675a35', 12, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'e93afdee-9104-49b1-73d8-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b8fa96ee-e10e-4863-73d9-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'275ce4ae-07f8-4419-73da-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'367bf9c7-fe68-40d7-73db-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'1533b70e-34db-449b-73dc-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'3a3db162-8a9e-4345-73dd-08d737675a35', 23, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'8b169d0a-50bd-4337-73de-08d737675a35', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'6bd1c6c7-e4f8-43d7-73df-08d737675a35', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'06657f8c-7ac7-4b74-73e0-08d737675a35', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'8aeb0af7-1791-4864-73e1-08d737675a35', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'29e83c4c-ad9d-47d0-73e2-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'9ac017a3-4ea6-4402-73e3-08d737675a35', 18, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'57ced7af-b5d5-45cb-73e4-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'fc645229-e309-44bb-73e5-08d737675a35', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'328dc22a-129c-48d5-73e6-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'1e715c62-89a0-472e-73e7-08d737675a35', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'9f145e0e-6e0f-4e9b-73e8-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd6f8c674-3998-46fb-73e9-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'57d7b635-0b4c-4a15-73ea-08d737675a35', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'185a29f7-7f63-4310-73eb-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'ccb3c68e-5fa1-4614-73ec-08d737675a35', 24, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'a0fc21cf-6b5a-4c96-73ed-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c13d063c-5cec-47f4-73ee-08d737675a35', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'54499bc4-503e-4a72-73ef-08d737675a35', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2eb36971-47e4-4ec5-bcee-21283b2388c7', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'fe0cf8b6-052e-475d-738c-08d737675a35', N'G10000', N'matt@groovetechnology.com', N'Matt Long', N'Long', N'Matt', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'91502f09-e02e-43dd-738d-08d737675a35', N'G00001', N'tan.truong@groovetechnology.com', N'Tan Truong', N'Truong', N'Tan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'da548f23-ee8b-4128-738e-08d737675a35', N'G00002', N'thuan.ha@groovetechnology.com', N'Thuan Ha', N'Ha', N'Thuan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b7c39efc-675f-494f-738f-08d737675a35', N'G00004', N'tuan.hoang@groovetechnology.com', N'Tuan Hoang', N'Hoang', N'Tuan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-18T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'f3f32d44-33e0-471a-7390-08d737675a35', N'G00005', N'tho.lai@groovetechnology.com', N'Tho Lai', N'Lai', N'Tho', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-18T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'80a6001b-be1b-4209-7391-08d737675a35', N'G00007', N'hanh.tran@groovetechnology.com', N'Hanh Tran', N'Tran', N'Hanh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'a03ab6a1-b528-4083-7392-08d737675a35', N'G00008', N'an.ha@groovetechnology.com', N'An Ha', N'Ha', N'An', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'ed300fe5-d016-47fd-7393-08d737675a35', N'G00010', N'duy.nguyen@groovetechnology.com', N'Duy Nguyen', N'Nguyen', N'Duy', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b9194558-66aa-4307-7394-08d737675a35', N'G00012', N'my.trinh@groovetechnology.com', N'My Trinh', N'Trinh', N'My', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'ed4b51b0-a6ff-4647-7395-08d737675a35', N'G00013', N'hai.nguyen@groovetechnology.com', N'Hai Nguyen', N'Nguyen', N'Hai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'fdd1e876-20d0-42e3-7396-08d737675a35', N'G00014', N'sanh.nguyen@groovetechnology.com', N'Sanh Nguyen', N'Nguyen', N'Sanh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'32712441-cf5b-4d88-7397-08d737675a35', N'G00015', N'loan.ngo@groovetechnology.com', N'Loan Ngo', N'Ngo', N'Loan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd7525d1f-3c95-4bcf-7398-08d737675a35', N'G00016', N'duong.tiet@groovetechnology.com', N'Duong Tiet', N'Tiet', N'Duong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'592a4bb0-11da-49da-7399-08d737675a35', N'G00017', N'thanh.pham@groovetechnology.com', N'Thanh Pham', N'Pham', N'Thanh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'97157224-c78d-4f97-739a-08d737675a35', N'G00018', N'van.tran@groovetechnology.com', N'Van Tran', N'Tran', N'Van', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'f15c9549-b2a4-49d2-739b-08d737675a35', N'G00019', N'tri.nguyen@groovetechnology.com', N'Tri Nguyen', N'Nguyen', N'Tri', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'dcee4b20-505b-4da8-739c-08d737675a35', N'G00020', N'luong.pham@groovetechnology.com', N'Luong Pham', N'Pham', N'Luong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'0a26435c-1881-4840-739d-08d737675a35', N'G00021', N'duc.quach@groovetechnology.com', N'Duc Quach', N'Quach', N'Duc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'4dce4b22-34dc-4149-739e-08d737675a35', N'G00022', N'dung.nguyen@groovetechnology.com', N'Dung Nguyen', N'Nguyen', N'Dung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'3931623f-f03d-481e-739f-08d737675a35', N'G00023', N'dung.le@groovetechnology.com', N'Dung Le', N'Le', N'Dung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'92d11cf5-8edb-41f5-73a0-08d737675a35', N'G00024', N'phung.tran@groovetechnology.com', N'Phung Tran', N'Tran', N'Phung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'dc75fae2-7ec6-494c-73a1-08d737675a35', N'G00025', N'hieu.le@groovetechnology.com', N'Hieu Le', N'Le', N'Hieu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'8ab14c85-80d2-4759-73a2-08d737675a35', N'G00027', N'dat.phung@groovetechnology.com', N'Dat Phung', N'Phung', N'Dat', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'22baa715-43fa-429f-73a3-08d737675a35', N'G00028', N'phu.phung@groovetechnology.com', N'Phu Phung', N'Phung', N'Phu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-22T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c497f964-6c82-4259-73a4-08d737675a35', N'G00029', N'binh.doan@groovetechnology.com', N'Binh Doan', N'Doan', N'Binh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-22T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'96f66a47-f879-4939-73a5-08d737675a35', N'G00030', N'thin.nguyen@groovetechnology.com', N'Thin Nguyen', N'Nguyen', N'Thin', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-22T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'ef3799fa-3e78-4ef5-73a6-08d737675a35', N'G00032', N'huy.luu@groovetechnology.com', N'Huy Luu', N'Luu', N'Huy', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-22T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b1e99f7f-96e2-4a34-73a7-08d737675a35', N'G00033', N'linh.vu@groovetechnology.com', N'Linh Vu', N'Vu', N'Linh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'62babbf9-343a-47c9-73a8-08d737675a35', N'G00034', N'chung.ly@groovetechnology.com', N'Chung Ly', N'Ly', N'Chung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'47ad2f82-4abc-4e6e-73a9-08d737675a35', N'G00037', N'tuyen.nguyen@groovetechnology.com', N'Tuyen Nguyen', N'Nguyen', N'Tuyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-24T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'343dbb1d-db6a-4e6d-73aa-08d737675a35', N'G00040', N'victor.tran@groovetechnology.com', N'Viet Tran', N'Tran', N'Viet', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-12-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'a274c058-befa-49cb-73ab-08d737675a35', N'G00041', N'lien.chung@groovetechnology.com', N'Lien Chung', N'Chung', N'Lien', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-12-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd63240a9-6d45-4c69-73ac-08d737675a35', N'G00042', N'mai.duong@groovetechnology.com', N'Mai Duong', N'Duong', N'Mai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'49790a0b-4f67-45f6-73ad-08d737675a35', N'G00043', N'phuoc.hoang@groovetechnology.com', N'Phuoc Hoang', N'Hoang', N'Phuoc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2bc446ed-277c-43dc-73ae-08d737675a35', N'G00044', N'ba.nguyen@groovetechnology.com', N'Ba Nguyen', N'Nguyen', N'Ba', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'33fd95b5-0612-45da-73af-08d737675a35', N'G00045', N'anh.nguyen@groovetechnology.com', N'Anh Nguyen', N'Nguyen', N'Anh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-09T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b92f221d-89bc-4dc1-73b0-08d737675a35', N'G00046', N'tin.truong@groovetechnology.com', N'Tin Truong', N'Truong', N'Tin', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-16T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'0df351be-417c-4b6b-73b1-08d737675a35', N'G00049', N'hieu.tran@groovetechnology.com', N'Hieu Tran', N'Tran', N'Hieu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'bf6ccbd5-2132-4c28-73b2-08d737675a35', N'G00051', N'huyen.nguyent@groovetechnology.com', N'Huyen Nguyen', N'Nguyen', N'Huyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'f645fa18-9b70-4395-73b3-08d737675a35', N'G00052', N'truc.ha@groovetechnology.com', N'Truc Ha', N'Ha', N'Truc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'905c1d00-8664-4e6b-73b4-08d737675a35', N'G00053', N'mai.nguyen@groovetechnology.com', N'Mai Nguyen', N'Nguyen', N'Mai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-03-06T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'8913e9b2-0238-4ea3-73b5-08d737675a35', N'G00055', N'thao.nguyen@groovetechnology.com', N'Thao Nguyen', N'Nguyen', N'Thao', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-03-15T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'444ef3b1-200e-4650-73b6-08d737675a35', N'G00056', N'tuan.nguyen@groovetechnology.com', N'Tuan Nguyen', N'Nguyen', N'Tuan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-03-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'0f30ec33-8684-48b7-73b7-08d737675a35', N'G00058', N'hai.le@groovetechnology.com', N'Hai Le', N'Le', N'Hai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-03-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'8da98199-08f7-418b-73b8-08d737675a35', N'G00059', N'tri.le@groovetechnology.com', N'Tri Le', N'Le', N'Tri', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-04-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'570cd560-9252-45a3-73b9-08d737675a35', N'G00063', N'nhi.truong@groovetechnology.com', N'Nhi Truong', N'Truong', N'Nhi', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-04-10T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'9bfc46bb-2484-4c4d-73ba-08d737675a35', N'G00064', N'mai.nguyentn@groovetechnology.com', N'Mai Nguyen', N'Nguyen', N'Mai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-04-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'47cdb882-5848-48f9-73bb-08d737675a35', N'G00066', N'khang.tran@groovetechnology.com', N'Khang Tran', N'Tran', N'Khang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-04-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'da50cceb-cb04-4bf8-73bc-08d737675a35', N'G00069', N'dinh.vu@groovetechnology.com', N'Dinh Vu', N'Vu', N'Dinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-05-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'53a04f77-a6ab-44f0-73bd-08d737675a35', N'G00070', N'huy.do@groovetechnology.com', N'Huy Do', N'Do', N'Huy', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-05-08T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'59873592-554e-48eb-73be-08d737675a35', N'G00071', N'hung.nguyen@groovetechnology.com', N'Hung Nguyen', N'Nguyen', N'Hung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-07-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'4169389d-9ca4-46a2-73bf-08d737675a35', N'G00072', N'hung.doan@groovetechnology.com', N'Hung Doan', N'Doan', N'Hung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-08-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'a0f79b16-b304-4792-73c0-08d737675a35', N'G00074', N'thinh.vo@groovetechnology.com', N'Thinh Vo', N'Vo', N'Thinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-09-05T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'08ed8955-211b-411e-73c1-08d737675a35', N'G00079', N'dung.nguyentm@groovetechnology.com', N'Dung Nguyen', N'Nguyen', N'Dung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-10-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd7f9fba1-dd70-4c8f-73c2-08d737675a35', N'G00083', N'vu.nguyen@groovetechnology.com', N'Vu Nguyen', N'Nguyen', N'Vu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-10-16T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'34b9cd56-f1eb-433f-73c3-08d737675a35', N'G00084', N'vinh.trang@groovetechnology.com', N'Vinh Trang', N'Trang', N'Vinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-11-06T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c9fcc43a-a9e0-4482-73c4-08d737675a35', N'G00087', N'trinh.nguyen@groovetechnology.com', N'Trinh Nguyen', N'Nguyen', N'Trinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-11-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'12a90d06-6419-4a6f-73c5-08d737675a35', N'G00088', N'vinh.trinh@groovetechnology.com', N'Vinh Trinh', N'Trinh', N'Vinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-11-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2ebff174-b6aa-4d4f-73c6-08d737675a35', N'G00090', N'phuong.nguyen@groovetechnology.com', N'Phuong Nguyen', N'Nguyen', N'Phuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-01-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'6a082577-42db-44bf-73c7-08d737675a35', N'G00093', N'dung.trann@groovetechnology.com', N'Dung Tran', N'Tran', N'Dung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-02-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'67110b67-26af-4c53-73c8-08d737675a35', N'G00095', N'hai.hoang@groovetechnology.com', N'Hai Hoang', N'Hoang', N'Hai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-02-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'bc6de099-0a84-4129-73c9-08d737675a35', N'G00098', N'vuong.do@groovetechnology.com', N'Vuong Do', N'Do', N'Vuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-03-05T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'483b31ac-f275-49cc-73ca-08d737675a35', N'G00099', N'nam.ngo@groovetechnology.com', N'Nam Ngo', N'Ngo', N'Nam', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-03-05T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b6f8c502-aea8-4fa7-73cb-08d737675a35', N'G00100', N'ngan.luong@groovetechnology.com', N'Ngan Luong', N'Luong', N'Ngan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-03-19T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'7f54a997-5139-4b33-73cc-08d737675a35', N'G00103', N'chuong.le@groovetechnology.com', N'Chuong Le', N'Le', N'Chuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-04-09T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd183ce3d-40a9-4e98-73cd-08d737675a35', N'G00108', N'minh.vu@groovetechnology.com', N'Minh Vu', N'Vu', N'Minh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-05-15T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'e2b58443-da54-4c85-73ce-08d737675a35', N'G00109', N'cuong.phan@groovetechnology.com', N'Cuong Phan', N'Phan', N'Cuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-04T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'59b0c65a-279a-47ab-73cf-08d737675a35', N'G00110', N'khoi.nguyen@groovetechnology.com', N'Khoi Nguyen', N'Nguyen', N'Khoi', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-04T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'f43ad146-61c4-44de-73d0-08d737675a35', N'G00111', N'thai.nguyen@groovetechnology.com', N'Thai Nguyen', N'Nguyen', N'Thai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-04T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'696054f7-20aa-491c-73d1-08d737675a35', N'G00113', N'tin.pham@groovetechnology.com', N'Tin Pham', N'Pham', N'Tin', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-25T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'200f9ce5-5313-4df2-73d2-08d737675a35', N'G00114', N'van.tang@groovetechnology.com', N'Van Tang', N'Tang', N'Van', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-25T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'75b0e9cb-261e-4b73-73d3-08d737675a35', N'G00116', N'truc.luc@groovetechnology.com', N'Truc Luc', N'Luc', N'Truc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-07-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c1f6660f-9570-41a9-73d4-08d737675a35', N'G00117', N'phuong.nguyenduy@groovetechnology.com', N'Phuong Nguyen', N'Nguyen', N'Phuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-07-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b2e046f4-ad06-4c7d-73d5-08d737675a35', N'G00121', N'hoa.nguyen@groovetechnology.com', N'Hoa Nguyen', N'Nguyen', N'Hoa', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-07-16T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'4af4be05-c351-427c-73d6-08d737675a35', N'G00122', N'cuc.nguyen@groovetechnology.com', N'Cuc Nguyen', N'Nguyen', N'Cuc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-07-16T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'4417eb5b-e9c8-4741-73d7-08d737675a35', N'G00123', N'quynh.ho@groovetechnology.com', N'Quynh Ho', N'Ho', N'Quynh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-08-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'e93afdee-9104-49b1-73d8-08d737675a35', N'G00124', N'cuong.duong@groovetechnology.com', N'Cuong Duong', N'Duong', N'Cuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-08-06T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b8fa96ee-e10e-4863-73d9-08d737675a35', N'G00126', N'thien.nguyen@groovetechnology.com', N'Thien Nguyen', N'Nguyen', N'Thien', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-08-13T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'275ce4ae-07f8-4419-73da-08d737675a35', N'G00127', N'tuan.thai@groovetechnology.com', N'Tuan Thai', N'Thai', N'Tuan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-08-13T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'367bf9c7-fe68-40d7-73db-08d737675a35', N'G00129', N'thanh.vong@groovetechnology.com', N'Thanh Vong', N'Vong', N'Thanh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-09-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'1533b70e-34db-449b-73dc-08d737675a35', N'G00130', N'trung.nguyen@groovetechnology.com', N'Trung Nguyen', N'Nguyen', N'Trung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-08T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'3a3db162-8a9e-4345-73dd-08d737675a35', N'G00132', N'thuy.nguyenlm@groovetechnology.com', N'Thuy Nguyen', N'Nguyen', N'Thuy', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'8b169d0a-50bd-4337-73de-08d737675a35', N'G00133', N'duc.doan@groovetechnology.com', N'Duc Doan', N'Doan', N'Duc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'6bd1c6c7-e4f8-43d7-73df-08d737675a35', N'G00134', N'vu.nguyenhuy@groovetechnology.com', N'Vu Nguyen Huy', N'Nguyen Huy', N'Vu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'06657f8c-7ac7-4b74-73e0-08d737675a35', N'G00135', N'dinh.nguyen@groovetechnology.com', N'Dinh Nguyen', N'Nguyen', N'Dinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'8aeb0af7-1791-4864-73e1-08d737675a35', N'G00136', N'tan.trinh@groovetechnology.com', N'Tan Trinh', N'Trinh', N'Tan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'29e83c4c-ad9d-47d0-73e2-08d737675a35', N'G00140', N'cuong.tran@groovetechnology.com', N'Cuong Tran', N'Tran', N'Cuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-11-12T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'9ac017a3-4ea6-4402-73e3-08d737675a35', N'G00142', N'an.nguyen@groovetechnology.com', N'An Nguyen', N'Nguyen', N'An', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-11-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'57ced7af-b5d5-45cb-73e4-08d737675a35', N'G00143', N'thang.nguyen@groovetechnology.com', N'Thang Nguyen', N'Nguyen', N'Thang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-11-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'fc645229-e309-44bb-73e5-08d737675a35', N'G00144', N'tam.hua@groovetechnology.com', N'Tam Hua', N'Hua', N'Tam', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-12-24T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'328dc22a-129c-48d5-73e6-08d737675a35', N'G00146', N'an.ho@groovetechnology.com', N'An Ho', N'Ho', N'An', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-01-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'1e715c62-89a0-472e-73e7-08d737675a35', N'G00147', N'khang.ton@groovetechnology.com', N'Khang Ton', N'Ton', N'Khang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-01-28T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'9f145e0e-6e0f-4e9b-73e8-08d737675a35', N'G00157', N'hoang.ha@groovetechnology.com', N'Hoang Ha', N'Ha', N'Hoang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-02-18T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd6f8c674-3998-46fb-73e9-08d737675a35', N'G00148', N'huy.nguyen@groovetechnology.com', N'Huy Nguyen', N'Nguyen', N'Huy', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'57d7b635-0b4c-4a15-73ea-08d737675a35', N'G00149', N'bach.nguyen@groovetechnology.com', N'Bach Nguyen', N'Nguyen', N'Bach', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'185a29f7-7f63-4310-73eb-08d737675a35', N'G00150', N'thang.tran@groovetechnology.com', N'Thang Tran', N'Tran', N'Thang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-02-25T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'ccb3c68e-5fa1-4614-73ec-08d737675a35', N'G00151', N'phong.nguyen@groovetechnology.com', N'Phong Nguyen', N'Nguyen', N'Phong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-03-04T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'a0fc21cf-6b5a-4c96-73ed-08d737675a35', N'G00152', N'tuan.truong@groovetechnology.com', N'Tuan Truong', N'Truong', N'Tuan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-04-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c13d063c-5cec-47f4-73ee-08d737675a35', N'G00153', N'phuoc.le@groovetechnology.com', N'Phuoc Le', N'Le', N'Phuoc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-04-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'54499bc4-503e-4a72-73ef-08d737675a35', N'G00159', N'hoa.pham@groovetechnology.com', N'Hoa Pham', N'Pham', N'Hoa', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-05-20T00:00:00.0000000' AS DateTime2), 1)
GO
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2eb36971-47e4-4ec5-bcee-21283b2388c7', N'G00160', N'truc.phan@groovetechnology.com', N'Truc Phan', N'Phan', N'Truc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-06-17T00:00:00.0000000' AS DateTime2), 1)
ALTER TABLE [dbo].[AppraisalParticipants] ADD  DEFAULT (CONVERT([bit],(0))) FOR [IsInvited]
GO
ALTER TABLE [dbo].[AppraisalParticipants]  WITH CHECK ADD  CONSTRAINT [FK_AppraisalParticipants_Appraisals_AppraisalId] FOREIGN KEY([AppraisalId])
REFERENCES [dbo].[Appraisals] ([Id])
GO
ALTER TABLE [dbo].[AppraisalParticipants] CHECK CONSTRAINT [FK_AppraisalParticipants_Appraisals_AppraisalId]
GO
ALTER TABLE [dbo].[AppraisalParticipants]  WITH CHECK ADD  CONSTRAINT [FK_AppraisalParticipants_Roles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[AppraisalParticipants] CHECK CONSTRAINT [FK_AppraisalParticipants_Roles_RoleId]
GO
ALTER TABLE [dbo].[AppraisalParticipants]  WITH CHECK ADD  CONSTRAINT [FK_AppraisalParticipants_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[AppraisalParticipants] CHECK CONSTRAINT [FK_AppraisalParticipants_Users_UserId]
GO
ALTER TABLE [dbo].[Appraisals]  WITH CHECK ADD  CONSTRAINT [FK_Appraisals_PerformanceReviewCycles_PerformanceReviewCycleId] FOREIGN KEY([PerformanceReviewCycleId])
REFERENCES [dbo].[PerformanceReviewCycles] ([Id])
GO
ALTER TABLE [dbo].[Appraisals] CHECK CONSTRAINT [FK_Appraisals_PerformanceReviewCycles_PerformanceReviewCycleId]
GO
ALTER TABLE [dbo].[Criterias]  WITH CHECK ADD  CONSTRAINT [FK_Criterias_CriteriaGroups_CriteriaGroupId] FOREIGN KEY([CriteriaGroupId])
REFERENCES [dbo].[CriteriaGroups] ([Id])
GO
ALTER TABLE [dbo].[Criterias] CHECK CONSTRAINT [FK_Criterias_CriteriaGroups_CriteriaGroupId]
GO
ALTER TABLE [dbo].[CriteriaWeights]  WITH CHECK ADD  CONSTRAINT [FK_CriteriaWeights_Criterias_CriteriaId] FOREIGN KEY([CriteriaId])
REFERENCES [dbo].[Criterias] ([Id])
GO
ALTER TABLE [dbo].[CriteriaWeights] CHECK CONSTRAINT [FK_CriteriaWeights_Criterias_CriteriaId]
GO
ALTER TABLE [dbo].[CriteriaWeights]  WITH CHECK ADD  CONSTRAINT [FK_CriteriaWeights_LevelCriteriaGroups_LevelCriteriaGroupId] FOREIGN KEY([LevelCriteriaGroupId])
REFERENCES [dbo].[LevelCriteriaGroups] ([Id])
GO
ALTER TABLE [dbo].[CriteriaWeights] CHECK CONSTRAINT [FK_CriteriaWeights_LevelCriteriaGroups_LevelCriteriaGroupId]
GO
ALTER TABLE [dbo].[EmployeeProjects]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeProjects_Projects_ProjectId] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([Id])
GO
ALTER TABLE [dbo].[EmployeeProjects] CHECK CONSTRAINT [FK_EmployeeProjects_Projects_ProjectId]
GO
ALTER TABLE [dbo].[EmployeeProjects]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeProjects_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[EmployeeProjects] CHECK CONSTRAINT [FK_EmployeeProjects_Users_UserId]
GO
ALTER TABLE [dbo].[Goals]  WITH CHECK ADD  CONSTRAINT [FK_Goals_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Goals] CHECK CONSTRAINT [FK_Goals_Users_UserId]
GO
ALTER TABLE [dbo].[LevelCriteriaGroups]  WITH CHECK ADD  CONSTRAINT [FK_LevelCriteriaGroups_CriteriaGroups_CriteriaGroupId] FOREIGN KEY([CriteriaGroupId])
REFERENCES [dbo].[CriteriaGroups] ([Id])
GO
ALTER TABLE [dbo].[LevelCriteriaGroups] CHECK CONSTRAINT [FK_LevelCriteriaGroups_CriteriaGroups_CriteriaGroupId]
GO
ALTER TABLE [dbo].[LevelCriteriaGroups]  WITH CHECK ADD  CONSTRAINT [FK_LevelCriteriaGroups_Levels_LevelId] FOREIGN KEY([LevelId])
REFERENCES [dbo].[Levels] ([Id])
GO
ALTER TABLE [dbo].[LevelCriteriaGroups] CHECK CONSTRAINT [FK_LevelCriteriaGroups_Levels_LevelId]
GO
ALTER TABLE [dbo].[Objectives]  WITH CHECK ADD  CONSTRAINT [FK_Objectives_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Objectives] CHECK CONSTRAINT [FK_Objectives_Users_UserId]
GO
ALTER TABLE [dbo].[PerformanceReviewCycles]  WITH CHECK ADD  CONSTRAINT [FK_PerformanceReviewCycles_AppraisalModes_AppraisalModeId] FOREIGN KEY([AppraisalModeId])
REFERENCES [dbo].[AppraisalModes] ([Id])
GO
ALTER TABLE [dbo].[PerformanceReviewCycles] CHECK CONSTRAINT [FK_PerformanceReviewCycles_AppraisalModes_AppraisalModeId]
GO
ALTER TABLE [dbo].[PerformanceReviewCycles]  WITH CHECK ADD  CONSTRAINT [FK_PerformanceReviewCycles_Cycles_CycleId] FOREIGN KEY([CycleId])
REFERENCES [dbo].[Cycles] ([Id])
GO
ALTER TABLE [dbo].[PerformanceReviewCycles] CHECK CONSTRAINT [FK_PerformanceReviewCycles_Cycles_CycleId]
GO
ALTER TABLE [dbo].[Projects]  WITH CHECK ADD  CONSTRAINT [FK_Projects_Teams_TeamId] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[Projects] CHECK CONSTRAINT [FK_Projects_Teams_TeamId]
GO
ALTER TABLE [dbo].[Ratings]  WITH CHECK ADD  CONSTRAINT [FK_Ratings_AppraisalParticipants_AppraisalParticipantId] FOREIGN KEY([AppraisalParticipantId])
REFERENCES [dbo].[AppraisalParticipants] ([Id])
GO
ALTER TABLE [dbo].[Ratings] CHECK CONSTRAINT [FK_Ratings_AppraisalParticipants_AppraisalParticipantId]
GO
ALTER TABLE [dbo].[Ratings]  WITH CHECK ADD  CONSTRAINT [FK_Ratings_CriteriaWeights_CriteriaWeightId] FOREIGN KEY([CriteriaWeightId])
REFERENCES [dbo].[CriteriaWeights] ([Id])
GO
ALTER TABLE [dbo].[Ratings] CHECK CONSTRAINT [FK_Ratings_CriteriaWeights_CriteriaWeightId]
GO
ALTER TABLE [dbo].[ReviewTimelines]  WITH CHECK ADD  CONSTRAINT [FK_ReviewTimelines_Appraisals_AppraisalId] FOREIGN KEY([AppraisalId])
REFERENCES [dbo].[Appraisals] ([Id])
GO
ALTER TABLE [dbo].[ReviewTimelines] CHECK CONSTRAINT [FK_ReviewTimelines_Appraisals_AppraisalId]
GO
ALTER TABLE [dbo].[ReviewTimelines]  WITH CHECK ADD  CONSTRAINT [FK_ReviewTimelines_Stages_StageId] FOREIGN KEY([StageId])
REFERENCES [dbo].[Stages] ([Id])
GO
ALTER TABLE [dbo].[ReviewTimelines] CHECK CONSTRAINT [FK_ReviewTimelines_Stages_StageId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_BusinessUnits_BusinessUnitId] FOREIGN KEY([BusinessUnitId])
REFERENCES [dbo].[BusinessUnits] ([Id])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_BusinessUnits_BusinessUnitId]
GO
ALTER TABLE [dbo].[UserLevels]  WITH CHECK ADD  CONSTRAINT [FK_UserLevels_Levels_LevelId] FOREIGN KEY([LevelId])
REFERENCES [dbo].[Levels] ([Id])
GO
ALTER TABLE [dbo].[UserLevels] CHECK CONSTRAINT [FK_UserLevels_Levels_LevelId]
GO
ALTER TABLE [dbo].[UserLevels]  WITH CHECK ADD  CONSTRAINT [FK_UserLevels_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UserLevels] CHECK CONSTRAINT [FK_UserLevels_Users_UserId]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Roles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Roles_RoleId]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Users_UserId]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_ContractTypes_ContractTypeId] FOREIGN KEY([ContractTypeId])
REFERENCES [dbo].[ContractTypes] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_ContractTypes_ContractTypeId]
GO
