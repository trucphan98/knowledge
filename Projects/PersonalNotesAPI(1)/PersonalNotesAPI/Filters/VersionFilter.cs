﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Text.RegularExpressions;

namespace PersonalNotesAPI.Filters
{
    public class VersonFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string user_agent = context.HttpContext.Request.Headers["User-Agent"];
            Regex expression = new Regex(@"(?<= Chrome\/)(.*?)(?=\.)");
            Match results = expression.Match(user_agent);
            int version = int.Parse(results.Value);
            if (version < 70)
            {
                context.Result = new ObjectResult("Can't process this!")
                {
                    StatusCode = 422,
                };
            }
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
        }
    }
}
