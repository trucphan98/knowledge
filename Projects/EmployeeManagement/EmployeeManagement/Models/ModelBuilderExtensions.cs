﻿using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Models
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Employee>().HasData(
                new Employee
                {
                    Id = 1,
                    Name = "Mark",
                    Department = "IT",
                    Email = "nam@gmail.com"
                },
            new Employee
            {
                Id = 2,
                Name = "Mark2",
                Department = "HR",
                Email = "Mark2@gmail.com"
            });
        }
    }
}
