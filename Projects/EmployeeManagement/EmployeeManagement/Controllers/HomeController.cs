﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EmployeeManagement.Models;

namespace EmployeeManagement.Controllers
{
    public class HomeController : Controller
    {
        private IEmployeeRepository _employRepository;

        public HomeController(IEmployeeRepository employRepository)
        {
            _employRepository = employRepository;
        }
        
        // Customize Route [Route("Hello/yo")]
        public ViewResult Index()
        {
            var model = _employRepository.GetAllEmployee();
            return View(model);
        }
        [Route("Home/Details/{Id}")]
        public ViewResult Details(int Id)
        {
            Employee model = _employRepository.GetEmployee(Id);
            return View(model);
        }
    }
}
