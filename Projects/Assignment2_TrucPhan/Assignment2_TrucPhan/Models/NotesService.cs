﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment2_TrucPhan.Models
{
    public class NotesService : INoteRepository
    {
        public Note this[int id] => throw new NotImplementedException();

        public List<Note> NotesData = new List<Note>(){
            new Note {
                Id =1,
                Title ="Note01",
                Description ="Description note 01",
                Finished =false,
                CreatedBy ="Truc",
                CreatedOn =DateTime.Now,
                UpdatedBy ="",
                UpdatedOn =null,
                Deleted =false,
                NotebookId =1,
                Timestamp =null },
            new Note {
                Id =2,
                Title ="Note02",
                Description ="Description note 02",
                Finished =false,
                CreatedBy ="Truc",
                CreatedOn =DateTime.Now,
                UpdatedBy ="",
                UpdatedOn =null,
                Deleted =false,
                NotebookId =1,
                Timestamp =null },
            new Note {
                Id =3,
                Title ="Note03",
                Description ="Description note 03",
                Finished =false,
                CreatedBy ="Truc",
                CreatedOn =DateTime.Now,
                UpdatedBy ="",
                UpdatedOn =null,
                Deleted =false,
                NotebookId =2,
                Timestamp =null }
            };

        public IEnumerable<Note> Notes => NotesData;

        public Note AddNote(Note reservation)
        {
            throw new NotImplementedException();
        }

        public void DeleteNote(int id)
        {
            throw new NotImplementedException();
        }

        public Note UpdateNote(Note reservation)
        {
            throw new NotImplementedException();
        }
    }
}
