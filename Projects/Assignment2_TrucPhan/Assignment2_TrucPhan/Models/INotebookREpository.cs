﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment2_TrucPhan.Models
{
    public interface INotebookRepository
    {
        IEnumerable<Notebook> Notebooks { get; }
        Notebook this[int id] { get; }
        Notebook AddNotebook(Notebook reservation);
        Notebook UpdateNotebook(Notebook reservation);
        void DeleteNotebook(int id);
    }
}
