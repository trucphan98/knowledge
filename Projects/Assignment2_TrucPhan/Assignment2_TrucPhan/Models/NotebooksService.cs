﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment2_TrucPhan.Models
{
    public class NotebooksService : INotebookRepository
    {
        public List<Notebook> NotebooksData = new List<Notebook>(){
            new Notebook {
                Id =1,
                Name ="Notebook01",
                ParentId =null,
                CreatedBy ="Truc",
                CreatedOn =DateTime.Now,
                UpdatedBy ="",
                UpdatedOn =null,
                Deleted =false,
                Timestamp =null },
            new Notebook {
                Id =2,
                Name ="Note02",
                ParentId =null,
                CreatedBy ="Truc",
                CreatedOn =DateTime.Now,
                UpdatedBy ="",
                UpdatedOn =null,
                Deleted =false,
                Timestamp =null }
        };
        public Notebook this[int id] => NotebooksData.FirstOrDefault(x=>x.Id==id);
           
        public IEnumerable<Notebook> Notebooks => NotebooksData;

        public Notebook AddNotebook(Notebook newNotebook)
        {
            NotebooksData.Add(newNotebook);
            return newNotebook;
        }

        public void DeleteNotebook(int id)
        {
            throw new NotImplementedException();
        }

        public Notebook UpdateNotebook(Notebook reservation)
        {
            throw new NotImplementedException();
        }
    }
}
