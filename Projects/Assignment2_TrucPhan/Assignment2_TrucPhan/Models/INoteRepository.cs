﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment2_TrucPhan.Models
{
    public interface INoteRepository
    {
        IEnumerable<Note> Notes { get; }
        Note this[int id] { get; }
        Note AddNote(Note reservation);
        Note UpdateNote(Note reservation);
        void DeleteNote(int id);
    }
}
