﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment2_TrucPhan.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Assignment2_TrucPhan.Controllers
{
    [Route("api/notebooks")]
    [ApiController]
    public class NotebooksController : ControllerBase
    {
        private INotebookRepository notebookRepository;

        public NotebooksController(INotebookRepository repo) => notebookRepository = repo;

        // GET: api/Notebooks
        [HttpGet]
        public IEnumerable<Notebook> Get()
        {
            return notebookRepository.Notebooks;
        }

        // GET: api/Notebooks/5
        [HttpGet("{id}", Name = "GetNotebook")]
        public Notebook Get(int id)
        {
            return notebookRepository[id];
        }

        // POST: api/Notebooks
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Notebooks/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
