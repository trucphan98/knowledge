﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment2_TrucPhan.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Assignment2_TrucPhan.Controllers
{
    [Route("api/notes")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        private INoteRepository noteRepository;

        public NotesController(INoteRepository repo) => noteRepository = repo;
        
        // GET: api/Notes
        [HttpGet]
        public IEnumerable<Note> Get()
        {
            return noteRepository.Notes;
        }

        // GET: api/Notes/5
        [HttpGet("{id}", Name = "GetNote")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Notes
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Notes/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
