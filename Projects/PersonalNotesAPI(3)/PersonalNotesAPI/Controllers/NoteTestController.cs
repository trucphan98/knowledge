﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PersonalNotesAPI.Interface.Repositories;
using PersonalNotesAPI.Models;

namespace PersonalNotesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NoteTestController : ControllerBase
    {
        public readonly INoteRepository _noteService;

        public NoteTestController(INoteRepository noteService)
        {
            _noteService = noteService;
        }

        // GET: api/NoteTest
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/NoteTest/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/NoteTest
        //[HttpPost]
        //public IActionResult CreateNote([FromBody] CreateNote note)
        //{
        //    //if (ModelState.IsValid)
        //    //{
        //    //    _noteService.AddNote(note);
        //    //}
        //    //else
        //    //{
        //    //    return new ObjectResult(note);
        //    //}
        //    //return Ok();
        //}

        // PUT: api/NoteTest/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
