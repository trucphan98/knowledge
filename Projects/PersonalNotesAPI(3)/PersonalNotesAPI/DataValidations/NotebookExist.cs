﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using PersonalNotesAPI.Models;
using PersonalNotesAPI.Repositories.Interface;

namespace PersonalNotesAPI.DataValidation
{
    public class NotebookExist: ValidationAttribute
    {
        public INotebookRepository _notebooksRepository;

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            INotebookRepository db = (INotebookRepository)validationContext.GetService(typeof(INotebookRepository));
            if (db.Notebooks.FirstOrDefault(x => x.Id == (int)value) == null)
                return new ValidationResult("ParentId does not exist");
            else
                return ValidationResult.Success;
        }
    }
}
