﻿
using PersonalNotesAPI.Repositories.Interface;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PersonalNotesAPI.DataValidation
{
    public class NoteExist : ValidationAttribute
    {
        public INotebookRepository _notebooksRepository;

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            INotebookRepository db = (INotebookRepository)validationContext.GetService(typeof(INotebookRepository));
            if (db.Notebooks.FirstOrDefault(x => x.Id == (int)value) == null)
                return new ValidationResult("NotebookId does not exist");
            else
                return ValidationResult.Success;
        }
    }
}
