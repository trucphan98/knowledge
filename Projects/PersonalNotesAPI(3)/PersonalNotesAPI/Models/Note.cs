﻿using Microsoft.AspNetCore.Mvc;
using PersonalNotesAPI.DataValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalNotesAPI.Models
{
    public class Note
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [StringLength(50)]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        public bool Finished { get; set; }

        //[NoteExist]
        public int NotebookId { get; set; }

        [Required(ErrorMessage = "CreatedBy is required")]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public bool? Deleted { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

    }
}
