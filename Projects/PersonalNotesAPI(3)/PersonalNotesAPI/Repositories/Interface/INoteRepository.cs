﻿using PersonalNotesAPI.Entities;
using PersonalNotesAPI.Models;
using System.Collections.Generic;

namespace PersonalNotesAPI.Interface.Repositories
{
    public interface INoteRepository
    {
        IEnumerable<NoteEntity> Notes();
        NoteEntity GetNoteEntityById(int id);
        void AddNote(NoteEntity note);
        void UpdateNote(NoteEntity note);
        void DeleteNote(int id);
    }
}
