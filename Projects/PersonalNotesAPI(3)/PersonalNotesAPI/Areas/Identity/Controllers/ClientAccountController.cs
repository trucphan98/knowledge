﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PersonalNotesAPI.Auth;
using PersonalNotesAPI.Data;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace PersonalNotesAPI.Identity.Controllers
{
    [Route("identity/[controller]")]
    [ApiController]
    public class ClientAccountController : ControllerBase
    {
        public class LoginModel
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger<LoginModel> _logger;
        private readonly IConfiguration _config;

        public ClientAccountController(SignInManager<ApplicationUser> signInManager, ILogger<LoginModel> logger, IConfiguration config)
        {
            _signInManager = signInManager;
            _logger = logger;
            _config = config;
        }

        public IActionResult Index()
        {
            return Ok();
        }
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(LoginModel data)
        {
            var result = await _signInManager.PasswordSignInAsync(data.Username, data.Password, false, lockoutOnFailure: true);
            if (result.Succeeded)
            {
                _logger.LogInformation("Login Successfully");
                var tokenString = AuthTokenUtil.GetJwtSecurityTokenString(data.Username, _config);
                return new ObjectResult(tokenString);
            }
            return Unauthorized();
        }
    }
}