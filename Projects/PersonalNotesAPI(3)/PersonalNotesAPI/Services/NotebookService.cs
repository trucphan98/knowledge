﻿using PersonalNotesAPI.Data;
using PersonalNotesAPI.Models;
using PersonalNotesAPI.Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonalNotesAPI.Repositories
{
    public class NotebookService : INotebookRepository
    {
        private readonly NoteDBContext context;

        public NotebookService(NoteDBContext context)
        {
            this.context = context;
        }

        public Notebook this[int id] => context.Notebook.FirstOrDefault(x => x.Id == id);

        public IEnumerable<Notebook> Notebooks => context.Notebook;

        public Notebook AddNotebook(Notebook notebook)
        {
            context.Notebook.Add(notebook);
            context.SaveChanges();
            return notebook;
        }

        public void DeleteNotebook(int id)
        {
            Notebook notenook = context.Notebook.FirstOrDefault(x => x.Id == id);
            context.Notebook.Remove(notenook);
            context.SaveChanges();
        }

        public Notebook UpdateNotebook(Notebook notebook)
        {
            var oldNotebook = context.Notebook.Attach(notebook);
            oldNotebook.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
            return notebook;
        }
    }
}
