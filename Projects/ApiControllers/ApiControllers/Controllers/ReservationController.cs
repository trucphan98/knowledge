﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiControllers.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace ApiControllers.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationController : ControllerBase
    {
        private IRepository repository;
        public ReservationController(IRepository repo) => repository = repo;
        // GET: api/Reservation
        [HttpGet]
        public IEnumerable<Reservation> Get() => repository.Reservations;

        // GET: api/Reservation/5
        [HttpGet("{id}", Name = "Get")]
        public Reservation Get(int id) => repository[id];


        // POST: api/Reservation
        [HttpPost]
        public void Post([FromBody] Reservation res) =>
            repository.AddReservation(new Reservation
            {
                ClientName = res.ClientName,
                Location = res.Location
            });

        // PUT: api/Reservation/5
        [HttpPut]
        public void Put(int id, [FromBody] Reservation res) =>
            repository.UpdateReservation(res);


        [HttpPatch("{id}")]
        public StatusCodeResult Patch(int id,[FromBody]JsonPatchDocument<Reservation> patch)
        {
            Reservation res = Get(id);
            if (res != null)
            {
                patch.ApplyTo(res);
                return Ok();
            }
            return NotFound();
        }
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id) => repository.DeleteReservation(id);
    }
}
