import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

const authUrl = 'https://localhost:44316/api/account';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {

  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const securityToken = this.authService.getToken();
    if (req.url === authUrl) {
      req.headers.set('Content-Type', 'application/json; charset=UTF-8');
      return next.handle(req);
    } else if (securityToken.length > 0) {
      req = req.clone({
        setHeaders: {
          'Content-Type': 'application/json; charset=UTF-8',
          Authorization: `Bearer ${securityToken}`
        }
      });
      return next.handle(req);
    }

  }
}

