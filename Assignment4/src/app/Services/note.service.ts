import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Note, INote } from '../Models/note';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class NoteService {

  constructor(private http: HttpClient) { }

  getAllNotes(): Observable<INote[]> {
    return this.http.get<INote[]>('https://localhost:44316/api/note');
  }



}
