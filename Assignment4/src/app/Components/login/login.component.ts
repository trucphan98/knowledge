import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../Services/user.service';
import { User } from '../../Models/user';
import { AuthService } from '../../Services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  get f() { return this.loginForm.controls; }

  onSubmit() {
    debugger
    // User  = {username: this.f.username.value, password: this.f.password.value}
    const a: User = { username: this.f.username.value, password: this.f.password.value };
    this.userService.Login(a)
      .subscribe(val => {
        console.log(val);
        this.authService.setToken(val);
        this.router.navigate(['dashboard']);
      },
        err => { console.log(err); });
  }
}
