import { Component, OnInit } from '@angular/core';
import { NoteService } from '../../Services/note.service';
@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit {

  constructor(
    private noteService: NoteService
  ) { }

  ngOnInit() {
    this.noteService.getAllNotes().subscribe(data => console.log(data));
  }


}
