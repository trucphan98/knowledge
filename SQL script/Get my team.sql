;
                                    WITH cte AS
                                      (SELECT ROW_NUMBER() OVER (PARTITION BY u.Id
                                                                 ORDER BY ul.PromotionDate DESC) AS rn,
                                                                u.Id,
                                                                u.EmployeeId AS GrooveId,
                                                                u.FullName AS EmployeeName,
                                                                l.Title AS EmployeeLevel,
                                                                bu.Name AS BU,
                                                                t.Name AS Team,
                                                                ROUND(SUM(r.Point * cw.Weight), 0) AS SelfReviewResult,
                                                                ap.AppraisalId,
                                                                ul.PromotionDate
                                       FROM Appraisals a
                                       INNER JOIN AppraisalParticipants ap ON a.Id = ap.AppraisalId
                                       LEFT JOIN Users u ON u.Id = a.OwnerId
                                       LEFT JOIN EmployeeProjects ep ON ep.UserId = u.Id
                                       LEFT JOIN Projects p ON p.Id = ep.ProjectId
                                       LEFT JOIN Teams t ON t.Id = p.TeamId
                                       LEFT JOIN BusinessUnits bu ON bu.Id = t.BusinessUnitId
                                       LEFT JOIN Ratings r ON r.AppraisalParticipantId = ap.Id
                                       LEFT JOIN [CriteriaWeights] cw ON cw.Id = r.CriteriaWeightId
                                       LEFT JOIN UserLevels ul ON ul.UserId = u.Id
                                       LEFT JOIN Levels l ON l.Id = ul.LevelId
						            LEFT JOIN PerformanceReviewCycles prc ON prc.Id = a.PerformanceReviewCycleId
                                       WHERE ap.RoleId = 1
                                         AND u.Email != 'truc.phan@groovetechnology.com'
                                       GROUP BY u.Id,
                                                u.EmployeeId,
                                                u.FullName,
                                                l.Title,
                                                bu.Name,
                                                t.Name,
                                                ap.AppraisalId,
                                                ul.PromotionDate)
                                    SELECT *
                                    FROM Cte
                                    WHERE rn < 2