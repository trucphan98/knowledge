SELECT DISTINCT cr.Id, cr.Name, cr.Description, cw.Weight, r.Point, r.Comment, _r.Name, (SELECT * FROM users WHERE id = _u.id FOR JSON AUTO) AS ReviewerObject, cg.id as criteriagroupid
FROM PerformanceReviewCycles prc 
INNER JOIN AppraisalModes pm ON prc.AppraisalModeId = pm.Id
INNER JOIN Appraisals a ON a.PerformanceReviewCycleId = prc.Id
INNER JOIN AppraisalParticipants ap ON ap.AppraisalId = a.Id
INNER JOIN Users _u ON ap.UserId = _u.Id
INNER JOIN Roles _r ON ap.RoleId = _r.Id
INNER JOIN Cycles c ON prc.CycleId = c.Id
INNER JOIN Ratings r ON r.AppraisalParticipantId = ap.id
INNER JOIN criteriaWeights cw ON r.CriteriaWeightId = cw.Id
INNER JOIN Users u ON u.Id = a.OwnerId
INNER JOIN UserLevels ul ON ul.UserId = u.Id
INNER JOIN Levels l ON ul.LevelId = l.Id
INNER JOIN LevelCriteriaGroups lcg ON lcg.LevelId = l.Id
INNER JOIN CriteriaGroups cg ON lcg.CriteriaGroupId = cg.Id
RIGHT JOIN Criterias cr ON cr.CriteriaGroupId = cg.Id AND cw.CriteriaId = cr.Id
WHERE a.Id = 2 AND (2 = 0 OR 2 = cg.Id)

--  @AppraisalId @CriteriaGroupId