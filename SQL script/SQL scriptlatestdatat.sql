USE [PR-dev]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppraisalModes]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppraisalModes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_AppraisalModes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppraisalParticipants]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppraisalParticipants](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[GeneralComment] [nvarchar](max) NULL,
	[IsMain] [bit] NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[AppraisalId] [int] NOT NULL,
 CONSTRAINT [PK_AppraisalParticipants] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Appraisals]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Appraisals](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DueDate] [datetime2](7) NOT NULL,
	[OwnerId] [uniqueidentifier] NOT NULL,
	[PerformanceReviewCycleId] [int] NOT NULL,
 CONSTRAINT [PK_Appraisals] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BusinessUnits]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusinessUnits](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Abbre] [nvarchar](max) NULL,
	[DeliveryManagerId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_BusinessUnits] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContractTypes]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_ContractTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CriteriaGroups]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CriteriaGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_CriteriaGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Criterias]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Criterias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Scale] [int] NOT NULL,
	[CriteriaGroupId] [int] NOT NULL,
 CONSTRAINT [PK_Criterias] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CriteriaWeights]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CriteriaWeights](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Weight] [decimal](18, 2) NOT NULL,
	[LevelCriteriaGroupId] [int] NOT NULL,
	[CriteriaId] [int] NOT NULL,
 CONSTRAINT [PK_CriteriaWeights] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cycles]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cycles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Date] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Cycles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeProjects]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeProjects](
	[ProjectId] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[JoiningDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_EmployeeProjects] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[ProjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Goals]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Goals](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[NextGoals] [nvarchar](max) NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[LastGoals] [nvarchar](max) NULL,
 CONSTRAINT [PK_Goals] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LevelCriteriaGroups]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LevelCriteriaGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Weight] [decimal](18, 2) NOT NULL,
	[LevelId] [int] NOT NULL,
	[CriteriaGroupId] [int] NOT NULL,
 CONSTRAINT [PK_LevelCriteriaGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Levels]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Levels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Abbre] [nvarchar](max) NULL,
 CONSTRAINT [PK_Levels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookups]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lookups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Objectives]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Objectives](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[NextObjectives] [nvarchar](max) NULL,
	[Plan] [nvarchar](max) NULL,
	[TlComment] [nvarchar](max) NULL,
	[PmComment] [nvarchar](max) NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[LastObjectives] [nvarchar](max) NULL,
 CONSTRAINT [PK_Objectives] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PerformanceReviewCycles]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PerformanceReviewCycles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Year] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[CycleId] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[AppraisalModeId] [int] NOT NULL,
 CONSTRAINT [PK_PerformanceReviewCycles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Projects]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Abbre] [nvarchar](max) NULL,
	[TeamId] [int] NOT NULL,
 CONSTRAINT [PK_Projects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ratings]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ratings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Point] [float] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[CriteriaWeightId] [int] NOT NULL,
	[AppraisalParticipantId] [int] NOT NULL,
 CONSTRAINT [PK_Ratings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReviewTimelines]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReviewTimelines](
	[AppraisalId] [int] NOT NULL,
	[StageId] [int] NOT NULL,
	[FromDate] [datetime2](7) NOT NULL,
	[ToDate] [datetime2](7) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_ReviewTimelines] PRIMARY KEY CLUSTERED 
(
	[AppraisalId] ASC,
	[StageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Stages]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stages](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Days] [int] NOT NULL,
 CONSTRAINT [PK_Stages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Teams]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teams](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Abbre] [nvarchar](max) NULL,
	[BusinessUnitId] [int] NOT NULL,
 CONSTRAINT [PK_Teams] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserLevels]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLevels](
	[UserId] [uniqueidentifier] NOT NULL,
	[LevelId] [int] NOT NULL,
	[PromotionDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_UserLevels] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 9/11/2019 6:24:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [uniqueidentifier] NOT NULL,
	[EmployeeId] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[FullName] [nvarchar](max) NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[isMale] [bit] NOT NULL,
	[Photo] [varbinary](max) NULL,
	[BirthDate] [datetime2](7) NOT NULL,
	[StartDate] [datetime2](7) NOT NULL,
	[ContractTypeId] [int] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190829170239_InitiateDatabase', N'3.0.0-preview6.19304.10')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190904080048_UpdateRatingDataTypeToDouble', N'3.0.0-preview6.19304.10')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190904092758_AddColumnIn_Goal_Objective', N'3.0.0-preview6.19304.10')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190904184939_UpdateDataTypeInCycleTable', N'3.0.0-preview6.19304.10')
SET IDENTITY_INSERT [dbo].[AppraisalModes] ON 

INSERT [dbo].[AppraisalModes] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Special')
INSERT [dbo].[AppraisalModes] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Probation ')
INSERT [dbo].[AppraisalModes] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Annual Review')
SET IDENTITY_INSERT [dbo].[AppraisalModes] OFF
SET IDENTITY_INSERT [dbo].[AppraisalParticipants] ON 

INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'00000000-0000-0000-0000-000000000000', 0, N'Hoi ok', 0, 7, N'02cea9a3-c47a-4ea1-bb96-2adc402e3850', 2)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId]) VALUES (7, CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, NULL, 0, 7, N'33b0662b-4f16-43d8-915e-08d7301c24a5', 5)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId]) VALUES (9, CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, NULL, 0, 7, N'd533c687-d8bb-4c76-915d-08d7301c24a5', 7)
SET IDENTITY_INSERT [dbo].[AppraisalParticipants] OFF
SET IDENTITY_INSERT [dbo].[Appraisals] ON 

INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'00000000-0000-0000-0000-000000000000', 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'02cea9a3-c47a-4ea1-bb96-2adc402e3850', 1)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'00000000-0000-0000-0000-000000000000', 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'2eb36971-47e4-4ec5-bcee-21283b2388c7', 1)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (5, CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), NULL, NULL, NULL, 0, CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), N'33b0662b-4f16-43d8-915e-08d7301c24a5', 1)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (7, CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), N'd533c687-d8bb-4c76-915d-08d7301c24a5', 1)
SET IDENTITY_INSERT [dbo].[Appraisals] OFF
SET IDENTITY_INSERT [dbo].[BusinessUnits] ON 

INSERT [dbo].[BusinessUnits] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [DeliveryManagerId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'eCommerce', N'eCom', N'00000000-0000-0000-0000-000000000000')
INSERT [dbo].[BusinessUnits] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [DeliveryManagerId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Logistics', N'LOG', N'00000000-0000-0000-0000-000000000000')
INSERT [dbo].[BusinessUnits] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [DeliveryManagerId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Operational Development', N'OD', N'00000000-0000-0000-0000-000000000000')
INSERT [dbo].[BusinessUnits] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [DeliveryManagerId]) VALUES (4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Supporting', N'SUP', N'00000000-0000-0000-0000-000000000000')
SET IDENTITY_INSERT [dbo].[BusinessUnits] OFF
SET IDENTITY_INSERT [dbo].[ContractTypes] ON 

INSERT [dbo].[ContractTypes] ([Id], [Name]) VALUES (1, N'OnGoing')
SET IDENTITY_INSERT [dbo].[ContractTypes] OFF
SET IDENTITY_INSERT [dbo].[CriteriaGroups] ON 

INSERT [dbo].[CriteriaGroups] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Management Criteria')
INSERT [dbo].[CriteriaGroups] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Performance Criteria')
SET IDENTITY_INSERT [dbo].[CriteriaGroups] OFF
SET IDENTITY_INSERT [dbo].[Criterias] ON 

INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Staff Development', N'- Ability to recognize subordinates'' strengths and weaknesses
- Capability and willingness to help staffs overcome their weaknesses
- Ability to instruct and guide subordinates to further develop their strong points.', 5, 1)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Leadership/Management skills', N'- Exhibit self-confidence
- Inspire trust and respect from others
- Ability to motivate people to perform well
- React under critical situations
- Ability to make decisions
- Ability to control and coordinate
- Ability to resolve conflicts
- Ability to delegate (assign) appropriate tasks to members', 5, 1)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Responsibility/Accountability', N'- Ability to receive workload and respond to problems
- Ability to complete tasks on time meet the deadlines with good output
- Ability to support others complete their tasks on time meet the deadlines with good output
- Go to work on time, Go to meeting and other activities on time
- Participate in company activities', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Decision making and problem solving', N'- Ability to adapt to unexpected changes
- Capable of foreseeing potential problems / issues
- Ability to give solutions to dev in task development and responsible for accuracy.
- Ability to make decision on most of tasks / defects in reviewing.
- Ask / Confirm with ATL for complicated technical issues to make final decision', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Decision making', N'- Ability to identify the problem
- Ask/Confirm with ATL for complicated technical issues to make final decision
- Ability to analysis and select the best solution on most of tasks/defects in reviewing.', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Problem solving', N'- Ability to adapt to unexpected changes/Ability to handle unexpected changes as well as complex situations
- Capable of foreseeing potential problems / issues
- Ability to give solutions to dev in task development and responsible for accuracy.', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (7, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Teamworks', N'- Ability to cooperate with other team members
- Ability to reach an outcome of mutually acceptable compromise
- Frequency of causing conflict in the team
- Ability to actively participate in team meetings, brain storming and review sessionseview sessions', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Time management and planning skills', N'- Ability to prioritize assigned tasks
- Ability to handle multi-tasking
- Willingness to work late or over weekend in order to complete the urgent tasks
- Ability to take over the task rather than development: testing, maintenance, bug fixing
- Ability to actively report status or raise issue on the assignment', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (9, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'English level', N'Refer to skill matrix for each level', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (10, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Communication skills', N'- Ability to expresses thoughts effectively in individual and group situations
- Ability to effectively transfer knowledge and know-how to others
- Documentation skill
- Ability to express ideas in writing / email', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (11, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Technical skills', N'- Level of domain knowledge of specific team/as per specific customer
- Ability to quickly learn, adjust and adapt to change technical request in project
- Technology Watch: ability to  stay current with new technologies, methods, and processes in software development industry?', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (12, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Job knowledge', N'- Ability to know how to use correct method and procedure to fulfill assigned task
- Ability to understand client''s request and requirement
- Ability to understand client''s bug report
- Abitity to actively propose feedback or solution on customer''s requirement / request', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (13, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Quality Of Work', N'- Productivity (Volume of work load vs. Time needed to finish task)
- Ablity to handle all tasks of a project with good quality and meet the deadlines
- Ability to get the job done in expected quality with no or little supervisory
- Smoothly follow the processes', 5, 2)
SET IDENTITY_INSERT [dbo].[Criterias] OFF
SET IDENTITY_INSERT [dbo].[CriteriaWeights] ON 

INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (1, CAST(0.10 AS Decimal(18, 2)), 5, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (2, CAST(0.10 AS Decimal(18, 2)), 34, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (3, CAST(0.10 AS Decimal(18, 2)), 34, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (4, CAST(0.20 AS Decimal(18, 2)), 34, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (5, CAST(0.10 AS Decimal(18, 2)), 34, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (6, CAST(0.50 AS Decimal(18, 2)), 33, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (7, CAST(0.25 AS Decimal(18, 2)), 32, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (8, CAST(0.10 AS Decimal(18, 2)), 32, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (9, CAST(0.15 AS Decimal(18, 2)), 32, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (10, CAST(0.05 AS Decimal(18, 2)), 32, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (11, CAST(0.05 AS Decimal(18, 2)), 32, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (12, CAST(0.05 AS Decimal(18, 2)), 32, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (13, CAST(0.10 AS Decimal(18, 2)), 32, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (14, CAST(0.05 AS Decimal(18, 2)), 32, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (15, CAST(0.05 AS Decimal(18, 2)), 32, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (16, CAST(0.15 AS Decimal(18, 2)), 32, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (17, CAST(0.20 AS Decimal(18, 2)), 31, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (18, CAST(0.10 AS Decimal(18, 2)), 31, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (19, CAST(0.10 AS Decimal(18, 2)), 31, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (20, CAST(0.10 AS Decimal(18, 2)), 31, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (21, CAST(0.10 AS Decimal(18, 2)), 31, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (22, CAST(0.10 AS Decimal(18, 2)), 31, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (23, CAST(0.10 AS Decimal(18, 2)), 31, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (24, CAST(0.05 AS Decimal(18, 2)), 31, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (25, CAST(0.05 AS Decimal(18, 2)), 31, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (26, CAST(0.10 AS Decimal(18, 2)), 31, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (27, CAST(0.20 AS Decimal(18, 2)), 38, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (28, CAST(0.10 AS Decimal(18, 2)), 38, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (29, CAST(0.10 AS Decimal(18, 2)), 34, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (30, CAST(0.10 AS Decimal(18, 2)), 38, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (31, CAST(0.10 AS Decimal(18, 2)), 34, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (32, CAST(0.15 AS Decimal(18, 2)), 34, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (33, CAST(0.15 AS Decimal(18, 2)), 9, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (34, CAST(0.15 AS Decimal(18, 2)), 20, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (35, CAST(0.15 AS Decimal(18, 2)), 20, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (36, CAST(0.10 AS Decimal(18, 2)), 20, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (37, CAST(0.10 AS Decimal(18, 2)), 20, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (38, CAST(0.15 AS Decimal(18, 2)), 20, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (39, CAST(0.10 AS Decimal(18, 2)), 20, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (40, CAST(0.15 AS Decimal(18, 2)), 20, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (41, CAST(0.10 AS Decimal(18, 2)), 20, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (42, CAST(0.50 AS Decimal(18, 2)), 29, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (43, CAST(0.15 AS Decimal(18, 2)), 21, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (44, CAST(0.15 AS Decimal(18, 2)), 21, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (45, CAST(0.10 AS Decimal(18, 2)), 21, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (46, CAST(0.10 AS Decimal(18, 2)), 21, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (47, CAST(0.15 AS Decimal(18, 2)), 21, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (48, CAST(0.10 AS Decimal(18, 2)), 21, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (49, CAST(0.15 AS Decimal(18, 2)), 21, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (50, CAST(0.10 AS Decimal(18, 2)), 21, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (51, CAST(0.50 AS Decimal(18, 2)), 37, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (52, CAST(0.15 AS Decimal(18, 2)), 36, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (53, CAST(0.15 AS Decimal(18, 2)), 36, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (54, CAST(0.10 AS Decimal(18, 2)), 36, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (55, CAST(0.10 AS Decimal(18, 2)), 36, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (56, CAST(0.10 AS Decimal(18, 2)), 36, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (57, CAST(0.10 AS Decimal(18, 2)), 36, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (58, CAST(0.20 AS Decimal(18, 2)), 36, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (59, CAST(0.10 AS Decimal(18, 2)), 36, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (60, CAST(0.15 AS Decimal(18, 2)), 34, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (61, CAST(0.10 AS Decimal(18, 2)), 9, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (62, CAST(0.10 AS Decimal(18, 2)), 38, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (63, CAST(0.10 AS Decimal(18, 2)), 38, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (64, CAST(0.10 AS Decimal(18, 2)), 25, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (65, CAST(0.15 AS Decimal(18, 2)), 25, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (66, CAST(0.05 AS Decimal(18, 2)), 25, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (67, CAST(0.15 AS Decimal(18, 2)), 25, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (68, CAST(0.20 AS Decimal(18, 2)), 24, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (69, CAST(0.10 AS Decimal(18, 2)), 24, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (70, CAST(0.15 AS Decimal(18, 2)), 24, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (71, CAST(0.10 AS Decimal(18, 2)), 24, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (72, CAST(0.10 AS Decimal(18, 2)), 24, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (73, CAST(0.10 AS Decimal(18, 2)), 24, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (74, CAST(0.10 AS Decimal(18, 2)), 24, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (75, CAST(0.15 AS Decimal(18, 2)), 24, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (76, CAST(0.15 AS Decimal(18, 2)), 23, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (77, CAST(0.15 AS Decimal(18, 2)), 23, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (78, CAST(0.10 AS Decimal(18, 2)), 23, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (79, CAST(0.10 AS Decimal(18, 2)), 23, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (80, CAST(0.10 AS Decimal(18, 2)), 23, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (81, CAST(0.10 AS Decimal(18, 2)), 23, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (82, CAST(0.15 AS Decimal(18, 2)), 23, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (83, CAST(0.15 AS Decimal(18, 2)), 23, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (84, CAST(0.15 AS Decimal(18, 2)), 22, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (85, CAST(0.15 AS Decimal(18, 2)), 22, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (86, CAST(0.10 AS Decimal(18, 2)), 22, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (87, CAST(0.10 AS Decimal(18, 2)), 22, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (88, CAST(0.10 AS Decimal(18, 2)), 22, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (89, CAST(0.10 AS Decimal(18, 2)), 22, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (90, CAST(0.20 AS Decimal(18, 2)), 22, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (91, CAST(0.10 AS Decimal(18, 2)), 25, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (92, CAST(0.10 AS Decimal(18, 2)), 38, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (93, CAST(0.10 AS Decimal(18, 2)), 25, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (94, CAST(0.25 AS Decimal(18, 2)), 25, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (95, CAST(0.15 AS Decimal(18, 2)), 38, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (96, CAST(0.15 AS Decimal(18, 2)), 38, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (97, CAST(0.15 AS Decimal(18, 2)), 30, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (98, CAST(0.15 AS Decimal(18, 2)), 30, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (99, CAST(0.15 AS Decimal(18, 2)), 30, 10)
GO
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (100, CAST(0.10 AS Decimal(18, 2)), 30, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (101, CAST(0.10 AS Decimal(18, 2)), 30, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (102, CAST(0.10 AS Decimal(18, 2)), 30, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (103, CAST(0.10 AS Decimal(18, 2)), 30, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (104, CAST(0.15 AS Decimal(18, 2)), 30, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (105, CAST(0.15 AS Decimal(18, 2)), 28, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (106, CAST(0.15 AS Decimal(18, 2)), 28, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (107, CAST(0.10 AS Decimal(18, 2)), 28, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (108, CAST(0.10 AS Decimal(18, 2)), 28, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (109, CAST(0.10 AS Decimal(18, 2)), 28, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (110, CAST(0.10 AS Decimal(18, 2)), 28, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (111, CAST(0.20 AS Decimal(18, 2)), 28, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (112, CAST(0.10 AS Decimal(18, 2)), 28, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (113, CAST(0.40 AS Decimal(18, 2)), 27, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (114, CAST(0.20 AS Decimal(18, 2)), 26, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (115, CAST(0.10 AS Decimal(18, 2)), 26, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (116, CAST(0.10 AS Decimal(18, 2)), 26, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (117, CAST(0.10 AS Decimal(18, 2)), 26, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (118, CAST(0.10 AS Decimal(18, 2)), 26, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (119, CAST(0.10 AS Decimal(18, 2)), 26, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (120, CAST(0.15 AS Decimal(18, 2)), 26, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (121, CAST(0.15 AS Decimal(18, 2)), 26, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (122, CAST(0.10 AS Decimal(18, 2)), 25, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (123, CAST(0.10 AS Decimal(18, 2)), 22, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (124, CAST(0.10 AS Decimal(18, 2)), 9, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (125, CAST(0.10 AS Decimal(18, 2)), 9, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (126, CAST(0.10 AS Decimal(18, 2)), 18, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (127, CAST(0.15 AS Decimal(18, 2)), 18, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (128, CAST(0.10 AS Decimal(18, 2)), 18, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (129, CAST(0.10 AS Decimal(18, 2)), 18, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (130, CAST(0.10 AS Decimal(18, 2)), 18, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (131, CAST(0.10 AS Decimal(18, 2)), 18, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (132, CAST(0.10 AS Decimal(18, 2)), 18, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (133, CAST(0.10 AS Decimal(18, 2)), 18, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (134, CAST(0.15 AS Decimal(18, 2)), 18, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (135, CAST(0.40 AS Decimal(18, 2)), 11, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (136, CAST(0.10 AS Decimal(18, 2)), 12, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (137, CAST(0.15 AS Decimal(18, 2)), 12, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (138, CAST(0.10 AS Decimal(18, 2)), 12, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (139, CAST(0.10 AS Decimal(18, 2)), 12, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (140, CAST(0.10 AS Decimal(18, 2)), 12, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (141, CAST(0.10 AS Decimal(18, 2)), 12, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (142, CAST(0.10 AS Decimal(18, 2)), 12, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (143, CAST(0.10 AS Decimal(18, 2)), 12, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (144, CAST(0.15 AS Decimal(18, 2)), 12, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (145, CAST(0.40 AS Decimal(18, 2)), 13, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (146, CAST(0.10 AS Decimal(18, 2)), 14, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (147, CAST(0.15 AS Decimal(18, 2)), 14, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (148, CAST(0.10 AS Decimal(18, 2)), 14, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (149, CAST(0.10 AS Decimal(18, 2)), 14, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (150, CAST(0.10 AS Decimal(18, 2)), 14, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (151, CAST(0.10 AS Decimal(18, 2)), 14, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (152, CAST(0.10 AS Decimal(18, 2)), 14, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (153, CAST(0.40 AS Decimal(18, 2)), 10, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (154, CAST(0.10 AS Decimal(18, 2)), 14, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (155, CAST(0.25 AS Decimal(18, 2)), 8, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (156, CAST(0.10 AS Decimal(18, 2)), 8, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (157, CAST(0.10 AS Decimal(18, 2)), 5, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (158, CAST(0.10 AS Decimal(18, 2)), 6, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (159, CAST(0.10 AS Decimal(18, 2)), 6, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (160, CAST(0.05 AS Decimal(18, 2)), 6, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (161, CAST(0.10 AS Decimal(18, 2)), 6, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (162, CAST(0.10 AS Decimal(18, 2)), 6, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (163, CAST(0.10 AS Decimal(18, 2)), 6, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (164, CAST(0.10 AS Decimal(18, 2)), 6, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (165, CAST(0.10 AS Decimal(18, 2)), 6, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (166, CAST(0.10 AS Decimal(18, 2)), 6, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (167, CAST(0.15 AS Decimal(18, 2)), 6, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (168, CAST(0.15 AS Decimal(18, 2)), 7, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (169, CAST(0.05 AS Decimal(18, 2)), 7, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (170, CAST(0.05 AS Decimal(18, 2)), 7, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (171, CAST(0.10 AS Decimal(18, 2)), 7, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (172, CAST(0.05 AS Decimal(18, 2)), 7, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (173, CAST(0.05 AS Decimal(18, 2)), 7, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (174, CAST(0.05 AS Decimal(18, 2)), 7, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (175, CAST(0.15 AS Decimal(18, 2)), 7, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (176, CAST(0.10 AS Decimal(18, 2)), 7, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (177, CAST(0.25 AS Decimal(18, 2)), 7, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (178, CAST(0.15 AS Decimal(18, 2)), 8, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (179, CAST(0.05 AS Decimal(18, 2)), 8, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (180, CAST(0.15 AS Decimal(18, 2)), 8, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (181, CAST(0.05 AS Decimal(18, 2)), 8, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (182, CAST(0.05 AS Decimal(18, 2)), 8, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (183, CAST(0.10 AS Decimal(18, 2)), 8, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (184, CAST(0.10 AS Decimal(18, 2)), 8, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (185, CAST(0.10 AS Decimal(18, 2)), 9, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (186, CAST(0.15 AS Decimal(18, 2)), 14, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (187, CAST(0.15 AS Decimal(18, 2)), 16, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (188, CAST(0.05 AS Decimal(18, 2)), 39, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (189, CAST(0.05 AS Decimal(18, 2)), 39, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (190, CAST(0.10 AS Decimal(18, 2)), 39, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (191, CAST(0.10 AS Decimal(18, 2)), 39, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (192, CAST(0.10 AS Decimal(18, 2)), 39, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (193, CAST(0.25 AS Decimal(18, 2)), 39, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (194, CAST(0.10 AS Decimal(18, 2)), 5, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (195, CAST(0.10 AS Decimal(18, 2)), 5, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (196, CAST(0.10 AS Decimal(18, 2)), 5, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (197, CAST(0.10 AS Decimal(18, 2)), 5, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (198, CAST(0.10 AS Decimal(18, 2)), 5, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (199, CAST(0.15 AS Decimal(18, 2)), 5, 4)
GO
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (200, CAST(0.15 AS Decimal(18, 2)), 5, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (201, CAST(0.40 AS Decimal(18, 2)), 4, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (202, CAST(0.10 AS Decimal(18, 2)), 3, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (203, CAST(0.10 AS Decimal(18, 2)), 3, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (204, CAST(0.10 AS Decimal(18, 2)), 3, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (205, CAST(0.10 AS Decimal(18, 2)), 3, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (206, CAST(0.10 AS Decimal(18, 2)), 3, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (207, CAST(0.10 AS Decimal(18, 2)), 3, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (208, CAST(0.10 AS Decimal(18, 2)), 3, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (209, CAST(0.15 AS Decimal(18, 2)), 3, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (210, CAST(0.15 AS Decimal(18, 2)), 3, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (211, CAST(0.40 AS Decimal(18, 2)), 2, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (212, CAST(0.15 AS Decimal(18, 2)), 9, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (213, CAST(0.15 AS Decimal(18, 2)), 9, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (214, CAST(0.15 AS Decimal(18, 2)), 9, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (215, CAST(0.15 AS Decimal(18, 2)), 39, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (216, CAST(0.40 AS Decimal(18, 2)), 15, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (217, CAST(0.05 AS Decimal(18, 2)), 39, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (218, CAST(0.25 AS Decimal(18, 2)), 19, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (219, CAST(0.15 AS Decimal(18, 2)), 16, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (220, CAST(0.10 AS Decimal(18, 2)), 16, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (221, CAST(0.10 AS Decimal(18, 2)), 16, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (222, CAST(0.10 AS Decimal(18, 2)), 16, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (223, CAST(0.10 AS Decimal(18, 2)), 16, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (224, CAST(0.10 AS Decimal(18, 2)), 16, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (225, CAST(0.10 AS Decimal(18, 2)), 16, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (226, CAST(0.10 AS Decimal(18, 2)), 16, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (227, CAST(0.10 AS Decimal(18, 2)), 17, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (228, CAST(0.10 AS Decimal(18, 2)), 17, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (229, CAST(0.05 AS Decimal(18, 2)), 17, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (230, CAST(0.10 AS Decimal(18, 2)), 17, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (231, CAST(0.10 AS Decimal(18, 2)), 17, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (232, CAST(0.10 AS Decimal(18, 2)), 17, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (233, CAST(0.10 AS Decimal(18, 2)), 17, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (234, CAST(0.10 AS Decimal(18, 2)), 17, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (235, CAST(0.10 AS Decimal(18, 2)), 17, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (236, CAST(0.15 AS Decimal(18, 2)), 17, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (237, CAST(0.15 AS Decimal(18, 2)), 19, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (238, CAST(0.05 AS Decimal(18, 2)), 19, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (239, CAST(0.05 AS Decimal(18, 2)), 19, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (240, CAST(0.10 AS Decimal(18, 2)), 19, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (241, CAST(0.05 AS Decimal(18, 2)), 19, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (242, CAST(0.05 AS Decimal(18, 2)), 19, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (243, CAST(0.05 AS Decimal(18, 2)), 19, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (244, CAST(0.15 AS Decimal(18, 2)), 19, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (245, CAST(0.10 AS Decimal(18, 2)), 19, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (246, CAST(0.15 AS Decimal(18, 2)), 39, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (247, CAST(0.40 AS Decimal(18, 2)), 1, 2)
SET IDENTITY_INSERT [dbo].[CriteriaWeights] OFF
SET IDENTITY_INSERT [dbo].[Cycles] ON 

INSERT [dbo].[Cycles] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Date], [Month], [Description]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'H2', 1, 7, N'0')
INSERT [dbo].[Cycles] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Date], [Month], [Description]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'H1', 7, 1, N'0')
SET IDENTITY_INSERT [dbo].[Cycles] OFF
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'25bf9479-de7f-4a80-910b-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'c1bb47aa-0806-4703-910c-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'1286b7da-fe5a-4bf0-910d-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'cbc0e424-1335-4c4b-910e-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'6fa52aa6-3323-4123-910f-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'19014ec4-831f-422f-9110-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'955139f3-dd30-48f7-9111-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'5447a792-5bc6-496f-9112-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'0542f8b1-41ea-4673-9113-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'4ca263a8-0dff-468e-9114-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'3de69b42-6ef2-4cda-9115-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'2bc5e797-0b6c-4ec9-9116-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'7452571e-3afb-4a0f-9117-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'06bbb8db-7d08-4111-9118-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'6b6b4614-49e5-4403-9119-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'0a43de53-6cfd-4734-911a-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'0148acef-4f0b-48b8-911b-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'20cff03b-d7d7-4324-911c-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'e0f4de62-eddd-4eaf-911d-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'70bb290f-2457-4235-911e-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'64eeab46-57e0-432f-911f-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'635a5dbc-a07d-49c1-9120-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'88f5d552-f1c8-4b7b-9121-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'd63f717a-93c0-49b9-9122-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'149bb922-aa87-4813-9123-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'db522d4b-17af-4e62-9124-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'25e4cbf0-2883-4bcf-9125-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'c4f46c32-b3a6-4a66-9126-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'765683d2-b921-4045-9127-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'c35c2ed4-735c-45d0-9128-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'5e0e30db-efbe-49e5-9129-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'57d36636-240b-4486-912a-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'c9f0d40e-d7c2-4d9b-912b-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'd6bac481-8c3b-4e60-912c-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'a36f69f7-8bf4-48ee-912d-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'2c293448-e80b-4422-912e-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'd6e04232-c012-4daa-912f-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'9b924835-d7f5-4237-9130-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'6c4bebf4-2b55-4594-9131-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'66eba3eb-d5db-4492-9132-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'446c83b7-18f1-4287-9133-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'ed1484c0-b486-4b45-9134-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'4914782e-7977-4a4b-9135-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'2cd3be0f-574e-4b65-9136-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'449a201c-d7f8-444f-9137-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'fd9f3bfa-e242-4a9a-9138-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'111cac25-bfc6-4d59-9139-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'bdc0ccf3-6445-4410-913a-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'd8cff38f-7720-495e-913b-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'8b957f3e-271a-464a-913c-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'c454129b-c15f-4e10-913d-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'dda9b8f2-add4-4623-913e-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'09b00738-43fc-4350-913f-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'aa9d4615-66bc-42ae-9140-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'7f880231-b651-4754-9141-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'60f8b54e-b78e-416d-9142-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'6978662c-9f4c-4583-9143-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'423e5a4b-dbd8-4af2-9144-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'1085c524-8819-410b-9145-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'27183494-56f9-42fd-9146-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'269d195a-05bf-4216-9147-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'354561ca-6d0a-460a-9148-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'5381b840-3921-43a7-9149-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'78c8db3b-63b5-4a08-914a-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'40ab54a2-4d60-4d6f-914b-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'2299043c-0cbd-4ac9-914c-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'5cd5091b-1af6-40f1-914d-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'e5e746dd-fd75-47f1-914e-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'10f2b7af-6311-4eec-914f-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'ebe6e355-10a0-40e3-9150-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'dc6295a1-04ec-4621-9151-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'5551c1c1-022e-4536-9152-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'46d977ba-cc06-4ea8-9153-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'9739b491-fd5a-4a17-9154-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'02be4790-9e7e-4f05-9155-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'5f77baed-fa44-4274-9156-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'4510d057-2e39-40c7-9157-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'06bd9f0a-e4a4-4741-9158-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'32bb905d-40ad-4ac4-9159-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'60973b22-8044-4ee1-915a-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'7b994a40-d204-4d3b-915b-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'dff5206b-fda5-4fa0-915c-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'd533c687-d8bb-4c76-915d-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'33b0662b-4f16-43d8-915e-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'7d5953a5-a7cb-4341-915f-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'921936cb-caa8-4a79-9160-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'c3b73fee-677e-4ab2-9161-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'8a5b02b4-ec75-4909-9162-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'27703a2b-4ce0-4837-9163-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'452672a3-8463-45e7-9164-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'f3fe3ccd-f0a6-4cd8-9165-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'9546aad9-263e-4f92-9166-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'31ea8087-d29d-4202-9167-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'c004af73-cf67-4d80-9168-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'8b79c87d-0e1e-4ca9-9169-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'4e2ddeae-a9a8-4498-916a-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'c83ee880-12f0-42d3-916b-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'0db9cd05-55b5-4049-916c-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'83d9dd3e-5283-4167-916d-08d7301c24a5', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'2eb36971-47e4-4ec5-bcee-21283b2388c7', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'02cea9a3-c47a-4ea1-bb96-2adc402e3850', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[Goals] ON 

INSERT [dbo].[Goals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [NextGoals], [UserId], [LastGoals]) VALUES (1, CAST(N'2019-09-11T14:43:01.9845074' AS DateTime2), N'02cea9a3-c47a-4ea1-bb96-2adc402e3850', NULL, NULL, 0, N'FBI', N'02cea9a3-c47a-4ea1-bb96-2adc402e3850', N'API')
SET IDENTITY_INSERT [dbo].[Goals] OFF
SET IDENTITY_INSERT [dbo].[LevelCriteriaGroups] ON 

INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (1, CAST(0.30 AS Decimal(18, 2)), 26, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (2, CAST(0.30 AS Decimal(18, 2)), 11, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (3, CAST(0.70 AS Decimal(18, 2)), 11, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (4, CAST(0.30 AS Decimal(18, 2)), 10, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (5, CAST(0.70 AS Decimal(18, 2)), 10, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (6, CAST(1.00 AS Decimal(18, 2)), 9, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (7, CAST(1.00 AS Decimal(18, 2)), 8, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (8, CAST(1.00 AS Decimal(18, 2)), 7, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (9, CAST(1.00 AS Decimal(18, 2)), 12, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (10, CAST(0.30 AS Decimal(18, 2)), 6, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (11, CAST(0.30 AS Decimal(18, 2)), 5, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (12, CAST(0.70 AS Decimal(18, 2)), 5, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (13, CAST(0.30 AS Decimal(18, 2)), 4, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (14, CAST(0.70 AS Decimal(18, 2)), 4, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (15, CAST(0.35 AS Decimal(18, 2)), 3, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (16, CAST(0.65 AS Decimal(18, 2)), 3, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (17, CAST(1.00 AS Decimal(18, 2)), 2, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (18, CAST(0.70 AS Decimal(18, 2)), 6, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (19, CAST(1.00 AS Decimal(18, 2)), 27, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (20, CAST(0.70 AS Decimal(18, 2)), 13, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (21, CAST(0.70 AS Decimal(18, 2)), 14, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (22, CAST(0.70 AS Decimal(18, 2)), 26, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (23, CAST(1.00 AS Decimal(18, 2)), 25, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (24, CAST(1.00 AS Decimal(18, 2)), 24, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (25, CAST(1.00 AS Decimal(18, 2)), 23, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (26, CAST(1.00 AS Decimal(18, 2)), 22, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (27, CAST(0.30 AS Decimal(18, 2)), 21, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (28, CAST(0.70 AS Decimal(18, 2)), 21, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (29, CAST(0.30 AS Decimal(18, 2)), 13, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (30, CAST(1.00 AS Decimal(18, 2)), 20, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (31, CAST(1.00 AS Decimal(18, 2)), 18, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (32, CAST(1.00 AS Decimal(18, 2)), 17, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (33, CAST(0.30 AS Decimal(18, 2)), 16, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (34, CAST(0.70 AS Decimal(18, 2)), 16, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (35, CAST(0.30 AS Decimal(18, 2)), 15, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (36, CAST(0.70 AS Decimal(18, 2)), 15, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (37, CAST(0.30 AS Decimal(18, 2)), 14, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (38, CAST(1.00 AS Decimal(18, 2)), 19, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (39, CAST(1.00 AS Decimal(18, 2)), 28, 2)
SET IDENTITY_INSERT [dbo].[LevelCriteriaGroups] OFF
SET IDENTITY_INSERT [dbo].[Levels] ON 

INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (1, N'Chief Executive Officer', N'CEO')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (2, N'Senior Software Engineer', N'SSE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (3, N'Technical Leader', N'TL')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (4, N'Technical Manager', N'TM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (5, N'Software Manager', N'SM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (6, N'Solution Architecture', N'SA')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (7, N'Associate Test Engineer', N'ATS')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (8, N'Test Engineer', N'TS')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (9, N'Senior Testing Engineer', N'STS')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (10, N'QA Lead', N'QAL')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (11, N'Test Lead', N'TSL')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (12, N'Associate Project Assistant', N'APA')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (13, N'Associate Project Manager', N'APM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (14, N'Project Manager', N'PM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (15, N'Senior Project Manager', N'SPM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (16, N'Delivery Manager', N'DM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (17, N'UX-UI designer', N'UXD')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (18, N'Business Analyst ', N'BA')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (19, N'Office Manager', N'OM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (20, N'Accountant ', N'ACC')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (21, N'Accounting Manager', N'ACM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (22, N'IT Manager', N'ITM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (23, N'Associate HR Executive', N'AHE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (24, N'HR Executive', N'HE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (25, N'Senior HR Executive', N'SHE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (26, N'HR Manager', N'HMR')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (27, N'Software Engineer', N'SE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (28, N'Associate Software Engineer', N'ASE')
SET IDENTITY_INSERT [dbo].[Levels] OFF
SET IDENTITY_INSERT [dbo].[Objectives] ON 

INSERT [dbo].[Objectives] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [NextObjectives], [Plan], [TlComment], [PmComment], [UserId], [LastObjectives]) VALUES (1, CAST(N'2019-09-11T14:09:27.8630282' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, N'string', N'string', NULL, NULL, N'02cea9a3-c47a-4ea1-bb96-2adc402e3850', N'hihi')
SET IDENTITY_INSERT [dbo].[Objectives] OFF
SET IDENTITY_INSERT [dbo].[PerformanceReviewCycles] ON 

INSERT [dbo].[PerformanceReviewCycles] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Year], [Description], [CycleId], [Status], [AppraisalModeId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'00000000-0000-0000-0000-000000000000', 0, N'N1', 2019, N'No Des', 1, 20, 1)
SET IDENTITY_INSERT [dbo].[PerformanceReviewCycles] OFF
SET IDENTITY_INSERT [dbo].[Projects] ON 

INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'General - SUP', N'PRJSUP', 6)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'General - Adj', N'PRJADJ', 5)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'General - LOG', N'PRJLOG', 1)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'General - App', N'PRJAPP', 2)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'General - Amb', N'PRJAMB', 3)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'General - F4', N'PRJF4', 4)
SET IDENTITY_INSERT [dbo].[Projects] OFF
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (2, 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (2, 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (2, 3, CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), 1)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (5, 1, CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), CAST(N'2019-09-13T16:04:58.2300000' AS DateTime2), 1)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (5, 2, CAST(N'2019-09-13T16:04:58.2300000' AS DateTime2), CAST(N'2019-09-15T16:04:58.2300000' AS DateTime2), 1)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (5, 3, CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), 1)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (7, 1, CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), CAST(N'2019-09-13T16:04:58.2300000' AS DateTime2), 1)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (7, 2, CAST(N'2019-09-13T16:04:58.2300000' AS DateTime2), CAST(N'2019-09-15T16:04:58.2300000' AS DateTime2), 1)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Active]) VALUES (7, 3, CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), CAST(N'2019-09-11T16:04:58.2300000' AS DateTime2), 1)
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([Id], [Name]) VALUES (1, N'TL')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (2, N'PM')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (3, N'DM')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (4, N'CEO')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (5, N'HR')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (6, N'System Admin')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (7, N'Employee')
SET IDENTITY_INSERT [dbo].[Roles] OFF
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (1, N'Self rating', NULL, 3)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (2, N'TL review', NULL, 6)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (3, N'PM review', NULL, 6)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (4, N'DM review ', NULL, 3)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (5, N'HR consolidate', NULL, 7)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (6, N'CEO', NULL, 3)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (7, N'Sharing', NULL, 10)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (8, N'Employee Sign-off', NULL, 5)
SET IDENTITY_INSERT [dbo].[Teams] ON 

INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Logistics', N'LOG', 2)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'AppDev', N'APP', 1)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Amblique', N'AMB', 1)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'FactFour', N'F4', 1)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Adjuno', N'ADJ', 3)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, N'00000000-0000-0000-0000-000000000000', 0, N'Supporting', N'SUP', 4)
SET IDENTITY_INSERT [dbo].[Teams] OFF
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'310dff0a-208a-49f2-910a-08d7301c24a5', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'25bf9479-de7f-4a80-910b-08d7301c24a5', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c1bb47aa-0806-4703-910c-08d7301c24a5', 4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'1286b7da-fe5a-4bf0-910d-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'cbc0e424-1335-4c4b-910e-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'6fa52aa6-3323-4123-910f-08d7301c24a5', 9, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'19014ec4-831f-422f-9110-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'955139f3-dd30-48f7-9111-08d7301c24a5', 9, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'5447a792-5bc6-496f-9112-08d7301c24a5', 13, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'0542f8b1-41ea-4673-9113-08d7301c24a5', 9, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'4ca263a8-0dff-468e-9114-08d7301c24a5', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'3de69b42-6ef2-4cda-9115-08d7301c24a5', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2bc5e797-0b6c-4ec9-9116-08d7301c24a5', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'7452571e-3afb-4a0f-9117-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'06bbb8db-7d08-4111-9118-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'6b6b4614-49e5-4403-9119-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'0a43de53-6cfd-4734-911a-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'0148acef-4f0b-48b8-911b-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'20cff03b-d7d7-4324-911c-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'e0f4de62-eddd-4eaf-911d-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'70bb290f-2457-4235-911e-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'64eeab46-57e0-432f-911f-08d7301c24a5', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'635a5dbc-a07d-49c1-9120-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'88f5d552-f1c8-4b7b-9121-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd63f717a-93c0-49b9-9122-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'149bb922-aa87-4813-9123-08d7301c24a5', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'db522d4b-17af-4e62-9124-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'25e4cbf0-2883-4bcf-9125-08d7301c24a5', 4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c4f46c32-b3a6-4a66-9126-08d7301c24a5', 10, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'765683d2-b921-4045-9127-08d7301c24a5', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c35c2ed4-735c-45d0-9128-08d7301c24a5', 16, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'5e0e30db-efbe-49e5-9129-08d7301c24a5', 19, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'57d36636-240b-4486-912a-08d7301c24a5', 26, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c9f0d40e-d7c2-4d9b-912b-08d7301c24a5', 21, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd6bac481-8c3b-4e60-912c-08d7301c24a5', 22, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'a36f69f7-8bf4-48ee-912d-08d7301c24a5', 25, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2c293448-e80b-4422-912e-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd6e04232-c012-4daa-912f-08d7301c24a5', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'9b924835-d7f5-4237-9130-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'6c4bebf4-2b55-4594-9131-08d7301c24a5', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'66eba3eb-d5db-4492-9132-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'446c83b7-18f1-4287-9133-08d7301c24a5', 16, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'ed1484c0-b486-4b45-9134-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'4914782e-7977-4a4b-9135-08d7301c24a5', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2cd3be0f-574e-4b65-9136-08d7301c24a5', 5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'449a201c-d7f8-444f-9137-08d7301c24a5', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'fd9f3bfa-e242-4a9a-9138-08d7301c24a5', 11, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'111cac25-bfc6-4d59-9139-08d7301c24a5', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'bdc0ccf3-6445-4410-913a-08d7301c24a5', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd8cff38f-7720-495e-913b-08d7301c24a5', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'8b957f3e-271a-464a-913c-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c454129b-c15f-4e10-913d-08d7301c24a5', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'dda9b8f2-add4-4623-913e-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'09b00738-43fc-4350-913f-08d7301c24a5', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'aa9d4615-66bc-42ae-9140-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'7f880231-b651-4754-9141-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'60f8b54e-b78e-416d-9142-08d7301c24a5', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'6978662c-9f4c-4583-9143-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'423e5a4b-dbd8-4af2-9144-08d7301c24a5', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'1085c524-8819-410b-9145-08d7301c24a5', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'27183494-56f9-42fd-9146-08d7301c24a5', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'269d195a-05bf-4216-9147-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'354561ca-6d0a-460a-9148-08d7301c24a5', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'5381b840-3921-43a7-9149-08d7301c24a5', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'78c8db3b-63b5-4a08-914a-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'40ab54a2-4d60-4d6f-914b-08d7301c24a5', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2299043c-0cbd-4ac9-914c-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'5cd5091b-1af6-40f1-914d-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'e5e746dd-fd75-47f1-914e-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'10f2b7af-6311-4eec-914f-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'ebe6e355-10a0-40e3-9150-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'dc6295a1-04ec-4621-9151-08d7301c24a5', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'5551c1c1-022e-4536-9152-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'46d977ba-cc06-4ea8-9153-08d7301c24a5', 17, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'9739b491-fd5a-4a17-9154-08d7301c24a5', 20, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'02be4790-9e7e-4f05-9155-08d7301c24a5', 12, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'5f77baed-fa44-4274-9156-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'4510d057-2e39-40c7-9157-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'06bd9f0a-e4a4-4741-9158-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'32bb905d-40ad-4ac4-9159-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'60973b22-8044-4ee1-915a-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'7b994a40-d204-4d3b-915b-08d7301c24a5', 23, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'dff5206b-fda5-4fa0-915c-08d7301c24a5', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd533c687-d8bb-4c76-915d-08d7301c24a5', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'33b0662b-4f16-43d8-915e-08d7301c24a5', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'7d5953a5-a7cb-4341-915f-08d7301c24a5', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'921936cb-caa8-4a79-9160-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c3b73fee-677e-4ab2-9161-08d7301c24a5', 18, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'8a5b02b4-ec75-4909-9162-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'27703a2b-4ce0-4837-9163-08d7301c24a5', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'452672a3-8463-45e7-9164-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'f3fe3ccd-f0a6-4cd8-9165-08d7301c24a5', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'9546aad9-263e-4f92-9166-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'31ea8087-d29d-4202-9167-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c004af73-cf67-4d80-9168-08d7301c24a5', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'8b79c87d-0e1e-4ca9-9169-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'4e2ddeae-a9a8-4498-916a-08d7301c24a5', 24, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c83ee880-12f0-42d3-916b-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'0db9cd05-55b5-4049-916c-08d7301c24a5', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'83d9dd3e-5283-4167-916d-08d7301c24a5', 8, CAST(N'2019-04-25T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2eb36971-47e4-4ec5-bcee-21283b2388c7', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'02cea9a3-c47a-4ea1-bb96-2adc402e3850', 4, CAST(N'2019-04-25T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'02cea9a3-c47a-4ea1-bb96-2adc402e3850', 28, CAST(N'2018-04-25T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserRoles] ([UserId], [RoleId]) VALUES (N'2eb36971-47e4-4ec5-bcee-21283b2388c7', 1)
INSERT [dbo].[UserRoles] ([UserId], [RoleId]) VALUES (N'02cea9a3-c47a-4ea1-bb96-2adc402e3850', 7)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'310dff0a-208a-49f2-910a-08d7301c24a5', N'G10000', N'matt@groovetechnology.com', N'Matt Long', N'Long', N'Matt', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'25bf9479-de7f-4a80-910b-08d7301c24a5', N'G00001', N'tan.truong@groovetechnology.com', N'Tan Truong', N'Truong', N'Tan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c1bb47aa-0806-4703-910c-08d7301c24a5', N'G00002', N'thuan.ha@groovetechnology.com', N'Thuan Ha', N'Ha', N'Thuan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'1286b7da-fe5a-4bf0-910d-08d7301c24a5', N'G00004', N'tuan.hoang@groovetechnology.com', N'Tuan Hoang', N'Hoang', N'Tuan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-18T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'cbc0e424-1335-4c4b-910e-08d7301c24a5', N'G00005', N'tho.lai@groovetechnology.com', N'Tho Lai', N'Lai', N'Tho', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-18T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'6fa52aa6-3323-4123-910f-08d7301c24a5', N'G00007', N'hanh.tran@groovetechnology.com', N'Hanh Tran', N'Tran', N'Hanh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'19014ec4-831f-422f-9110-08d7301c24a5', N'G00008', N'an.ha@groovetechnology.com', N'An Ha', N'Ha', N'An', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'955139f3-dd30-48f7-9111-08d7301c24a5', N'G00010', N'duy.nguyen@groovetechnology.com', N'Duy Nguyen', N'Nguyen', N'Duy', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'5447a792-5bc6-496f-9112-08d7301c24a5', N'G00012', N'my.trinh@groovetechnology.com', N'My Trinh', N'Trinh', N'My', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'0542f8b1-41ea-4673-9113-08d7301c24a5', N'G00013', N'hai.nguyen@groovetechnology.com', N'Hai Nguyen', N'Nguyen', N'Hai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'4ca263a8-0dff-468e-9114-08d7301c24a5', N'G00014', N'sanh.nguyen@groovetechnology.com', N'Sanh Nguyen', N'Nguyen', N'Sanh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'3de69b42-6ef2-4cda-9115-08d7301c24a5', N'G00015', N'loan.ngo@groovetechnology.com', N'Loan Ngo', N'Ngo', N'Loan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2bc5e797-0b6c-4ec9-9116-08d7301c24a5', N'G00016', N'duong.tiet@groovetechnology.com', N'Duong Tiet', N'Tiet', N'Duong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'7452571e-3afb-4a0f-9117-08d7301c24a5', N'G00017', N'thanh.pham@groovetechnology.com', N'Thanh Pham', N'Pham', N'Thanh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'06bbb8db-7d08-4111-9118-08d7301c24a5', N'G00018', N'van.tran@groovetechnology.com', N'Van Tran', N'Tran', N'Van', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'6b6b4614-49e5-4403-9119-08d7301c24a5', N'G00019', N'tri.nguyen@groovetechnology.com', N'Tri Nguyen', N'Nguyen', N'Tri', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'0a43de53-6cfd-4734-911a-08d7301c24a5', N'G00020', N'luong.pham@groovetechnology.com', N'Luong Pham', N'Pham', N'Luong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'0148acef-4f0b-48b8-911b-08d7301c24a5', N'G00021', N'duc.quach@groovetechnology.com', N'Duc Quach', N'Quach', N'Duc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'20cff03b-d7d7-4324-911c-08d7301c24a5', N'G00022', N'dung.nguyen@groovetechnology.com', N'Dung Nguyen', N'Nguyen', N'Dung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'e0f4de62-eddd-4eaf-911d-08d7301c24a5', N'G00023', N'dung.le@groovetechnology.com', N'Dung Le', N'Le', N'Dung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'70bb290f-2457-4235-911e-08d7301c24a5', N'G00024', N'phung.tran@groovetechnology.com', N'Phung Tran', N'Tran', N'Phung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'64eeab46-57e0-432f-911f-08d7301c24a5', N'G00025', N'hieu.le@groovetechnology.com', N'Hieu Le', N'Le', N'Hieu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'635a5dbc-a07d-49c1-9120-08d7301c24a5', N'G00027', N'dat.phung@groovetechnology.com', N'Dat Phung', N'Phung', N'Dat', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'88f5d552-f1c8-4b7b-9121-08d7301c24a5', N'G00028', N'phu.phung@groovetechnology.com', N'Phu Phung', N'Phung', N'Phu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-22T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd63f717a-93c0-49b9-9122-08d7301c24a5', N'G00029', N'binh.doan@groovetechnology.com', N'Binh Doan', N'Doan', N'Binh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-22T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'149bb922-aa87-4813-9123-08d7301c24a5', N'G00030', N'thin.nguyen@groovetechnology.com', N'Thin Nguyen', N'Nguyen', N'Thin', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-22T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'db522d4b-17af-4e62-9124-08d7301c24a5', N'G00032', N'huy.luu@groovetechnology.com', N'Huy Luu', N'Luu', N'Huy', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-22T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'25e4cbf0-2883-4bcf-9125-08d7301c24a5', N'G00033', N'linh.vu@groovetechnology.com', N'Linh Vu', N'Vu', N'Linh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c4f46c32-b3a6-4a66-9126-08d7301c24a5', N'G00034', N'chung.ly@groovetechnology.com', N'Chung Ly', N'Ly', N'Chung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'765683d2-b921-4045-9127-08d7301c24a5', N'G00037', N'tuyen.nguyen@groovetechnology.com', N'Tuyen Nguyen', N'Nguyen', N'Tuyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-24T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c35c2ed4-735c-45d0-9128-08d7301c24a5', N'G00040', N'victor.tran@groovetechnology.com', N'Viet Tran', N'Tran', N'Viet', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-12-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'5e0e30db-efbe-49e5-9129-08d7301c24a5', N'G00041', N'lien.chung@groovetechnology.com', N'Lien Chung', N'Chung', N'Lien', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-12-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'57d36636-240b-4486-912a-08d7301c24a5', N'G00042', N'mai.duong@groovetechnology.com', N'Mai Duong', N'Duong', N'Mai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c9f0d40e-d7c2-4d9b-912b-08d7301c24a5', N'G00043', N'phuoc.hoang@groovetechnology.com', N'Phuoc Hoang', N'Hoang', N'Phuoc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd6bac481-8c3b-4e60-912c-08d7301c24a5', N'G00044', N'ba.nguyen@groovetechnology.com', N'Ba Nguyen', N'Nguyen', N'Ba', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'a36f69f7-8bf4-48ee-912d-08d7301c24a5', N'G00045', N'anh.nguyen@groovetechnology.com', N'Anh Nguyen', N'Nguyen', N'Anh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-09T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2c293448-e80b-4422-912e-08d7301c24a5', N'G00046', N'tin.truong@groovetechnology.com', N'Tin Truong', N'Truong', N'Tin', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-16T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd6e04232-c012-4daa-912f-08d7301c24a5', N'G00049', N'hieu.tran@groovetechnology.com', N'Hieu Tran', N'Tran', N'Hieu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'9b924835-d7f5-4237-9130-08d7301c24a5', N'G00051', N'huyen.nguyent@groovetechnology.com', N'Huyen Nguyen', N'Nguyen', N'Huyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'6c4bebf4-2b55-4594-9131-08d7301c24a5', N'G00052', N'truc.ha@groovetechnology.com', N'Truc Ha', N'Ha', N'Truc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'66eba3eb-d5db-4492-9132-08d7301c24a5', N'G00053', N'mai.nguyen@groovetechnology.com', N'Mai Nguyen', N'Nguyen', N'Mai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-03-06T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'446c83b7-18f1-4287-9133-08d7301c24a5', N'G00055', N'thao.nguyen@groovetechnology.com', N'Thao Nguyen', N'Nguyen', N'Thao', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-03-15T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'ed1484c0-b486-4b45-9134-08d7301c24a5', N'G00056', N'tuan.nguyen@groovetechnology.com', N'Tuan Nguyen', N'Nguyen', N'Tuan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-03-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'4914782e-7977-4a4b-9135-08d7301c24a5', N'G00058', N'hai.le@groovetechnology.com', N'Hai Le', N'Le', N'Hai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-03-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2cd3be0f-574e-4b65-9136-08d7301c24a5', N'G00059', N'tri.le@groovetechnology.com', N'Tri Le', N'Le', N'Tri', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-04-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'449a201c-d7f8-444f-9137-08d7301c24a5', N'G00063', N'nhi.truong@groovetechnology.com', N'Nhi Truong', N'Truong', N'Nhi', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-04-10T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'fd9f3bfa-e242-4a9a-9138-08d7301c24a5', N'G00064', N'mai.nguyentn@groovetechnology.com', N'Mai Nguyen', N'Nguyen', N'Mai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-04-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'111cac25-bfc6-4d59-9139-08d7301c24a5', N'G00066', N'khang.tran@groovetechnology.com', N'Khang Tran', N'Tran', N'Khang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-04-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'bdc0ccf3-6445-4410-913a-08d7301c24a5', N'G00069', N'dinh.vu@groovetechnology.com', N'Dinh Vu', N'Vu', N'Dinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-05-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd8cff38f-7720-495e-913b-08d7301c24a5', N'G00070', N'huy.do@groovetechnology.com', N'Huy Do', N'Do', N'Huy', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-05-08T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'8b957f3e-271a-464a-913c-08d7301c24a5', N'G00071', N'hung.nguyen@groovetechnology.com', N'Hung Nguyen', N'Nguyen', N'Hung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-07-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c454129b-c15f-4e10-913d-08d7301c24a5', N'G00072', N'hung.doan@groovetechnology.com', N'Hung Doan', N'Doan', N'Hung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-08-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'dda9b8f2-add4-4623-913e-08d7301c24a5', N'G00074', N'thinh.vo@groovetechnology.com', N'Thinh Vo', N'Vo', N'Thinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-09-05T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'09b00738-43fc-4350-913f-08d7301c24a5', N'G00079', N'dung.nguyentm@groovetechnology.com', N'Dung Nguyen', N'Nguyen', N'Dung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-10-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'aa9d4615-66bc-42ae-9140-08d7301c24a5', N'G00083', N'vu.nguyen@groovetechnology.com', N'Vu Nguyen', N'Nguyen', N'Vu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-10-16T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'7f880231-b651-4754-9141-08d7301c24a5', N'G00084', N'vinh.trang@groovetechnology.com', N'Vinh Trang', N'Trang', N'Vinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-11-06T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'60f8b54e-b78e-416d-9142-08d7301c24a5', N'G00087', N'trinh.nguyen@groovetechnology.com', N'Trinh Nguyen', N'Nguyen', N'Trinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-11-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'6978662c-9f4c-4583-9143-08d7301c24a5', N'G00088', N'vinh.trinh@groovetechnology.com', N'Vinh Trinh', N'Trinh', N'Vinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-11-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'423e5a4b-dbd8-4af2-9144-08d7301c24a5', N'G00090', N'phuong.nguyen@groovetechnology.com', N'Phuong Nguyen', N'Nguyen', N'Phuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-01-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'1085c524-8819-410b-9145-08d7301c24a5', N'G00093', N'dung.trann@groovetechnology.com', N'Dung Tran', N'Tran', N'Dung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-02-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'27183494-56f9-42fd-9146-08d7301c24a5', N'G00095', N'hai.hoang@groovetechnology.com', N'Hai Hoang', N'Hoang', N'Hai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-02-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'269d195a-05bf-4216-9147-08d7301c24a5', N'G00098', N'vuong.do@groovetechnology.com', N'Vuong Do', N'Do', N'Vuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-03-05T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'354561ca-6d0a-460a-9148-08d7301c24a5', N'G00099', N'nam.ngo@groovetechnology.com', N'Nam Ngo', N'Ngo', N'Nam', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-03-05T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'5381b840-3921-43a7-9149-08d7301c24a5', N'G00100', N'ngan.luong@groovetechnology.com', N'Ngan Luong', N'Luong', N'Ngan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-03-19T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'78c8db3b-63b5-4a08-914a-08d7301c24a5', N'G00103', N'chuong.le@groovetechnology.com', N'Chuong Le', N'Le', N'Chuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-04-09T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'40ab54a2-4d60-4d6f-914b-08d7301c24a5', N'G00108', N'minh.vu@groovetechnology.com', N'Minh Vu', N'Vu', N'Minh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-05-15T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2299043c-0cbd-4ac9-914c-08d7301c24a5', N'G00109', N'cuong.phan@groovetechnology.com', N'Cuong Phan', N'Phan', N'Cuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-04T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'5cd5091b-1af6-40f1-914d-08d7301c24a5', N'G00110', N'khoi.nguyen@groovetechnology.com', N'Khoi Nguyen', N'Nguyen', N'Khoi', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-04T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'e5e746dd-fd75-47f1-914e-08d7301c24a5', N'G00111', N'thai.nguyen@groovetechnology.com', N'Thai Nguyen', N'Nguyen', N'Thai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-04T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'10f2b7af-6311-4eec-914f-08d7301c24a5', N'G00113', N'tin.pham@groovetechnology.com', N'Tin Pham', N'Pham', N'Tin', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-25T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'ebe6e355-10a0-40e3-9150-08d7301c24a5', N'G00114', N'van.tang@groovetechnology.com', N'Van Tang', N'Tang', N'Van', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-25T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'dc6295a1-04ec-4621-9151-08d7301c24a5', N'G00116', N'truc.luc@groovetechnology.com', N'Truc Luc', N'Luc', N'Truc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-07-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'5551c1c1-022e-4536-9152-08d7301c24a5', N'G00117', N'phuong.nguyenduy@groovetechnology.com', N'Phuong Nguyen', N'Nguyen', N'Phuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-07-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'46d977ba-cc06-4ea8-9153-08d7301c24a5', N'G00121', N'hoa.nguyen@groovetechnology.com', N'Hoa Nguyen', N'Nguyen', N'Hoa', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-07-16T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'9739b491-fd5a-4a17-9154-08d7301c24a5', N'G00122', N'cuc.nguyen@groovetechnology.com', N'Cuc Nguyen', N'Nguyen', N'Cuc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-07-16T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'02be4790-9e7e-4f05-9155-08d7301c24a5', N'G00123', N'quynh.ho@groovetechnology.com', N'Quynh Ho', N'Ho', N'Quynh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-08-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'5f77baed-fa44-4274-9156-08d7301c24a5', N'G00124', N'cuong.duong@groovetechnology.com', N'Cuong Duong', N'Duong', N'Cuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-08-06T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'4510d057-2e39-40c7-9157-08d7301c24a5', N'G00126', N'thien.nguyen@groovetechnology.com', N'Thien Nguyen', N'Nguyen', N'Thien', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-08-13T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'06bd9f0a-e4a4-4741-9158-08d7301c24a5', N'G00127', N'tuan.thai@groovetechnology.com', N'Tuan Thai', N'Thai', N'Tuan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-08-13T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'32bb905d-40ad-4ac4-9159-08d7301c24a5', N'G00129', N'thanh.vong@groovetechnology.com', N'Thanh Vong', N'Vong', N'Thanh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-09-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'60973b22-8044-4ee1-915a-08d7301c24a5', N'G00130', N'trung.nguyen@groovetechnology.com', N'Trung Nguyen', N'Nguyen', N'Trung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-08T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'7b994a40-d204-4d3b-915b-08d7301c24a5', N'G00132', N'thuy.nguyenlm@groovetechnology.com', N'Thuy Nguyen', N'Nguyen', N'Thuy', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'dff5206b-fda5-4fa0-915c-08d7301c24a5', N'G00133', N'duc.doan@groovetechnology.com', N'Duc Doan', N'Doan', N'Duc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd533c687-d8bb-4c76-915d-08d7301c24a5', N'G00134', N'vu.nguyenhuy@groovetechnology.com', N'Vu Nguyen Huy', N'Nguyen Huy', N'Vu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'33b0662b-4f16-43d8-915e-08d7301c24a5', N'G00135', N'dinh.nguyen@groovetechnology.com', N'Dinh Nguyen', N'Nguyen', N'Dinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'7d5953a5-a7cb-4341-915f-08d7301c24a5', N'G00136', N'tan.trinh@groovetechnology.com', N'Tan Trinh', N'Trinh', N'Tan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'921936cb-caa8-4a79-9160-08d7301c24a5', N'G00140', N'cuong.tran@groovetechnology.com', N'Cuong Tran', N'Tran', N'Cuong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-11-12T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c3b73fee-677e-4ab2-9161-08d7301c24a5', N'G00142', N'an.nguyen@groovetechnology.com', N'An Nguyen', N'Nguyen', N'An', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-11-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'8a5b02b4-ec75-4909-9162-08d7301c24a5', N'G00143', N'thang.nguyen@groovetechnology.com', N'Thang Nguyen', N'Nguyen', N'Thang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-11-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'27703a2b-4ce0-4837-9163-08d7301c24a5', N'G00144', N'tam.hua@groovetechnology.com', N'Tam Hua', N'Hua', N'Tam', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-12-24T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'452672a3-8463-45e7-9164-08d7301c24a5', N'G00146', N'an.ho@groovetechnology.com', N'An Ho', N'Ho', N'An', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-01-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'f3fe3ccd-f0a6-4cd8-9165-08d7301c24a5', N'G00147', N'khang.ton@groovetechnology.com', N'Khang Ton', N'Ton', N'Khang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-01-28T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'9546aad9-263e-4f92-9166-08d7301c24a5', N'G00157', N'hoang.ha@groovetechnology.com', N'Hoang Ha', N'Ha', N'Hoang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-02-18T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'31ea8087-d29d-4202-9167-08d7301c24a5', N'G00148', N'huy.nguyen@groovetechnology.com', N'Huy Nguyen', N'Nguyen', N'Huy', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c004af73-cf67-4d80-9168-08d7301c24a5', N'G00149', N'bach.nguyen@groovetechnology.com', N'Bach Nguyen', N'Nguyen', N'Bach', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'8b79c87d-0e1e-4ca9-9169-08d7301c24a5', N'G00150', N'thang.tran@groovetechnology.com', N'Thang Tran', N'Tran', N'Thang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-02-25T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'4e2ddeae-a9a8-4498-916a-08d7301c24a5', N'G00151', N'phong.nguyen@groovetechnology.com', N'Phong Nguyen', N'Nguyen', N'Phong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-03-04T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c83ee880-12f0-42d3-916b-08d7301c24a5', N'G00152', N'tuan.truong@groovetechnology.com', N'Tuan Truong', N'Truong', N'Tuan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-04-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'0db9cd05-55b5-4049-916c-08d7301c24a5', N'G00153', N'phuoc.le@groovetechnology.com', N'Phuoc Le', N'Le', N'Phuoc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-04-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'83d9dd3e-5283-4167-916d-08d7301c24a5', N'G00159', N'hoa.pham@groovetechnology.com', N'Hoa Pham', N'Pham', N'Hoa', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-05-20T00:00:00.0000000' AS DateTime2), 1)
GO
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2eb36971-47e4-4ec5-bcee-21283b2388c7', N'G00161', N'truc.phan@groovetechnology.com', N'Truc Phan', N'Phan', N'Truc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-05-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'02cea9a3-c47a-4ea1-bb96-2adc402e3850', N'G00160', N'hien.nguyen@groovetechnology.com', N'Nguyen Doan Hien', N'Nguyen', N'Hien', 1, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-05-20T00:00:00.0000000' AS DateTime2), 1)
ALTER TABLE [dbo].[AppraisalParticipants]  WITH CHECK ADD  CONSTRAINT [FK_AppraisalParticipants_Appraisals_AppraisalId] FOREIGN KEY([AppraisalId])
REFERENCES [dbo].[Appraisals] ([Id])
GO
ALTER TABLE [dbo].[AppraisalParticipants] CHECK CONSTRAINT [FK_AppraisalParticipants_Appraisals_AppraisalId]
GO
ALTER TABLE [dbo].[AppraisalParticipants]  WITH CHECK ADD  CONSTRAINT [FK_AppraisalParticipants_Roles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[AppraisalParticipants] CHECK CONSTRAINT [FK_AppraisalParticipants_Roles_RoleId]
GO
ALTER TABLE [dbo].[AppraisalParticipants]  WITH CHECK ADD  CONSTRAINT [FK_AppraisalParticipants_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[AppraisalParticipants] CHECK CONSTRAINT [FK_AppraisalParticipants_Users_UserId]
GO
ALTER TABLE [dbo].[Appraisals]  WITH CHECK ADD  CONSTRAINT [FK_Appraisals_PerformanceReviewCycles_PerformanceReviewCycleId] FOREIGN KEY([PerformanceReviewCycleId])
REFERENCES [dbo].[PerformanceReviewCycles] ([Id])
GO
ALTER TABLE [dbo].[Appraisals] CHECK CONSTRAINT [FK_Appraisals_PerformanceReviewCycles_PerformanceReviewCycleId]
GO
ALTER TABLE [dbo].[Criterias]  WITH CHECK ADD  CONSTRAINT [FK_Criterias_CriteriaGroups_CriteriaGroupId] FOREIGN KEY([CriteriaGroupId])
REFERENCES [dbo].[CriteriaGroups] ([Id])
GO
ALTER TABLE [dbo].[Criterias] CHECK CONSTRAINT [FK_Criterias_CriteriaGroups_CriteriaGroupId]
GO
ALTER TABLE [dbo].[CriteriaWeights]  WITH CHECK ADD  CONSTRAINT [FK_CriteriaWeights_Criterias_CriteriaId] FOREIGN KEY([CriteriaId])
REFERENCES [dbo].[Criterias] ([Id])
GO
ALTER TABLE [dbo].[CriteriaWeights] CHECK CONSTRAINT [FK_CriteriaWeights_Criterias_CriteriaId]
GO
ALTER TABLE [dbo].[CriteriaWeights]  WITH CHECK ADD  CONSTRAINT [FK_CriteriaWeights_LevelCriteriaGroups_LevelCriteriaGroupId] FOREIGN KEY([LevelCriteriaGroupId])
REFERENCES [dbo].[LevelCriteriaGroups] ([Id])
GO
ALTER TABLE [dbo].[CriteriaWeights] CHECK CONSTRAINT [FK_CriteriaWeights_LevelCriteriaGroups_LevelCriteriaGroupId]
GO
ALTER TABLE [dbo].[EmployeeProjects]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeProjects_Projects_ProjectId] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([Id])
GO
ALTER TABLE [dbo].[EmployeeProjects] CHECK CONSTRAINT [FK_EmployeeProjects_Projects_ProjectId]
GO
ALTER TABLE [dbo].[EmployeeProjects]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeProjects_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[EmployeeProjects] CHECK CONSTRAINT [FK_EmployeeProjects_Users_UserId]
GO
ALTER TABLE [dbo].[Goals]  WITH CHECK ADD  CONSTRAINT [FK_Goals_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Goals] CHECK CONSTRAINT [FK_Goals_Users_UserId]
GO
ALTER TABLE [dbo].[LevelCriteriaGroups]  WITH CHECK ADD  CONSTRAINT [FK_LevelCriteriaGroups_CriteriaGroups_CriteriaGroupId] FOREIGN KEY([CriteriaGroupId])
REFERENCES [dbo].[CriteriaGroups] ([Id])
GO
ALTER TABLE [dbo].[LevelCriteriaGroups] CHECK CONSTRAINT [FK_LevelCriteriaGroups_CriteriaGroups_CriteriaGroupId]
GO
ALTER TABLE [dbo].[LevelCriteriaGroups]  WITH CHECK ADD  CONSTRAINT [FK_LevelCriteriaGroups_Levels_LevelId] FOREIGN KEY([LevelId])
REFERENCES [dbo].[Levels] ([Id])
GO
ALTER TABLE [dbo].[LevelCriteriaGroups] CHECK CONSTRAINT [FK_LevelCriteriaGroups_Levels_LevelId]
GO
ALTER TABLE [dbo].[Objectives]  WITH CHECK ADD  CONSTRAINT [FK_Objectives_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Objectives] CHECK CONSTRAINT [FK_Objectives_Users_UserId]
GO
ALTER TABLE [dbo].[PerformanceReviewCycles]  WITH CHECK ADD  CONSTRAINT [FK_PerformanceReviewCycles_AppraisalModes_AppraisalModeId] FOREIGN KEY([AppraisalModeId])
REFERENCES [dbo].[AppraisalModes] ([Id])
GO
ALTER TABLE [dbo].[PerformanceReviewCycles] CHECK CONSTRAINT [FK_PerformanceReviewCycles_AppraisalModes_AppraisalModeId]
GO
ALTER TABLE [dbo].[PerformanceReviewCycles]  WITH CHECK ADD  CONSTRAINT [FK_PerformanceReviewCycles_Cycles_CycleId] FOREIGN KEY([CycleId])
REFERENCES [dbo].[Cycles] ([Id])
GO
ALTER TABLE [dbo].[PerformanceReviewCycles] CHECK CONSTRAINT [FK_PerformanceReviewCycles_Cycles_CycleId]
GO
ALTER TABLE [dbo].[Projects]  WITH CHECK ADD  CONSTRAINT [FK_Projects_Teams_TeamId] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[Projects] CHECK CONSTRAINT [FK_Projects_Teams_TeamId]
GO
ALTER TABLE [dbo].[Ratings]  WITH CHECK ADD  CONSTRAINT [FK_Ratings_AppraisalParticipants_AppraisalParticipantId] FOREIGN KEY([AppraisalParticipantId])
REFERENCES [dbo].[AppraisalParticipants] ([Id])
GO
ALTER TABLE [dbo].[Ratings] CHECK CONSTRAINT [FK_Ratings_AppraisalParticipants_AppraisalParticipantId]
GO
ALTER TABLE [dbo].[Ratings]  WITH CHECK ADD  CONSTRAINT [FK_Ratings_CriteriaWeights_CriteriaWeightId] FOREIGN KEY([CriteriaWeightId])
REFERENCES [dbo].[CriteriaWeights] ([Id])
GO
ALTER TABLE [dbo].[Ratings] CHECK CONSTRAINT [FK_Ratings_CriteriaWeights_CriteriaWeightId]
GO
ALTER TABLE [dbo].[ReviewTimelines]  WITH CHECK ADD  CONSTRAINT [FK_ReviewTimelines_Appraisals_AppraisalId] FOREIGN KEY([AppraisalId])
REFERENCES [dbo].[Appraisals] ([Id])
GO
ALTER TABLE [dbo].[ReviewTimelines] CHECK CONSTRAINT [FK_ReviewTimelines_Appraisals_AppraisalId]
GO
ALTER TABLE [dbo].[ReviewTimelines]  WITH CHECK ADD  CONSTRAINT [FK_ReviewTimelines_Stages_StageId] FOREIGN KEY([StageId])
REFERENCES [dbo].[Stages] ([Id])
GO
ALTER TABLE [dbo].[ReviewTimelines] CHECK CONSTRAINT [FK_ReviewTimelines_Stages_StageId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_BusinessUnits_BusinessUnitId] FOREIGN KEY([BusinessUnitId])
REFERENCES [dbo].[BusinessUnits] ([Id])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_BusinessUnits_BusinessUnitId]
GO
ALTER TABLE [dbo].[UserLevels]  WITH CHECK ADD  CONSTRAINT [FK_UserLevels_Levels_LevelId] FOREIGN KEY([LevelId])
REFERENCES [dbo].[Levels] ([Id])
GO
ALTER TABLE [dbo].[UserLevels] CHECK CONSTRAINT [FK_UserLevels_Levels_LevelId]
GO
ALTER TABLE [dbo].[UserLevels]  WITH CHECK ADD  CONSTRAINT [FK_UserLevels_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UserLevels] CHECK CONSTRAINT [FK_UserLevels_Users_UserId]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Roles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Roles_RoleId]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Users_UserId]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_ContractTypes_ContractTypeId] FOREIGN KEY([ContractTypeId])
REFERENCES [dbo].[ContractTypes] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_ContractTypes_ContractTypeId]
GO
