USE [PR-dev]
GO
/****** Object:  StoredProcedure [dbo].[uspGetMyTeamAppraisal]    Script Date: 8/29/2019 4:40:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Truc Phan
-- Create date: 29/08/2019
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[uspGetMyTeamAppraisal] 
	-- Add the parameters for the stored procedure here
	@email nvarchar(max) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @teamLeader nvarchar(max)
	SET @teamLeader = (SELECT t.Name
							FROM UserLevels ul
							INNER JOIN [Users] u ON u.Id = ul.UserId
							INNER JOIN Levels l ON ul.LevelId = l.Id
							INNER JOIN EmployeeProjects ep ON ep.UserId = u.Id
							INNER JOIN Projects p ON p.Id = ep.ProjectId 
							INNER JOIN Teams t ON t.Id = p.TeamId
							INNER JOIN BusinessUnits bu ON bu.Id = t.BusinessUnitId
							WHERE u.Email = @email)
	SELECT u.EmployeeId, u.FullName, l.Title, bu.Name, t.Name, SUM(r.Point) AS SumPoint
	FROM UserLevels ul
	INNER JOIN [Users] u ON u.Id = ul.UserId
	INNER JOIN Levels l ON ul.LevelId = l.Id
	INNER JOIN EmployeeProjects ep ON ep.UserId = u.Id
	INNER JOIN Projects p ON p.Id = ep.ProjectId 
	INNER JOIN Teams t ON t.Id = p.TeamId
	INNER JOIN BusinessUnits bu ON bu.Id = t.BusinessUnitId
	INNER JOIN AppraisalParticipants ap ON ap.UserId = u.Id
	INNER JOIN Ratings r ON r.AppraisalParticipantId = ap.Id 
	WHERE t.Name = @teamLeader
	GROUP BY  u.EmployeeId, u.FullName, l.Title, bu.Name, t.Name
END
SELECT u.EmployeeId, u.FullName, l.Title, bu.Name, t.Name, SUM(r.Point) AS SumPoint
	FROM UserLevels ul
	INNER JOIN [Users] u ON u.Id = ul.UserId
	INNER JOIN Levels l ON ul.LevelId = l.Id
	INNER JOIN EmployeeProjects ep ON ep.UserId = u.Id
	INNER JOIN Projects p ON p.Id = ep.ProjectId 
	INNER JOIN Teams t ON t.Id = p.TeamId
	INNER JOIN BusinessUnits bu ON bu.Id = t.BusinessUnitId
	INNER JOIN AppraisalParticipants ap ON ap.UserId = u.Id
	INNER JOIN Ratings r ON r.AppraisalParticipantId = ap.Id 
	WHERE t.Name = 'Logistics'
	GROUP BY  u.EmployeeId, u.FullName, l.Title, bu.Name, t.Name