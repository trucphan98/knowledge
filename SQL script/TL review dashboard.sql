SELECT u.EmployeeId as GrooveId,  u.FullName as EmployeeName,l.Title as EmployeeLevel, bu.Name as BU, t.Name as Team, ROUND(SUM(r.Point*cw.Weight),0) as SelfReviewResult, ap.AppraisalId 
FROM Appraisals a
INNER JOIN AppraisalParticipants ap ON a.Id = ap.AppraisalId
LEFT JOIN Users u ON u.Id = a.OwnerId
LEFT JOIN EmployeeProjects ep ON ep.UserId = u.Id
LEFT JOIN Projects p ON p.Id = ep.ProjectId
LEFT JOIN Teams t on t.Id = p.TeamId
LEFT JOIN BusinessUnits bu on bu.Id = t.BusinessUnitId
LEFT JOIN Ratings r on r.AppraisalParticipantId = ap.Id
LEFT JOIN [CriteriaWeights] cw on cw.Id = r.CriteriaWeightId
LEFT JOIN UserLevels ul on ul.UserId = u.Id
LEFT JOIN Levels l on l.Id = ul.LevelId
WHERE ap.RoleId = 7 
GROUP BY u.EmployeeId,  u.FullName,l.Title, bu.Name, t.Name ,ap.AppraisalId

