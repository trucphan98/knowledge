USE [PR-dev]
GO
/****** Object:  StoredProcedure [dbo].[usp_Criterias_GetPerformanceCriteriaList]    Script Date: 8/29/2019 9:30:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
                            -- Author: Truc Phan
                            -- Create date: 28 / 08 / 2019
                            -- Description:
                            -- =============================================
                            ALTER PROCEDURE[dbo].[usp_Criterias_GetPerformanceCriteriaList]
                                -- Add the parameters for the stored procedure here
								@Result TABLE(Id int not null, Name nvarchar(max) not null, Description nvarchar(max)  not null, IsAllLevel bit  not null) OUTPUT
                            AS
                            BEGIN
                                -- SET NOCOUNT ON added to prevent extra result sets from
                                -- interfering with SELECT statements.

                                SET NOCOUNT ON;

                                        --Insert statements for procedure here
       
                                        DECLARE @Result TABLE(Id int not null, Name nvarchar(max) not null, Description nvarchar(max)  not null, IsAllLevel bit  not null)
       
                                        DECLARE @TempTable TABLE(Id int not null, Name nvarchar(max) not null, Description nvarchar(max)  not null, LevelAmount int  not null)
       
                                        INSERT INTO @TempTable SELECT c.Id, c.Name, c.Description, Count(cw.LevelCriteriaGroupId) as LevelAmount
   
                                                                           FROM Criterias c
       
                                                                           INNER JOIN CriteriaWeights cw ON c.Id = cw.CriteriaId
       
                                                                           INNER JOIN LevelCriteriaGroups lvg ON lvg.Id = cw.LevelCriteriaGroupId
       
                                                                           WHERE c.CriteriaGroupId = 2
       
                                                                           GROUP BY c.Id, c.Name, c.Description
       
                                       WHILE(1 = 1)
       
                                       BEGIN
       
                                           DECLARE @levelAmountStandar int
       
                                           SET @levelAmountStandar = (SELECT COUNT(*) FROM Levels) -1


                                    DECLARE @id int
                                    SET @id = (SELECT TOP(1) Id FROM @TempTable)

		                            DECLARE @name nvarchar(max)

                                    SET @name = (SELECT TOP(1) Name FROM @TempTable)
		
		                            DECLARE @description nvarchar(max)

                                    SET @description = (SELECT TOP(1) Description FROM @TempTable)

		                            DECLARE @levelAmount int
                                    SET @levelAmount = (SELECT TOP(1) LevelAmount FROM @TempTable)
		                            IF @id IS NULL

                                        BREAK

                                    IF(@levelAmount = @levelAmountStandar)

                                        INSERT INTO @Result SELECT @id, @name, @description, 1

                                    ELSE
                                        INSERT INTO @Result SELECT @id, @name, @description, 0

                                    DELETE TOP(1) FROM @TempTable
                                END

                                SELECT* FROM @Result
                            END
