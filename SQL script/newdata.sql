USE [PR-dev]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppraisalModes]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppraisalModes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_AppraisalModes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppraisalParticipants]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppraisalParticipants](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[GeneralComment] [nvarchar](max) NULL,
	[IsMain] [bit] NOT NULL,
	[RoleId] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[AppraisalId] [int] NOT NULL,
	[DueDate] [datetime2](7) NULL,
	[IsInvited] [bit] NOT NULL,
 CONSTRAINT [PK_AppraisalParticipants] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Appraisals]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Appraisals](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[DueDate] [datetime2](7) NOT NULL,
	[OwnerId] [uniqueidentifier] NOT NULL,
	[PerformanceReviewCycleId] [int] NOT NULL,
 CONSTRAINT [PK_Appraisals] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BusinessUnits]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusinessUnits](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Abbre] [nvarchar](max) NULL,
	[DeliveryManagerId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_BusinessUnits] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ContractTypes]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_ContractTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CriteriaGroups]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CriteriaGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_CriteriaGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Criterias]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Criterias](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Scale] [int] NOT NULL,
	[CriteriaGroupId] [int] NOT NULL,
 CONSTRAINT [PK_Criterias] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CriteriaWeights]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CriteriaWeights](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Weight] [decimal](18, 2) NOT NULL,
	[LevelCriteriaGroupId] [int] NOT NULL,
	[CriteriaId] [int] NOT NULL,
 CONSTRAINT [PK_CriteriaWeights] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cycles]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cycles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Date] [int] NOT NULL,
	[Month] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
 CONSTRAINT [PK_Cycles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeProjects]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeProjects](
	[ProjectId] [int] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[JoiningDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_EmployeeProjects] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[ProjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Goals]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Goals](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[NextGoals] [nvarchar](max) NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[LastGoals] [nvarchar](max) NULL,
	[PerformanceReviewCycleId] [int] NOT NULL,
 CONSTRAINT [PK_Goals] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LevelCriteriaGroups]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LevelCriteriaGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Weight] [decimal](18, 2) NOT NULL,
	[LevelId] [int] NOT NULL,
	[CriteriaGroupId] [int] NOT NULL,
 CONSTRAINT [PK_LevelCriteriaGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Levels]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Levels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[Abbre] [nvarchar](max) NULL,
 CONSTRAINT [PK_Levels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lookups]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](max) NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_Lookups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Objectives]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Objectives](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[NextObjectives] [nvarchar](max) NULL,
	[Plan] [nvarchar](max) NULL,
	[TlComment] [nvarchar](max) NULL,
	[PmComment] [nvarchar](max) NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[LastObjectives] [nvarchar](max) NULL,
	[PerformanceReviewCycleId] [int] NOT NULL,
 CONSTRAINT [PK_Objectives] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PerformanceReviewCycles]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PerformanceReviewCycles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Year] [int] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[CycleId] [int] NOT NULL,
	[Status] [int] NOT NULL,
	[AppraisalModeId] [int] NOT NULL,
 CONSTRAINT [PK_PerformanceReviewCycles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Projects]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Abbre] [nvarchar](max) NULL,
	[TeamId] [int] NOT NULL,
 CONSTRAINT [PK_Projects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ratings]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ratings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Point] [float] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[CriteriaWeightId] [int] NOT NULL,
	[AppraisalParticipantId] [int] NOT NULL,
 CONSTRAINT [PK_Ratings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReviewTimelines]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReviewTimelines](
	[AppraisalId] [int] NOT NULL,
	[StageId] [int] NOT NULL,
	[FromDate] [datetime2](7) NOT NULL,
	[ToDate] [datetime2](7) NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_ReviewTimelines] PRIMARY KEY CLUSTERED 
(
	[AppraisalId] ASC,
	[StageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Stages]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stages](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[Days] [int] NOT NULL,
 CONSTRAINT [PK_Stages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Teams]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teams](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBy] [uniqueidentifier] NULL,
	[UpdatedDate] [datetime2](7) NULL,
	[UpdatedBy] [uniqueidentifier] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Abbre] [nvarchar](max) NULL,
	[BusinessUnitId] [int] NOT NULL,
 CONSTRAINT [PK_Teams] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserLevels]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLevels](
	[UserId] [uniqueidentifier] NOT NULL,
	[LevelId] [int] NOT NULL,
	[PromotionDate] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_UserLevels] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LevelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 9/26/2019 6:59:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [uniqueidentifier] NOT NULL,
	[EmployeeId] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[FullName] [nvarchar](max) NULL,
	[FirstName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[isMale] [bit] NOT NULL,
	[Photo] [varbinary](max) NULL,
	[BirthDate] [datetime2](7) NOT NULL,
	[StartDate] [datetime2](7) NOT NULL,
	[ContractTypeId] [int] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190829170239_InitiateDatabase', N'3.0.0-preview8.19405.11')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190904080048_UpdateRatingDataTypeToDouble', N'3.0.0-preview8.19405.11')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190904092758_AddColumnIn_Goal_Objective', N'3.0.0-preview8.19405.11')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190904184939_UpdateDataTypeInCycleTable', N'3.0.0-preview8.19405.11')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190913044531_AddColumnIn_AppraisalParticipant', N'3.0.0-preview8.19405.11')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190923091504_AddColumnIn_Goal_Objective_Ref_PerformanceCycleReview', N'3.0.0-preview8.19405.11')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20190924034745_AddStageStausInReviewTimeline', N'3.0.0-preview8.19405.11')
SET IDENTITY_INSERT [dbo].[AppraisalModes] ON 

INSERT [dbo].[AppraisalModes] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Special')
INSERT [dbo].[AppraisalModes] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Probation ')
INSERT [dbo].[AppraisalModes] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Annual Review')
SET IDENTITY_INSERT [dbo].[AppraisalModes] OFF
SET IDENTITY_INSERT [dbo].[AppraisalParticipants] ON 

INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (1, CAST(N'2019-09-26T17:01:16.0752348' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.0752358' AS DateTime2), NULL, 0, NULL, 1, 3, N'0e416fec-b6c6-4af2-b921-08d742677ae3', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (2, CAST(N'2019-09-26T17:01:16.0761886' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.0761902' AS DateTime2), NULL, 0, NULL, 1, 2, N'b3370f06-7556-4299-b941-08d742677ae3', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (3, CAST(N'2019-09-26T17:01:16.0761915' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.0761917' AS DateTime2), NULL, 0, NULL, 1, 7, N'7ebc8676-073a-4801-a6fc-72737782aa6f', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (4, CAST(N'2019-09-26T17:01:16.0762785' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.0762791' AS DateTime2), NULL, 0, NULL, 1, 6, N'd84f6f37-6b96-460f-b965-08d742677ae3', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (5, CAST(N'2019-09-26T17:01:16.0762803' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.0762804' AS DateTime2), NULL, 0, NULL, 1, 5, N'4fd278d4-3f6b-470c-b94d-08d742677ae3', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (6, CAST(N'2019-09-26T17:01:16.0762825' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.0762826' AS DateTime2), NULL, 0, NULL, 1, 4, N'a8d2732b-c464-4334-b94a-08d742677ae3', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (7, CAST(N'2019-09-26T17:01:16.0762829' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.0762830' AS DateTime2), NULL, 0, NULL, 0, 6, N'bf4ca84b-f41c-492b-ba89-dffd8d8e8b0b', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (8, CAST(N'2019-09-26T17:01:16.2988303' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.2988304' AS DateTime2), NULL, 0, NULL, 0, 6, N'4fd278d4-3f6b-470c-b94d-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (9, CAST(N'2019-09-26T17:01:16.2988300' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.2988301' AS DateTime2), NULL, 0, NULL, 0, 6, N'd84f6f37-6b96-460f-b965-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (10, CAST(N'2019-09-26T17:01:16.2988298' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.2988298' AS DateTime2), NULL, 0, NULL, 1, 4, N'a8d2732b-c464-4334-b94a-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (11, CAST(N'2019-09-26T17:01:16.2988293' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.2988294' AS DateTime2), NULL, 0, NULL, 1, 5, N'a8d2732b-c464-4334-b94a-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (12, CAST(N'2019-09-26T17:01:16.2988288' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.2988289' AS DateTime2), NULL, 0, NULL, 1, 6, N'7ebc8676-073a-4801-a6fc-72737782aa6f', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (13, CAST(N'2019-09-26T17:01:16.2988281' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.2988281' AS DateTime2), NULL, 0, NULL, 1, 7, N'bf4ca84b-f41c-492b-ba89-dffd8d8e8b0b', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (14, CAST(N'2019-09-26T17:01:16.2988277' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.2988277' AS DateTime2), NULL, 0, NULL, 1, 2, N'b3370f06-7556-4299-b941-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (15, CAST(N'2019-09-26T17:01:16.2988258' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:01:16.2988259' AS DateTime2), NULL, 0, NULL, 1, 3, N'0e416fec-b6c6-4af2-b921-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (16, CAST(N'2019-09-26T17:09:34.8796621' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.8796622' AS DateTime2), NULL, 0, NULL, 1, 3, N'0e416fec-b6c6-4af2-b921-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (17, CAST(N'2019-09-26T17:09:34.8796643' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.8796644' AS DateTime2), NULL, 0, NULL, 1, 2, N'b3370f06-7556-4299-b941-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (18, CAST(N'2019-09-26T17:09:34.8796648' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.8796649' AS DateTime2), NULL, 0, NULL, 1, 7, N'4fd278d4-3f6b-470c-b94d-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (19, CAST(N'2019-09-26T17:09:34.8796656' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.8796657' AS DateTime2), NULL, 0, NULL, 1, 6, N'7ebc8676-073a-4801-a6fc-72737782aa6f', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (20, CAST(N'2019-09-26T17:09:34.8796660' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.8796661' AS DateTime2), NULL, 0, NULL, 1, 5, N'a8d2732b-c464-4334-b94a-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (21, CAST(N'2019-09-26T17:09:34.8796665' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.8796666' AS DateTime2), NULL, 0, NULL, 1, 4, N'a8d2732b-c464-4334-b94a-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (22, CAST(N'2019-09-26T17:09:34.9145562' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9145563' AS DateTime2), NULL, 0, NULL, 1, 4, N'a8d2732b-c464-4334-b94a-08d742677ae3', 4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (23, CAST(N'2019-09-26T17:09:34.9145557' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9145558' AS DateTime2), NULL, 0, NULL, 1, 5, N'a8d2732b-c464-4334-b94a-08d742677ae3', 4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (24, CAST(N'2019-09-26T17:09:34.9145554' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9145555' AS DateTime2), NULL, 0, NULL, 1, 6, N'7ebc8676-073a-4801-a6fc-72737782aa6f', 4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (25, CAST(N'2019-09-26T17:09:34.9145548' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9145549' AS DateTime2), NULL, 0, NULL, 1, 7, N'd84f6f37-6b96-460f-b965-08d742677ae3', 4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (26, CAST(N'2019-09-26T17:09:34.9145543' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9145544' AS DateTime2), NULL, 0, NULL, 1, 2, N'b3370f06-7556-4299-b941-08d742677ae3', 4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (27, CAST(N'2019-09-26T17:09:34.9145524' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9145525' AS DateTime2), NULL, 0, NULL, 1, 3, N'0e416fec-b6c6-4af2-b921-08d742677ae3', 4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (28, CAST(N'2019-09-26T17:09:34.9263008' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9263009' AS DateTime2), NULL, 0, NULL, 1, 4, N'a8d2732b-c464-4334-b94a-08d742677ae3', 5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (29, CAST(N'2019-09-26T17:09:34.9263004' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9263005' AS DateTime2), NULL, 0, NULL, 1, 5, N'a8d2732b-c464-4334-b94a-08d742677ae3', 5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (30, CAST(N'2019-09-26T17:09:34.9263000' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9263001' AS DateTime2), NULL, 0, NULL, 1, 6, N'd84f6f37-6b96-460f-b965-08d742677ae3', 5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (31, CAST(N'2019-09-26T17:09:34.9262993' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9262993' AS DateTime2), NULL, 0, NULL, 1, 7, N'7ebc8676-073a-4801-a6fc-72737782aa6f', 5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (32, CAST(N'2019-09-26T17:09:34.9262988' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9262989' AS DateTime2), NULL, 0, NULL, 1, 2, N'b3370f06-7556-4299-b941-08d742677ae3', 5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (33, CAST(N'2019-09-26T17:09:34.9262968' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9262969' AS DateTime2), NULL, 0, NULL, 1, 3, N'0e416fec-b6c6-4af2-b921-08d742677ae3', 5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (34, CAST(N'2019-09-26T17:09:34.9342802' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9342803' AS DateTime2), NULL, 0, NULL, 0, 6, N'4fd278d4-3f6b-470c-b94d-08d742677ae3', 6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (35, CAST(N'2019-09-26T17:09:34.9342799' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9342800' AS DateTime2), NULL, 0, NULL, 0, 6, N'd84f6f37-6b96-460f-b965-08d742677ae3', 6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (36, CAST(N'2019-09-26T17:09:34.9342796' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9342797' AS DateTime2), NULL, 0, NULL, 1, 4, N'a8d2732b-c464-4334-b94a-08d742677ae3', 6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (37, CAST(N'2019-09-26T17:09:34.9342792' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9342793' AS DateTime2), NULL, 0, NULL, 1, 5, N'a8d2732b-c464-4334-b94a-08d742677ae3', 6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (38, CAST(N'2019-09-26T17:09:34.9342789' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9342790' AS DateTime2), NULL, 0, NULL, 1, 6, N'7ebc8676-073a-4801-a6fc-72737782aa6f', 6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (39, CAST(N'2019-09-26T17:09:34.9342783' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9342784' AS DateTime2), NULL, 0, NULL, 1, 7, N'bf4ca84b-f41c-492b-ba89-dffd8d8e8b0b', 6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (40, CAST(N'2019-09-26T17:09:34.9342778' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9342780' AS DateTime2), NULL, 0, NULL, 1, 2, N'b3370f06-7556-4299-b941-08d742677ae3', 6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
INSERT [dbo].[AppraisalParticipants] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [GeneralComment], [IsMain], [RoleId], [UserId], [AppraisalId], [DueDate], [IsInvited]) VALUES (41, CAST(N'2019-09-26T17:09:34.9342761' AS DateTime2), N'00000000-0000-0000-0000-000000000000', CAST(N'2019-09-26T17:09:34.9342762' AS DateTime2), NULL, 0, NULL, 1, 3, N'0e416fec-b6c6-4af2-b921-08d742677ae3', 6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0)
SET IDENTITY_INSERT [dbo].[AppraisalParticipants] OFF
SET IDENTITY_INSERT [dbo].[Appraisals] ON 

INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (1, CAST(N'2019-09-26T17:01:16.0741151' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'7ebc8676-073a-4801-a6fc-72737782aa6f', 1)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (2, CAST(N'2019-09-26T17:01:16.2988209' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'bf4ca84b-f41c-492b-ba89-dffd8d8e8b0b', 1)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (3, CAST(N'2019-09-26T17:09:34.8796572' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'4fd278d4-3f6b-470c-b94d-08d742677ae3', 2)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (4, CAST(N'2019-09-26T17:09:34.9145440' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'd84f6f37-6b96-460f-b965-08d742677ae3', 2)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (5, CAST(N'2019-09-26T17:09:34.9262931' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'7ebc8676-073a-4801-a6fc-72737782aa6f', 2)
INSERT [dbo].[Appraisals] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DueDate], [OwnerId], [PerformanceReviewCycleId]) VALUES (6, CAST(N'2019-09-26T17:09:34.9342724' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), N'bf4ca84b-f41c-492b-ba89-dffd8d8e8b0b', 2)
SET IDENTITY_INSERT [dbo].[Appraisals] OFF
SET IDENTITY_INSERT [dbo].[BusinessUnits] ON 

INSERT [dbo].[BusinessUnits] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [DeliveryManagerId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Supporting', N'SUP', N'00000000-0000-0000-0000-000000000000')
INSERT [dbo].[BusinessUnits] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [DeliveryManagerId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Operational Development', N'OD', N'5c97d12d-cdf9-4641-b95b-08d742677ae3')
INSERT [dbo].[BusinessUnits] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [DeliveryManagerId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Logistics', N'LOG', N'a8d2732b-c464-4334-b94a-08d742677ae3')
INSERT [dbo].[BusinessUnits] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [DeliveryManagerId]) VALUES (4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'eCommerce', N'eCom', N'21a9337b-14b0-4ed3-b93f-08d742677ae3')
SET IDENTITY_INSERT [dbo].[BusinessUnits] OFF
SET IDENTITY_INSERT [dbo].[ContractTypes] ON 

INSERT [dbo].[ContractTypes] ([Id], [Name]) VALUES (1, N'OnGoing')
SET IDENTITY_INSERT [dbo].[ContractTypes] OFF
SET IDENTITY_INSERT [dbo].[CriteriaGroups] ON 

INSERT [dbo].[CriteriaGroups] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Management Criteria')
INSERT [dbo].[CriteriaGroups] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Performance Criteria')
SET IDENTITY_INSERT [dbo].[CriteriaGroups] OFF
SET IDENTITY_INSERT [dbo].[Criterias] ON 

INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Staff Development', N'- Ability to recognize subordinates'' strengths and weaknesses
- Capability and willingness to help staffs overcome their weaknesses
- Ability to instruct and guide subordinates to further develop their strong points.', 5, 1)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Leadership/Management skills', N'- Exhibit self-confidence
- Inspire trust and respect from others
- Ability to motivate people to perform well
- React under critical situations
- Ability to make decisions
- Ability to control and coordinate
- Ability to resolve conflicts
- Ability to delegate (assign) appropriate tasks to members', 5, 1)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Responsibility/Accountability', N'- Ability to receive workload and respond to problems
- Ability to complete tasks on time meet the deadlines with good output
- Ability to support others complete their tasks on time meet the deadlines with good output
- Go to work on time, Go to meeting and other activities on time
- Participate in company activities', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Decision making and problem solving', N'- Ability to adapt to unexpected changes
- Capable of foreseeing potential problems / issues
- Ability to give solutions to dev in task development and responsible for accuracy.
- Ability to make decision on most of tasks / defects in reviewing.
- Ask / Confirm with ATL for complicated technical issues to make final decision', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Decision making', N'- Ability to identify the problem
- Ask/Confirm with ATL for complicated technical issues to make final decision
- Ability to analysis and select the best solution on most of tasks/defects in reviewing.', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Problem solving', N'- Ability to adapt to unexpected changes/Ability to handle unexpected changes as well as complex situations
- Capable of foreseeing potential problems / issues
- Ability to give solutions to dev in task development and responsible for accuracy.', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (7, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Teamworks', N'- Ability to cooperate with other team members
- Ability to reach an outcome of mutually acceptable compromise
- Frequency of causing conflict in the team
- Ability to actively participate in team meetings, brain storming and review sessionseview sessions', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Time management and planning skills', N'- Ability to prioritize assigned tasks
- Ability to handle multi-tasking
- Willingness to work late or over weekend in order to complete the urgent tasks
- Ability to take over the task rather than development: testing, maintenance, bug fixing
- Ability to actively report status or raise issue on the assignment', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (9, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'English level', N'Refer to skill matrix for each level', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (10, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Communication skills', N'- Ability to expresses thoughts effectively in individual and group situations
- Ability to effectively transfer knowledge and know-how to others
- Documentation skill
- Ability to express ideas in writing / email', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (11, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Technical skills', N'- Level of domain knowledge of specific team/as per specific customer
- Ability to quickly learn, adjust and adapt to change technical request in project
- Technology Watch: ability to  stay current with new technologies, methods, and processes in software development industry?', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (12, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Job knowledge', N'- Ability to know how to use correct method and procedure to fulfill assigned task
- Ability to understand client''s request and requirement
- Ability to understand client''s bug report
- Abitity to actively propose feedback or solution on customer''s requirement / request', 5, 2)
INSERT [dbo].[Criterias] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Description], [Scale], [CriteriaGroupId]) VALUES (13, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Quality Of Work', N'- Productivity (Volume of work load vs. Time needed to finish task)
- Ablity to handle all tasks of a project with good quality and meet the deadlines
- Ability to get the job done in expected quality with no or little supervisory
- Smoothly follow the processes', 5, 2)
SET IDENTITY_INSERT [dbo].[Criterias] OFF
SET IDENTITY_INSERT [dbo].[CriteriaWeights] ON 

INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (1, CAST(0.00 AS Decimal(18, 2)), 14, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (2, CAST(0.20 AS Decimal(18, 2)), 46, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (3, CAST(0.00 AS Decimal(18, 2)), 47, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (4, CAST(0.00 AS Decimal(18, 2)), 47, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (5, CAST(0.10 AS Decimal(18, 2)), 48, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (6, CAST(0.00 AS Decimal(18, 2)), 48, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (7, CAST(0.05 AS Decimal(18, 2)), 48, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (8, CAST(0.05 AS Decimal(18, 2)), 48, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (9, CAST(0.10 AS Decimal(18, 2)), 48, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (10, CAST(0.10 AS Decimal(18, 2)), 46, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (11, CAST(0.10 AS Decimal(18, 2)), 48, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (12, CAST(0.10 AS Decimal(18, 2)), 48, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (13, CAST(0.10 AS Decimal(18, 2)), 48, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (14, CAST(0.10 AS Decimal(18, 2)), 48, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (15, CAST(0.20 AS Decimal(18, 2)), 48, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (16, CAST(0.00 AS Decimal(18, 2)), 49, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (17, CAST(0.00 AS Decimal(18, 2)), 49, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (18, CAST(0.15 AS Decimal(18, 2)), 50, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (19, CAST(0.00 AS Decimal(18, 2)), 50, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (20, CAST(0.10 AS Decimal(18, 2)), 48, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (21, CAST(0.00 AS Decimal(18, 2)), 46, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (22, CAST(0.10 AS Decimal(18, 2)), 46, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (23, CAST(0.10 AS Decimal(18, 2)), 46, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (24, CAST(0.15 AS Decimal(18, 2)), 44, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (25, CAST(0.10 AS Decimal(18, 2)), 44, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (26, CAST(0.00 AS Decimal(18, 2)), 44, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (27, CAST(0.00 AS Decimal(18, 2)), 44, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (28, CAST(0.10 AS Decimal(18, 2)), 44, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (29, CAST(0.10 AS Decimal(18, 2)), 44, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (30, CAST(0.10 AS Decimal(18, 2)), 44, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (31, CAST(0.15 AS Decimal(18, 2)), 44, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (32, CAST(0.00 AS Decimal(18, 2)), 44, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (33, CAST(0.15 AS Decimal(18, 2)), 44, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (34, CAST(0.15 AS Decimal(18, 2)), 44, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (35, CAST(0.00 AS Decimal(18, 2)), 45, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (36, CAST(0.00 AS Decimal(18, 2)), 45, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (37, CAST(0.15 AS Decimal(18, 2)), 46, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (38, CAST(0.15 AS Decimal(18, 2)), 46, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (39, CAST(0.00 AS Decimal(18, 2)), 46, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (40, CAST(0.00 AS Decimal(18, 2)), 46, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (41, CAST(0.10 AS Decimal(18, 2)), 46, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (42, CAST(0.10 AS Decimal(18, 2)), 46, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (43, CAST(0.05 AS Decimal(18, 2)), 50, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (44, CAST(0.05 AS Decimal(18, 2)), 50, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (45, CAST(0.10 AS Decimal(18, 2)), 50, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (46, CAST(0.05 AS Decimal(18, 2)), 50, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (47, CAST(0.00 AS Decimal(18, 2)), 54, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (48, CAST(0.10 AS Decimal(18, 2)), 54, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (49, CAST(0.10 AS Decimal(18, 2)), 54, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (50, CAST(0.10 AS Decimal(18, 2)), 54, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (51, CAST(0.10 AS Decimal(18, 2)), 54, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (52, CAST(0.00 AS Decimal(18, 2)), 54, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (53, CAST(0.15 AS Decimal(18, 2)), 54, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (54, CAST(0.15 AS Decimal(18, 2)), 54, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (55, CAST(0.50 AS Decimal(18, 2)), 29, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (56, CAST(0.50 AS Decimal(18, 2)), 29, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (57, CAST(0.10 AS Decimal(18, 2)), 28, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (58, CAST(0.15 AS Decimal(18, 2)), 28, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (59, CAST(0.00 AS Decimal(18, 2)), 28, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (60, CAST(0.00 AS Decimal(18, 2)), 28, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (61, CAST(0.10 AS Decimal(18, 2)), 28, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (62, CAST(0.15 AS Decimal(18, 2)), 28, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (63, CAST(0.10 AS Decimal(18, 2)), 28, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (64, CAST(0.10 AS Decimal(18, 2)), 28, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (65, CAST(0.00 AS Decimal(18, 2)), 28, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (66, CAST(0.00 AS Decimal(18, 2)), 54, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (67, CAST(0.00 AS Decimal(18, 2)), 43, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (68, CAST(0.20 AS Decimal(18, 2)), 54, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (69, CAST(0.50 AS Decimal(18, 2)), 53, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (70, CAST(0.05 AS Decimal(18, 2)), 50, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (71, CAST(0.05 AS Decimal(18, 2)), 50, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (72, CAST(0.15 AS Decimal(18, 2)), 50, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (73, CAST(0.10 AS Decimal(18, 2)), 50, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (74, CAST(0.25 AS Decimal(18, 2)), 50, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (75, CAST(0.50 AS Decimal(18, 2)), 51, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (76, CAST(0.50 AS Decimal(18, 2)), 51, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (77, CAST(0.10 AS Decimal(18, 2)), 52, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (78, CAST(0.20 AS Decimal(18, 2)), 52, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (79, CAST(0.00 AS Decimal(18, 2)), 52, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (80, CAST(0.00 AS Decimal(18, 2)), 52, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (81, CAST(0.10 AS Decimal(18, 2)), 52, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (82, CAST(0.10 AS Decimal(18, 2)), 52, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (83, CAST(0.10 AS Decimal(18, 2)), 52, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (84, CAST(0.10 AS Decimal(18, 2)), 52, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (85, CAST(0.00 AS Decimal(18, 2)), 52, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (86, CAST(0.15 AS Decimal(18, 2)), 52, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (87, CAST(0.15 AS Decimal(18, 2)), 52, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (88, CAST(0.50 AS Decimal(18, 2)), 53, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (89, CAST(0.10 AS Decimal(18, 2)), 54, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (90, CAST(0.15 AS Decimal(18, 2)), 28, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (91, CAST(0.00 AS Decimal(18, 2)), 43, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (92, CAST(0.15 AS Decimal(18, 2)), 42, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (93, CAST(0.15 AS Decimal(18, 2)), 32, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (94, CAST(0.00 AS Decimal(18, 2)), 33, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (95, CAST(0.00 AS Decimal(18, 2)), 33, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (96, CAST(0.15 AS Decimal(18, 2)), 34, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (97, CAST(0.15 AS Decimal(18, 2)), 34, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (98, CAST(0.00 AS Decimal(18, 2)), 34, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (99, CAST(0.00 AS Decimal(18, 2)), 34, 6)
GO
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (100, CAST(0.10 AS Decimal(18, 2)), 34, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (101, CAST(0.15 AS Decimal(18, 2)), 32, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (102, CAST(0.10 AS Decimal(18, 2)), 34, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (103, CAST(0.10 AS Decimal(18, 2)), 34, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (104, CAST(0.00 AS Decimal(18, 2)), 34, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (105, CAST(0.15 AS Decimal(18, 2)), 34, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (106, CAST(0.15 AS Decimal(18, 2)), 34, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (107, CAST(0.00 AS Decimal(18, 2)), 35, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (108, CAST(0.00 AS Decimal(18, 2)), 35, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (109, CAST(0.15 AS Decimal(18, 2)), 36, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (110, CAST(0.10 AS Decimal(18, 2)), 36, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (111, CAST(0.10 AS Decimal(18, 2)), 34, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (112, CAST(0.00 AS Decimal(18, 2)), 32, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (113, CAST(0.10 AS Decimal(18, 2)), 32, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (114, CAST(0.10 AS Decimal(18, 2)), 32, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (115, CAST(0.00 AS Decimal(18, 2)), 30, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (116, CAST(0.00 AS Decimal(18, 2)), 30, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (117, CAST(0.00 AS Decimal(18, 2)), 30, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (118, CAST(0.00 AS Decimal(18, 2)), 30, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (119, CAST(0.00 AS Decimal(18, 2)), 30, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (120, CAST(0.00 AS Decimal(18, 2)), 30, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (121, CAST(0.00 AS Decimal(18, 2)), 30, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (122, CAST(0.00 AS Decimal(18, 2)), 30, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (123, CAST(0.00 AS Decimal(18, 2)), 30, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (124, CAST(0.00 AS Decimal(18, 2)), 30, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (125, CAST(0.00 AS Decimal(18, 2)), 30, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (126, CAST(0.60 AS Decimal(18, 2)), 31, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (127, CAST(0.40 AS Decimal(18, 2)), 31, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (128, CAST(0.10 AS Decimal(18, 2)), 32, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (129, CAST(0.20 AS Decimal(18, 2)), 32, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (130, CAST(0.00 AS Decimal(18, 2)), 32, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (131, CAST(0.00 AS Decimal(18, 2)), 32, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (132, CAST(0.10 AS Decimal(18, 2)), 32, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (133, CAST(0.10 AS Decimal(18, 2)), 32, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (134, CAST(0.00 AS Decimal(18, 2)), 36, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (135, CAST(0.00 AS Decimal(18, 2)), 36, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (136, CAST(0.10 AS Decimal(18, 2)), 36, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (137, CAST(0.10 AS Decimal(18, 2)), 36, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (138, CAST(0.00 AS Decimal(18, 2)), 40, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (139, CAST(0.10 AS Decimal(18, 2)), 40, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (140, CAST(0.10 AS Decimal(18, 2)), 40, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (141, CAST(0.10 AS Decimal(18, 2)), 40, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (142, CAST(0.10 AS Decimal(18, 2)), 40, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (143, CAST(0.00 AS Decimal(18, 2)), 40, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (144, CAST(0.10 AS Decimal(18, 2)), 40, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (145, CAST(0.20 AS Decimal(18, 2)), 40, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (146, CAST(0.60 AS Decimal(18, 2)), 41, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (147, CAST(0.40 AS Decimal(18, 2)), 41, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (148, CAST(0.10 AS Decimal(18, 2)), 42, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (149, CAST(0.20 AS Decimal(18, 2)), 42, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (150, CAST(0.00 AS Decimal(18, 2)), 42, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (151, CAST(0.00 AS Decimal(18, 2)), 42, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (152, CAST(0.10 AS Decimal(18, 2)), 42, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (153, CAST(0.10 AS Decimal(18, 2)), 42, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (154, CAST(0.10 AS Decimal(18, 2)), 42, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (155, CAST(0.10 AS Decimal(18, 2)), 42, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (156, CAST(0.00 AS Decimal(18, 2)), 42, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (157, CAST(0.00 AS Decimal(18, 2)), 40, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (158, CAST(0.15 AS Decimal(18, 2)), 42, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (159, CAST(0.15 AS Decimal(18, 2)), 40, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (160, CAST(0.00 AS Decimal(18, 2)), 39, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (161, CAST(0.10 AS Decimal(18, 2)), 36, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (162, CAST(0.15 AS Decimal(18, 2)), 36, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (163, CAST(0.00 AS Decimal(18, 2)), 36, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (164, CAST(0.10 AS Decimal(18, 2)), 36, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (165, CAST(0.20 AS Decimal(18, 2)), 36, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (166, CAST(0.00 AS Decimal(18, 2)), 37, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (167, CAST(0.00 AS Decimal(18, 2)), 37, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (168, CAST(0.15 AS Decimal(18, 2)), 38, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (169, CAST(0.00 AS Decimal(18, 2)), 38, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (170, CAST(0.00 AS Decimal(18, 2)), 38, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (171, CAST(0.05 AS Decimal(18, 2)), 38, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (172, CAST(0.15 AS Decimal(18, 2)), 38, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (173, CAST(0.10 AS Decimal(18, 2)), 38, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (174, CAST(0.10 AS Decimal(18, 2)), 38, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (175, CAST(0.10 AS Decimal(18, 2)), 38, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (176, CAST(0.00 AS Decimal(18, 2)), 38, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (177, CAST(0.10 AS Decimal(18, 2)), 38, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (178, CAST(0.25 AS Decimal(18, 2)), 38, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (179, CAST(0.00 AS Decimal(18, 2)), 39, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (180, CAST(0.15 AS Decimal(18, 2)), 40, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (181, CAST(0.15 AS Decimal(18, 2)), 28, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (182, CAST(0.00 AS Decimal(18, 2)), 27, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (183, CAST(0.00 AS Decimal(18, 2)), 27, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (184, CAST(0.00 AS Decimal(18, 2)), 23, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (185, CAST(0.10 AS Decimal(18, 2)), 23, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (186, CAST(0.00 AS Decimal(18, 2)), 22, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (187, CAST(0.00 AS Decimal(18, 2)), 22, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (188, CAST(0.10 AS Decimal(18, 2)), 21, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (189, CAST(0.10 AS Decimal(18, 2)), 21, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (190, CAST(0.10 AS Decimal(18, 2)), 21, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (191, CAST(0.10 AS Decimal(18, 2)), 21, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (192, CAST(0.10 AS Decimal(18, 2)), 23, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (193, CAST(0.10 AS Decimal(18, 2)), 21, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (194, CAST(0.10 AS Decimal(18, 2)), 21, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (195, CAST(0.00 AS Decimal(18, 2)), 21, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (196, CAST(0.00 AS Decimal(18, 2)), 21, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (197, CAST(0.15 AS Decimal(18, 2)), 21, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (198, CAST(0.15 AS Decimal(18, 2)), 21, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (199, CAST(0.40 AS Decimal(18, 2)), 20, 2)
GO
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (200, CAST(0.60 AS Decimal(18, 2)), 20, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (201, CAST(0.15 AS Decimal(18, 2)), 19, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (202, CAST(0.10 AS Decimal(18, 2)), 21, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (203, CAST(0.05 AS Decimal(18, 2)), 23, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (204, CAST(0.10 AS Decimal(18, 2)), 23, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (205, CAST(0.10 AS Decimal(18, 2)), 23, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (206, CAST(0.00 AS Decimal(18, 2)), 55, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (207, CAST(0.25 AS Decimal(18, 2)), 25, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (208, CAST(0.10 AS Decimal(18, 2)), 25, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (209, CAST(0.15 AS Decimal(18, 2)), 25, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (210, CAST(0.05 AS Decimal(18, 2)), 25, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (211, CAST(0.05 AS Decimal(18, 2)), 25, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (212, CAST(0.05 AS Decimal(18, 2)), 25, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (213, CAST(0.10 AS Decimal(18, 2)), 25, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (214, CAST(0.05 AS Decimal(18, 2)), 25, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (215, CAST(0.05 AS Decimal(18, 2)), 25, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (216, CAST(0.00 AS Decimal(18, 2)), 25, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (217, CAST(0.15 AS Decimal(18, 2)), 25, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (218, CAST(0.00 AS Decimal(18, 2)), 24, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (219, CAST(0.00 AS Decimal(18, 2)), 24, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (220, CAST(0.15 AS Decimal(18, 2)), 23, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (221, CAST(0.10 AS Decimal(18, 2)), 23, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (222, CAST(0.10 AS Decimal(18, 2)), 23, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (223, CAST(0.10 AS Decimal(18, 2)), 23, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (224, CAST(0.10 AS Decimal(18, 2)), 23, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (225, CAST(0.10 AS Decimal(18, 2)), 19, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (226, CAST(0.10 AS Decimal(18, 2)), 19, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (227, CAST(0.10 AS Decimal(18, 2)), 19, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (228, CAST(0.10 AS Decimal(18, 2)), 19, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (229, CAST(0.10 AS Decimal(18, 2)), 15, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (230, CAST(0.10 AS Decimal(18, 2)), 15, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (231, CAST(0.10 AS Decimal(18, 2)), 15, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (232, CAST(0.10 AS Decimal(18, 2)), 15, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (233, CAST(0.10 AS Decimal(18, 2)), 15, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (234, CAST(0.00 AS Decimal(18, 2)), 15, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (235, CAST(0.00 AS Decimal(18, 2)), 15, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (236, CAST(0.15 AS Decimal(18, 2)), 15, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (237, CAST(0.10 AS Decimal(18, 2)), 15, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (238, CAST(0.40 AS Decimal(18, 2)), 26, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (239, CAST(0.60 AS Decimal(18, 2)), 26, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (240, CAST(0.25 AS Decimal(18, 2)), 14, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (241, CAST(0.10 AS Decimal(18, 2)), 14, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (242, CAST(0.10 AS Decimal(18, 2)), 14, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (243, CAST(0.10 AS Decimal(18, 2)), 14, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (244, CAST(0.05 AS Decimal(18, 2)), 14, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (245, CAST(0.05 AS Decimal(18, 2)), 14, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (246, CAST(0.15 AS Decimal(18, 2)), 14, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (247, CAST(0.05 AS Decimal(18, 2)), 14, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (248, CAST(0.10 AS Decimal(18, 2)), 15, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (249, CAST(0.00 AS Decimal(18, 2)), 55, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (250, CAST(0.15 AS Decimal(18, 2)), 15, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (251, CAST(0.40 AS Decimal(18, 2)), 16, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (252, CAST(0.10 AS Decimal(18, 2)), 19, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (253, CAST(0.10 AS Decimal(18, 2)), 19, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (254, CAST(0.00 AS Decimal(18, 2)), 19, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (255, CAST(0.00 AS Decimal(18, 2)), 19, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (256, CAST(0.15 AS Decimal(18, 2)), 19, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (257, CAST(0.10 AS Decimal(18, 2)), 19, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (258, CAST(0.40 AS Decimal(18, 2)), 18, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (259, CAST(0.60 AS Decimal(18, 2)), 18, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (260, CAST(0.15 AS Decimal(18, 2)), 17, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (261, CAST(0.10 AS Decimal(18, 2)), 17, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (262, CAST(0.10 AS Decimal(18, 2)), 17, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (263, CAST(0.10 AS Decimal(18, 2)), 17, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (264, CAST(0.10 AS Decimal(18, 2)), 17, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (265, CAST(0.10 AS Decimal(18, 2)), 17, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (266, CAST(0.10 AS Decimal(18, 2)), 17, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (267, CAST(0.00 AS Decimal(18, 2)), 17, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (268, CAST(0.00 AS Decimal(18, 2)), 17, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (269, CAST(0.15 AS Decimal(18, 2)), 17, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (270, CAST(0.10 AS Decimal(18, 2)), 17, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (271, CAST(0.60 AS Decimal(18, 2)), 16, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (272, CAST(0.15 AS Decimal(18, 2)), 56, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (273, CAST(0.00 AS Decimal(18, 2)), 56, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (274, CAST(0.00 AS Decimal(18, 2)), 56, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (275, CAST(0.15 AS Decimal(18, 2)), 3, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (276, CAST(0.60 AS Decimal(18, 2)), 4, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (277, CAST(0.40 AS Decimal(18, 2)), 4, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (278, CAST(0.15 AS Decimal(18, 2)), 5, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (279, CAST(0.15 AS Decimal(18, 2)), 5, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (280, CAST(0.00 AS Decimal(18, 2)), 5, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (281, CAST(0.00 AS Decimal(18, 2)), 5, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (282, CAST(0.10 AS Decimal(18, 2)), 5, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (283, CAST(0.10 AS Decimal(18, 2)), 5, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (284, CAST(0.10 AS Decimal(18, 2)), 5, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (285, CAST(0.10 AS Decimal(18, 2)), 5, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (286, CAST(0.10 AS Decimal(18, 2)), 5, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (287, CAST(0.10 AS Decimal(18, 2)), 5, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (288, CAST(0.10 AS Decimal(18, 2)), 5, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (289, CAST(0.60 AS Decimal(18, 2)), 6, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (290, CAST(0.40 AS Decimal(18, 2)), 6, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (291, CAST(0.15 AS Decimal(18, 2)), 7, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (292, CAST(0.15 AS Decimal(18, 2)), 7, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (293, CAST(0.00 AS Decimal(18, 2)), 7, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (294, CAST(0.15 AS Decimal(18, 2)), 3, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (295, CAST(0.00 AS Decimal(18, 2)), 7, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (296, CAST(0.00 AS Decimal(18, 2)), 3, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (297, CAST(0.10 AS Decimal(18, 2)), 3, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (298, CAST(0.10 AS Decimal(18, 2)), 13, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (299, CAST(0.15 AS Decimal(18, 2)), 13, 4)
GO
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (300, CAST(0.00 AS Decimal(18, 2)), 13, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (301, CAST(0.00 AS Decimal(18, 2)), 13, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (302, CAST(0.10 AS Decimal(18, 2)), 13, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (303, CAST(0.15 AS Decimal(18, 2)), 13, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (304, CAST(0.10 AS Decimal(18, 2)), 13, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (305, CAST(0.10 AS Decimal(18, 2)), 13, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (306, CAST(0.00 AS Decimal(18, 2)), 13, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (307, CAST(0.15 AS Decimal(18, 2)), 13, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (308, CAST(0.15 AS Decimal(18, 2)), 13, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (309, CAST(0.00 AS Decimal(18, 2)), 2, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (310, CAST(0.00 AS Decimal(18, 2)), 2, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (311, CAST(0.15 AS Decimal(18, 2)), 3, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (312, CAST(0.10 AS Decimal(18, 2)), 3, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (313, CAST(0.00 AS Decimal(18, 2)), 3, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (314, CAST(0.00 AS Decimal(18, 2)), 3, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (315, CAST(0.10 AS Decimal(18, 2)), 3, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (316, CAST(0.10 AS Decimal(18, 2)), 3, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (317, CAST(0.15 AS Decimal(18, 2)), 3, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (318, CAST(0.00 AS Decimal(18, 2)), 1, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (319, CAST(0.10 AS Decimal(18, 2)), 7, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (320, CAST(0.10 AS Decimal(18, 2)), 7, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (321, CAST(0.10 AS Decimal(18, 2)), 11, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (322, CAST(0.05 AS Decimal(18, 2)), 11, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (323, CAST(0.05 AS Decimal(18, 2)), 11, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (324, CAST(0.05 AS Decimal(18, 2)), 11, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (325, CAST(0.15 AS Decimal(18, 2)), 11, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (326, CAST(0.10 AS Decimal(18, 2)), 11, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (327, CAST(0.25 AS Decimal(18, 2)), 11, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (328, CAST(0.00 AS Decimal(18, 2)), 12, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (329, CAST(0.00 AS Decimal(18, 2)), 12, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (330, CAST(0.15 AS Decimal(18, 2)), 14, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (331, CAST(0.00 AS Decimal(18, 2)), 14, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (332, CAST(0.25 AS Decimal(18, 2)), 56, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (333, CAST(0.10 AS Decimal(18, 2)), 56, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (334, CAST(0.10 AS Decimal(18, 2)), 56, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (335, CAST(0.10 AS Decimal(18, 2)), 56, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (336, CAST(0.05 AS Decimal(18, 2)), 56, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (337, CAST(0.05 AS Decimal(18, 2)), 56, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (338, CAST(0.15 AS Decimal(18, 2)), 56, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (339, CAST(0.05 AS Decimal(18, 2)), 56, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (340, CAST(0.05 AS Decimal(18, 2)), 11, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (341, CAST(0.10 AS Decimal(18, 2)), 7, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (342, CAST(0.05 AS Decimal(18, 2)), 11, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (343, CAST(0.15 AS Decimal(18, 2)), 11, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (344, CAST(0.10 AS Decimal(18, 2)), 7, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (345, CAST(0.10 AS Decimal(18, 2)), 7, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (346, CAST(0.10 AS Decimal(18, 2)), 7, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (347, CAST(0.10 AS Decimal(18, 2)), 7, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (348, CAST(0.00 AS Decimal(18, 2)), 8, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (349, CAST(0.00 AS Decimal(18, 2)), 8, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (350, CAST(0.10 AS Decimal(18, 2)), 9, 3)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (351, CAST(0.00 AS Decimal(18, 2)), 9, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (352, CAST(0.10 AS Decimal(18, 2)), 9, 5)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (353, CAST(0.05 AS Decimal(18, 2)), 9, 6)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (354, CAST(0.10 AS Decimal(18, 2)), 9, 7)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (355, CAST(0.10 AS Decimal(18, 2)), 9, 8)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (356, CAST(0.10 AS Decimal(18, 2)), 9, 9)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (357, CAST(0.10 AS Decimal(18, 2)), 9, 10)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (358, CAST(0.10 AS Decimal(18, 2)), 9, 11)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (359, CAST(0.10 AS Decimal(18, 2)), 9, 12)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (360, CAST(0.15 AS Decimal(18, 2)), 9, 13)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (361, CAST(0.00 AS Decimal(18, 2)), 10, 1)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (362, CAST(0.00 AS Decimal(18, 2)), 10, 2)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (363, CAST(0.00 AS Decimal(18, 2)), 11, 4)
INSERT [dbo].[CriteriaWeights] ([Id], [Weight], [LevelCriteriaGroupId], [CriteriaId]) VALUES (364, CAST(0.00 AS Decimal(18, 2)), 1, 1)
SET IDENTITY_INSERT [dbo].[CriteriaWeights] OFF
SET IDENTITY_INSERT [dbo].[Cycles] ON 

INSERT [dbo].[Cycles] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Date], [Month], [Description]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'H2', 1, 7, NULL)
INSERT [dbo].[Cycles] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Date], [Month], [Description]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'H1', 7, 1, NULL)
SET IDENTITY_INSERT [dbo].[Cycles] OFF
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'85cea027-1a35-4233-b922-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'051137e7-acb5-4a03-b923-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'87ec1b5d-24a1-4bed-b924-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'718f9e1e-692d-424c-b925-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'b9ce3d50-8acc-4c3d-b926-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'3da77fbb-1f18-4702-b927-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'9a92ce42-7ac0-49b3-b928-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'2a1bf690-56be-4616-b929-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'e4c2735c-ae01-4872-b92a-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'64e0fad6-cb03-48ea-b92b-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'048239fd-3330-4cde-b92c-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'314e732b-754d-430d-b92d-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'aea18990-2fed-430f-b92e-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'8304dec9-e8cb-45f1-b92f-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'6e8aaae5-eeef-40bf-b930-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'c230823e-fe45-43e7-b931-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'ed1e46c2-6162-42f3-b932-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'70112dd4-c5b9-478d-b933-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'ee6c2f4f-7d47-4b8b-b934-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'13a42d64-9ad9-40a3-b935-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'768516b8-d4c2-4af3-b936-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'3770f4f3-bc18-4fae-b937-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'78231a6b-8042-4e42-b938-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'a33f4129-902e-43f9-b939-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'd2abebf8-e6e1-485a-b93a-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'225da51a-bcd2-42aa-b93b-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'c2d13c4d-de20-4d31-b93c-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'ccf28f32-d658-4164-b93d-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'db9f75a5-4a75-4452-b93e-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'21a9337b-14b0-4ed3-b93f-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'2043e0f8-f342-4d10-b940-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'b3370f06-7556-4299-b941-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'0812b971-79ec-4725-b942-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'193b5277-8bb2-4e32-b943-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'2bec9f20-653b-4c47-b944-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'3e470405-3112-47e0-b945-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'73f30349-b95c-4e9b-b946-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'7fc8c53f-1148-4b90-b947-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'baf6d9bc-0b9c-4074-b948-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'1900089a-745d-48ee-b949-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'a8d2732b-c464-4334-b94a-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'2459ba9d-ecd1-4a86-b94b-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'b2162dd8-5129-4031-b94c-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'4fd278d4-3f6b-470c-b94d-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'de2ffa90-8dfc-4805-b94e-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'c71bb72d-c919-4cb5-b94f-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'd83e85fd-03a2-4599-b950-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'e7eab004-d2e7-47c1-b951-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'823bdb40-e3ee-4afe-b952-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'7951c3e0-fa7c-420f-b953-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'c1db3843-1c2d-4e78-b954-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'6522121e-e643-4cc6-b955-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'63a7ba28-fd19-48ab-b956-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'84385a96-5de1-4962-b957-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'91512936-93f2-41ca-b958-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (6, N'24d5d532-02cd-46a5-b959-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'd1f95517-d069-4d4c-b95a-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'5c97d12d-cdf9-4641-b95b-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'622eadd9-8cb0-488f-b95c-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'b6f9389b-2145-444e-b95d-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'52b16e87-3f59-4ffc-b95e-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'c96b1c56-d9d0-4ae5-b95f-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'd04c3ff9-5130-48ef-b960-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'e0c28fc1-7465-4ee9-b961-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'2557dbb8-053b-4f93-b962-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'f30c1565-8266-4bb7-b963-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'32241301-34ff-499c-b964-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'd84f6f37-6b96-460f-b965-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (5, N'e8cc739f-9954-43ee-b966-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (4, N'4e334b96-efca-484a-b967-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'b5aa397c-6571-422a-b968-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'1ce772e5-aa38-48e8-b969-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'240a685b-9a8b-440a-b96a-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'2a2ced90-1b9e-413b-b96b-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'b6004516-a221-4c5a-b96c-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'59398bb9-aa38-47db-b96d-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'8b1be196-1e9f-4f25-b96e-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'9a5f9b08-4413-4bec-b96f-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'70d0949d-ff32-4651-b970-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'c295fbd8-bc2f-4e91-b971-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'0f81bf85-f3ee-4744-b972-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'7850add0-cb56-479e-b973-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'ba124980-ac9e-4397-b974-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'93ce3b9f-7674-4030-b975-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'cdfcb208-ee6a-4288-b976-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'c47245ea-d747-4b43-b977-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'c2ad0081-7336-4b9d-b978-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'3d2784d5-8ddc-494e-b979-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'9c31a8b8-a864-4bc7-b97a-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'c9411c24-caf9-480c-b97b-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'1e630cd8-e908-4be9-b97c-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'd9e473a8-134b-4c4b-b97d-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'70652f4d-c823-4740-b97e-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'04c208c6-ec32-47e8-b97f-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'83b7fca1-00b3-451d-b980-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (1, N'68b5b328-a908-4799-b981-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (2, N'569933a5-4019-4ac1-b982-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'd489111e-a390-4212-b983-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'12f0f5f4-d2e6-41c7-b984-08d742677ae3', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'7ebc8676-073a-4801-a6fc-72737782aa6f', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[EmployeeProjects] ([ProjectId], [UserId], [JoiningDate]) VALUES (3, N'bf4ca84b-f41c-492b-ba89-dffd8d8e8b0b', CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
SET IDENTITY_INSERT [dbo].[LevelCriteriaGroups] ON 

INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (1, CAST(0.00 AS Decimal(18, 2)), 1, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (2, CAST(0.00 AS Decimal(18, 2)), 12, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (3, CAST(1.00 AS Decimal(18, 2)), 12, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (4, CAST(0.30 AS Decimal(18, 2)), 11, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (5, CAST(0.70 AS Decimal(18, 2)), 11, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (6, CAST(0.30 AS Decimal(18, 2)), 10, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (7, CAST(0.70 AS Decimal(18, 2)), 10, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (8, CAST(0.00 AS Decimal(18, 2)), 9, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (9, CAST(1.00 AS Decimal(18, 2)), 9, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (10, CAST(0.00 AS Decimal(18, 2)), 8, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (11, CAST(1.00 AS Decimal(18, 2)), 8, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (12, CAST(0.00 AS Decimal(18, 2)), 7, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (13, CAST(0.70 AS Decimal(18, 2)), 13, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (14, CAST(1.00 AS Decimal(18, 2)), 7, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (15, CAST(0.70 AS Decimal(18, 2)), 6, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (16, CAST(0.30 AS Decimal(18, 2)), 5, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (17, CAST(0.70 AS Decimal(18, 2)), 5, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (18, CAST(0.30 AS Decimal(18, 2)), 4, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (19, CAST(0.70 AS Decimal(18, 2)), 4, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (20, CAST(0.30 AS Decimal(18, 2)), 3, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (21, CAST(0.70 AS Decimal(18, 2)), 3, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (22, CAST(0.00 AS Decimal(18, 2)), 2, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (23, CAST(1.00 AS Decimal(18, 2)), 2, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (24, CAST(0.00 AS Decimal(18, 2)), 27, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (25, CAST(1.00 AS Decimal(18, 2)), 27, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (26, CAST(0.30 AS Decimal(18, 2)), 6, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (27, CAST(0.30 AS Decimal(18, 2)), 13, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (28, CAST(0.70 AS Decimal(18, 2)), 14, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (29, CAST(0.30 AS Decimal(18, 2)), 14, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (30, CAST(0.00 AS Decimal(18, 2)), 1, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (31, CAST(0.30 AS Decimal(18, 2)), 26, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (32, CAST(0.70 AS Decimal(18, 2)), 26, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (33, CAST(0.00 AS Decimal(18, 2)), 25, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (34, CAST(1.00 AS Decimal(18, 2)), 25, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (35, CAST(0.00 AS Decimal(18, 2)), 24, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (36, CAST(1.00 AS Decimal(18, 2)), 24, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (37, CAST(0.00 AS Decimal(18, 2)), 23, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (38, CAST(1.00 AS Decimal(18, 2)), 23, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (39, CAST(0.00 AS Decimal(18, 2)), 22, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (40, CAST(1.00 AS Decimal(18, 2)), 22, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (41, CAST(0.30 AS Decimal(18, 2)), 21, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (42, CAST(0.70 AS Decimal(18, 2)), 21, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (43, CAST(0.00 AS Decimal(18, 2)), 20, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (44, CAST(1.00 AS Decimal(18, 2)), 20, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (45, CAST(0.00 AS Decimal(18, 2)), 19, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (46, CAST(1.00 AS Decimal(18, 2)), 19, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (47, CAST(0.00 AS Decimal(18, 2)), 18, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (48, CAST(1.00 AS Decimal(18, 2)), 18, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (49, CAST(0.00 AS Decimal(18, 2)), 17, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (50, CAST(1.00 AS Decimal(18, 2)), 17, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (51, CAST(0.30 AS Decimal(18, 2)), 16, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (52, CAST(0.70 AS Decimal(18, 2)), 16, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (53, CAST(0.30 AS Decimal(18, 2)), 15, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (54, CAST(0.70 AS Decimal(18, 2)), 15, 2)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (55, CAST(0.00 AS Decimal(18, 2)), 28, 1)
INSERT [dbo].[LevelCriteriaGroups] ([Id], [Weight], [LevelId], [CriteriaGroupId]) VALUES (56, CAST(1.00 AS Decimal(18, 2)), 28, 2)
SET IDENTITY_INSERT [dbo].[LevelCriteriaGroups] OFF
SET IDENTITY_INSERT [dbo].[Levels] ON 

INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (1, N'Chief Executive Officer', N'CEO')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (2, N'Senior Software Engineer', N'SSE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (3, N'Technical Leader', N'TL')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (4, N'Technical Manager', N'TM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (5, N'Software Manager', N'SM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (6, N'Solution Architecture', N'SA')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (7, N'Associate Test Engineer', N'ATS')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (8, N'Test Engineer', N'TS')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (9, N'Senior Testing Engineer', N'STS')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (10, N'QA Lead', N'QAL')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (11, N'Test Lead', N'TSL')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (12, N'Associate Project Assistant', N'APA')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (13, N'Associate Project Manager', N'APM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (14, N'Project Manager', N'PM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (15, N'Senior Project Manager', N'SPM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (16, N'Delivery Manager', N'DM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (17, N'UX-UI designer', N'UXD')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (18, N'Business Analyst ', N'BA')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (19, N'Office Manager', N'OM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (20, N'Accountant ', N'ACC')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (21, N'Accounting Manager', N'ACM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (22, N'IT Manager', N'ITM')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (23, N'Associate HR Executive', N'AHE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (24, N'HR Executive', N'HE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (25, N'Senior HR Executive', N'SHE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (26, N'HR Manager', N'HMR')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (27, N'Software Engineer', N'SE')
INSERT [dbo].[Levels] ([Id], [Title], [Abbre]) VALUES (28, N'Associate Software Engineer', N'ASE')
SET IDENTITY_INSERT [dbo].[Levels] OFF
SET IDENTITY_INSERT [dbo].[Lookups] ON 

INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (1, N'LIST_TRAINING_PROGRAMS', N'Attention to Detail')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (2, N'LIST_TRAINING_PROGRAMS', N'Azure Machine Learning')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (3, N'LIST_TRAINING_PROGRAMS', N'Xamarin')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (4, N'LIST_TRAINING_PROGRAMS', N'JavaScript')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (5, N'LIST_TRAINING_PROGRAMS', N'Flutter')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (6, N'LIST_TRAINING_PROGRAMS', N'Python')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (7, N'LIST_TRAINING_PROGRAMS', N'Golang')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (8, N'LIST_TRAINING_PROGRAMS', N'Presentation Skills')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (9, N'LIST_TRAINING_PROGRAMS', N'Resolving conflict with peers')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (10, N'LIST_TRAINING_PROGRAMS', N'Time management')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (11, N'LIST_TRAINING_PROGRAMS', N'Vue.js')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (12, N'LIST_TRAINING_PROGRAMS', N'Angular')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (13, N'LIST_TRAINING_PROGRAMS', N'Oracle')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (14, N'LIST_TRAINING_PROGRAMS', N'MongoDB')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (15, N'LIST_TRAINING_PROGRAMS', N'Communication')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (16, N'LIST_TRAINING_PROGRAMS', N'Teamwork')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (17, N'LIST_TRAINING_PROGRAMS', N'Adaptability')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (18, N'LIST_TRAINING_PROGRAMS', N'Problem-Solving')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (19, N'LIST_TRAINING_PROGRAMS', N'Creativity')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (20, N'LIST_TRAINING_PROGRAMS', N'Interpersonal Skills')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (21, N'LIST_TRAINING_PROGRAMS', N'Leadership')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (22, N'LIST_TRAINING_PROGRAMS', N'Node.js')
INSERT [dbo].[Lookups] ([Id], [Key], [Value]) VALUES (23, N'LIST_TRAINING_PROGRAMS', N'.NET Core')
SET IDENTITY_INSERT [dbo].[Lookups] OFF
SET IDENTITY_INSERT [dbo].[PerformanceReviewCycles] ON 

INSERT [dbo].[PerformanceReviewCycles] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Year], [Description], [CycleId], [Status], [AppraisalModeId]) VALUES (1, CAST(N'2019-09-26T16:59:59.8778318' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, N'Annual Review H1 2019', 2019, N'hello world', 2, 20, 3)
INSERT [dbo].[PerformanceReviewCycles] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Year], [Description], [CycleId], [Status], [AppraisalModeId]) VALUES (2, CAST(N'2019-09-26T17:07:17.7282968' AS DateTime2), N'00000000-0000-0000-0000-000000000000', NULL, NULL, 0, N'Special H1 2019', 2019, N'hello', 2, 20, 1)
SET IDENTITY_INSERT [dbo].[PerformanceReviewCycles] OFF
SET IDENTITY_INSERT [dbo].[Projects] ON 

INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'General - SUP', N'PRJSUP', 1)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'General - Adj', N'PRJADJ', 2)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'General - LOG', N'PRJLOG', 3)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'General - App', N'PRJAPP', 4)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'General - Amb', N'PRJAMB', 5)
INSERT [dbo].[Projects] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [TeamId]) VALUES (6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'General - F4', N'PRJF4', 6)
SET IDENTITY_INSERT [dbo].[Projects] OFF
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (1, 1, CAST(N'2019-09-26T00:00:03.3910000' AS DateTime2), CAST(N'2019-09-29T00:00:03.3910000' AS DateTime2), 10)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (1, 2, CAST(N'2019-09-30T00:00:03.3910000' AS DateTime2), CAST(N'2019-10-06T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (1, 3, CAST(N'2019-10-07T00:00:03.3910000' AS DateTime2), CAST(N'2019-10-13T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (1, 4, CAST(N'2019-10-14T00:00:03.3910000' AS DateTime2), CAST(N'2019-10-17T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (1, 5, CAST(N'2019-10-18T00:00:03.3910000' AS DateTime2), CAST(N'2019-10-25T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (1, 6, CAST(N'2019-10-26T00:00:03.3910000' AS DateTime2), CAST(N'2019-10-29T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (1, 7, CAST(N'2019-10-30T00:00:03.3910000' AS DateTime2), CAST(N'2019-11-09T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (1, 8, CAST(N'2019-11-10T00:00:03.3910000' AS DateTime2), CAST(N'2019-11-15T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (2, 1, CAST(N'2019-09-26T00:00:03.3910000' AS DateTime2), CAST(N'2019-09-29T00:00:03.3910000' AS DateTime2), 10)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (2, 2, CAST(N'2019-09-30T00:00:03.3910000' AS DateTime2), CAST(N'2019-10-06T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (2, 3, CAST(N'2019-10-07T00:00:03.3910000' AS DateTime2), CAST(N'2019-10-13T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (2, 4, CAST(N'2019-10-14T00:00:03.3910000' AS DateTime2), CAST(N'2019-10-17T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (2, 5, CAST(N'2019-10-18T00:00:03.3910000' AS DateTime2), CAST(N'2019-10-25T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (2, 6, CAST(N'2019-10-26T00:00:03.3910000' AS DateTime2), CAST(N'2019-10-29T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (2, 7, CAST(N'2019-10-30T00:00:03.3910000' AS DateTime2), CAST(N'2019-11-09T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (2, 8, CAST(N'2019-11-10T00:00:03.3910000' AS DateTime2), CAST(N'2019-11-15T00:00:03.3910000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (3, 1, CAST(N'2019-09-26T00:07:23.3320000' AS DateTime2), CAST(N'2019-09-29T00:07:23.3320000' AS DateTime2), 10)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (3, 2, CAST(N'2019-09-30T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-06T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (3, 3, CAST(N'2019-10-07T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-13T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (3, 4, CAST(N'2019-10-14T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-17T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (3, 5, CAST(N'2019-10-18T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-25T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (3, 6, CAST(N'2019-10-26T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-29T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (3, 7, CAST(N'2019-10-30T00:07:23.3320000' AS DateTime2), CAST(N'2019-11-09T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (3, 8, CAST(N'2019-11-10T00:07:23.3320000' AS DateTime2), CAST(N'2019-11-15T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (4, 1, CAST(N'2019-09-26T00:07:23.3320000' AS DateTime2), CAST(N'2019-09-29T00:07:23.3320000' AS DateTime2), 10)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (4, 2, CAST(N'2019-09-30T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-06T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (4, 3, CAST(N'2019-10-07T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-13T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (4, 4, CAST(N'2019-10-14T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-17T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (4, 5, CAST(N'2019-10-18T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-25T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (4, 6, CAST(N'2019-10-26T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-29T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (4, 7, CAST(N'2019-10-30T00:07:23.3320000' AS DateTime2), CAST(N'2019-11-09T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (4, 8, CAST(N'2019-11-10T00:07:23.3320000' AS DateTime2), CAST(N'2019-11-15T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (5, 1, CAST(N'2019-09-26T00:07:23.3320000' AS DateTime2), CAST(N'2019-09-29T00:07:23.3320000' AS DateTime2), 10)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (5, 2, CAST(N'2019-09-30T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-06T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (5, 3, CAST(N'2019-10-07T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-13T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (5, 4, CAST(N'2019-10-14T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-17T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (5, 5, CAST(N'2019-10-18T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-25T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (5, 6, CAST(N'2019-10-26T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-29T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (5, 7, CAST(N'2019-10-30T00:07:23.3320000' AS DateTime2), CAST(N'2019-11-09T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (5, 8, CAST(N'2019-11-10T00:07:23.3320000' AS DateTime2), CAST(N'2019-11-15T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (6, 1, CAST(N'2019-09-26T00:07:23.3320000' AS DateTime2), CAST(N'2019-09-29T00:07:23.3320000' AS DateTime2), 10)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (6, 2, CAST(N'2019-09-30T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-06T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (6, 3, CAST(N'2019-10-07T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-13T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (6, 4, CAST(N'2019-10-14T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-17T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (6, 5, CAST(N'2019-10-18T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-25T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (6, 6, CAST(N'2019-10-26T00:07:23.3320000' AS DateTime2), CAST(N'2019-10-29T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (6, 7, CAST(N'2019-10-30T00:07:23.3320000' AS DateTime2), CAST(N'2019-11-09T00:07:23.3320000' AS DateTime2), 0)
INSERT [dbo].[ReviewTimelines] ([AppraisalId], [StageId], [FromDate], [ToDate], [Status]) VALUES (6, 8, CAST(N'2019-11-10T00:07:23.3320000' AS DateTime2), CAST(N'2019-11-15T00:07:23.3320000' AS DateTime2), 0)
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([Id], [Name]) VALUES (1, N'System Admin')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (2, N'HR')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (3, N'CEO')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (4, N'DM')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (5, N'PM')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (6, N'TL')
INSERT [dbo].[Roles] ([Id], [Name]) VALUES (7, N'Employee')
SET IDENTITY_INSERT [dbo].[Roles] OFF
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (1, N'Self rating', NULL, 3)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (2, N'TL review', NULL, 6)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (3, N'PM review', NULL, 6)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (4, N'DM review ', NULL, 3)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (5, N'HR consolidate', NULL, 7)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (6, N'CEO', NULL, 3)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (7, N'Sharing', NULL, 10)
INSERT [dbo].[Stages] ([Id], [Name], [Description], [Days]) VALUES (8, N'Employee Sign-off', NULL, 5)
SET IDENTITY_INSERT [dbo].[Teams] ON 

INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Supporting', N'SUP', 1)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Adjuno', N'ADJ', 2)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Logistics', N'LOG', 3)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'AppDev', N'APP', 4)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'Amblique', N'AMB', 4)
INSERT [dbo].[Teams] ([Id], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [Name], [Abbre], [BusinessUnitId]) VALUES (6, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), NULL, NULL, NULL, 0, N'FactFour', N'F4', 4)
SET IDENTITY_INSERT [dbo].[Teams] OFF
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'0e416fec-b6c6-4af2-b921-08d742677ae3', 1, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'85cea027-1a35-4233-b922-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'051137e7-acb5-4a03-b923-08d742677ae3', 4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'87ec1b5d-24a1-4bed-b924-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'718f9e1e-692d-424c-b925-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b9ce3d50-8acc-4c3d-b926-08d742677ae3', 9, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'3da77fbb-1f18-4702-b927-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'9a92ce42-7ac0-49b3-b928-08d742677ae3', 9, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2a1bf690-56be-4616-b929-08d742677ae3', 13, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'e4c2735c-ae01-4872-b92a-08d742677ae3', 9, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'64e0fad6-cb03-48ea-b92b-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'048239fd-3330-4cde-b92c-08d742677ae3', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'314e732b-754d-430d-b92d-08d742677ae3', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'aea18990-2fed-430f-b92e-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'8304dec9-e8cb-45f1-b92f-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'6e8aaae5-eeef-40bf-b930-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c230823e-fe45-43e7-b931-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'ed1e46c2-6162-42f3-b932-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'70112dd4-c5b9-478d-b933-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'ee6c2f4f-7d47-4b8b-b934-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'13a42d64-9ad9-40a3-b935-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'768516b8-d4c2-4af3-b936-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'3770f4f3-bc18-4fae-b937-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'78231a6b-8042-4e42-b938-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'a33f4129-902e-43f9-b939-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd2abebf8-e6e1-485a-b93a-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'225da51a-bcd2-42aa-b93b-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c2d13c4d-de20-4d31-b93c-08d742677ae3', 4, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'ccf28f32-d658-4164-b93d-08d742677ae3', 10, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'db9f75a5-4a75-4452-b93e-08d742677ae3', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'21a9337b-14b0-4ed3-b93f-08d742677ae3', 16, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2043e0f8-f342-4d10-b940-08d742677ae3', 19, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b3370f06-7556-4299-b941-08d742677ae3', 26, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'0812b971-79ec-4725-b942-08d742677ae3', 21, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'193b5277-8bb2-4e32-b943-08d742677ae3', 22, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2bec9f20-653b-4c47-b944-08d742677ae3', 25, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'3e470405-3112-47e0-b945-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'73f30349-b95c-4e9b-b946-08d742677ae3', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'7fc8c53f-1148-4b90-b947-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'baf6d9bc-0b9c-4074-b948-08d742677ae3', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'1900089a-745d-48ee-b949-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'a8d2732b-c464-4334-b94a-08d742677ae3', 16, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2459ba9d-ecd1-4a86-b94b-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b2162dd8-5129-4031-b94c-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'4fd278d4-3f6b-470c-b94d-08d742677ae3', 5, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'de2ffa90-8dfc-4805-b94e-08d742677ae3', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c71bb72d-c919-4cb5-b94f-08d742677ae3', 11, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd83e85fd-03a2-4599-b950-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'e7eab004-d2e7-47c1-b951-08d742677ae3', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'823bdb40-e3ee-4afe-b952-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'7951c3e0-fa7c-420f-b953-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c1db3843-1c2d-4e78-b954-08d742677ae3', 3, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'6522121e-e643-4cc6-b955-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'63a7ba28-fd19-48ab-b956-08d742677ae3', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'84385a96-5de1-4962-b957-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'91512936-93f2-41ca-b958-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'24d5d532-02cd-46a5-b959-08d742677ae3', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd1f95517-d069-4d4c-b95a-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'5c97d12d-cdf9-4641-b95b-08d742677ae3', 16, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'622eadd9-8cb0-488f-b95c-08d742677ae3', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b6f9389b-2145-444e-b95d-08d742677ae3', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'52b16e87-3f59-4ffc-b95e-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c96b1c56-d9d0-4ae5-b95f-08d742677ae3', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd04c3ff9-5130-48ef-b960-08d742677ae3', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'e0c28fc1-7465-4ee9-b961-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2557dbb8-053b-4f93-b962-08d742677ae3', 14, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'f30c1565-8266-4bb7-b963-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'32241301-34ff-499c-b964-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd84f6f37-6b96-460f-b965-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'e8cc739f-9954-43ee-b966-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'4e334b96-efca-484a-b967-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b5aa397c-6571-422a-b968-08d742677ae3', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'1ce772e5-aa38-48e8-b969-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'240a685b-9a8b-440a-b96a-08d742677ae3', 17, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'2a2ced90-1b9e-413b-b96b-08d742677ae3', 20, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'b6004516-a221-4c5a-b96c-08d742677ae3', 12, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'59398bb9-aa38-47db-b96d-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'8b1be196-1e9f-4f25-b96e-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'9a5f9b08-4413-4bec-b96f-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'70d0949d-ff32-4651-b970-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c295fbd8-bc2f-4e91-b971-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'0f81bf85-f3ee-4744-b972-08d742677ae3', 23, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'7850add0-cb56-479e-b973-08d742677ae3', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'ba124980-ac9e-4397-b974-08d742677ae3', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'93ce3b9f-7674-4030-b975-08d742677ae3', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'cdfcb208-ee6a-4288-b976-08d742677ae3', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c47245ea-d747-4b43-b977-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c2ad0081-7336-4b9d-b978-08d742677ae3', 18, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'3d2784d5-8ddc-494e-b979-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'9c31a8b8-a864-4bc7-b97a-08d742677ae3', 2, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'c9411c24-caf9-480c-b97b-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'1e630cd8-e908-4be9-b97c-08d742677ae3', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd9e473a8-134b-4c4b-b97d-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'70652f4d-c823-4740-b97e-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'04c208c6-ec32-47e8-b97f-08d742677ae3', 28, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'83b7fca1-00b3-451d-b980-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'68b5b328-a908-4799-b981-08d742677ae3', 24, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'569933a5-4019-4ac1-b982-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'd489111e-a390-4212-b983-08d742677ae3', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'12f0f5f4-d2e6-41c7-b984-08d742677ae3', 8, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
GO
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'7ebc8676-073a-4801-a6fc-72737782aa6f', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[UserLevels] ([UserId], [LevelId], [PromotionDate]) VALUES (N'bf4ca84b-f41c-492b-ba89-dffd8d8e8b0b', 27, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2))
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'0e416fec-b6c6-4af2-b921-08d742677ae3', N'G10000', N'matt@groovetechnology.com', N'Matt Long', N'Matt', N'Long', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'85cea027-1a35-4233-b922-08d742677ae3', N'G00001', N'tan.truong@groovetechnology.com', N'Tan Truong', N'Tan', N'Truong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'051137e7-acb5-4a03-b923-08d742677ae3', N'G00002', N'thuan.ha@groovetechnology.com', N'Thuan Ha', N'Thuan', N'Ha', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'87ec1b5d-24a1-4bed-b924-08d742677ae3', N'G00004', N'tuan.hoang@groovetechnology.com', N'Tuan Hoang', N'Tuan', N'Hoang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-18T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'718f9e1e-692d-424c-b925-08d742677ae3', N'G00005', N'tho.lai@groovetechnology.com', N'Tho Lai', N'Tho', N'Lai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-18T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b9ce3d50-8acc-4c3d-b926-08d742677ae3', N'G00007', N'hanh.tran@groovetechnology.com', N'Hanh Tran', N'Hanh', N'Tran', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'3da77fbb-1f18-4702-b927-08d742677ae3', N'G00008', N'an.ha@groovetechnology.com', N'An Ha', N'An', N'Ha', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'9a92ce42-7ac0-49b3-b928-08d742677ae3', N'G00010', N'duy.nguyen@groovetechnology.com', N'Duy Nguyen', N'Duy', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2a1bf690-56be-4616-b929-08d742677ae3', N'G00012', N'my.trinh@groovetechnology.com', N'My Trinh', N'My', N'Trinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'e4c2735c-ae01-4872-b92a-08d742677ae3', N'G00013', N'hai.nguyen@groovetechnology.com', N'Hai Nguyen', N'Hai', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'64e0fad6-cb03-48ea-b92b-08d742677ae3', N'G00014', N'sanh.nguyen@groovetechnology.com', N'Sanh Nguyen', N'Sanh', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'048239fd-3330-4cde-b92c-08d742677ae3', N'G00015', N'loan.ngo@groovetechnology.com', N'Loan Ngo', N'Loan', N'Ngo', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'314e732b-754d-430d-b92d-08d742677ae3', N'G00016', N'duong.tiet@groovetechnology.com', N'Duong Tiet', N'Duong', N'Tiet', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'aea18990-2fed-430f-b92e-08d742677ae3', N'G00017', N'thanh.pham@groovetechnology.com', N'Thanh Pham', N'Thanh', N'Pham', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'8304dec9-e8cb-45f1-b92f-08d742677ae3', N'G00018', N'van.tran@groovetechnology.com', N'Van Tran', N'Van', N'Tran', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'6e8aaae5-eeef-40bf-b930-08d742677ae3', N'G00019', N'tri.nguyen@groovetechnology.com', N'Tri Nguyen', N'Tri', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c230823e-fe45-43e7-b931-08d742677ae3', N'G00020', N'luong.pham@groovetechnology.com', N'Luong Pham', N'Luong', N'Pham', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'ed1e46c2-6162-42f3-b932-08d742677ae3', N'G00021', N'duc.quach@groovetechnology.com', N'Duc Quach', N'Duc', N'Quach', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'70112dd4-c5b9-478d-b933-08d742677ae3', N'G00022', N'dung.nguyen@groovetechnology.com', N'Dung Nguyen', N'Dung', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'ee6c2f4f-7d47-4b8b-b934-08d742677ae3', N'G00023', N'dung.le@groovetechnology.com', N'Dung Le', N'Dung', N'Le', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'13a42d64-9ad9-40a3-b935-08d742677ae3', N'G00024', N'phung.tran@groovetechnology.com', N'Phung Tran', N'Phung', N'Tran', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'768516b8-d4c2-4af3-b936-08d742677ae3', N'G00025', N'hieu.le@groovetechnology.com', N'Hieu Le', N'Hieu', N'Le', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'3770f4f3-bc18-4fae-b937-08d742677ae3', N'G00027', N'dat.phung@groovetechnology.com', N'Dat Phung', N'Dat', N'Phung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'78231a6b-8042-4e42-b938-08d742677ae3', N'G00028', N'phu.phung@groovetechnology.com', N'Phu Phung', N'Phu', N'Phung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-22T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'a33f4129-902e-43f9-b939-08d742677ae3', N'G00029', N'binh.doan@groovetechnology.com', N'Binh Doan', N'Binh', N'Doan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-22T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd2abebf8-e6e1-485a-b93a-08d742677ae3', N'G00030', N'thin.nguyen@groovetechnology.com', N'Thin Nguyen', N'Thin', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-22T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'225da51a-bcd2-42aa-b93b-08d742677ae3', N'G00032', N'huy.luu@groovetechnology.com', N'Huy Luu', N'Huy', N'Luu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-22T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c2d13c4d-de20-4d31-b93c-08d742677ae3', N'G00033', N'linh.vu@groovetechnology.com', N'Linh Vu', N'Linh', N'Vu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'ccf28f32-d658-4164-b93d-08d742677ae3', N'G00034', N'chung.ly@groovetechnology.com', N'Chung Ly', N'Chung', N'Ly', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'db9f75a5-4a75-4452-b93e-08d742677ae3', N'G00037', N'tuyen.nguyen@groovetechnology.com', N'Tuyen Nguyen', N'Tuyen', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-11-24T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'21a9337b-14b0-4ed3-b93f-08d742677ae3', N'G00040', N'victor.tran@groovetechnology.com', N'Viet Tran', N'Viet', N'Tran', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-12-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2043e0f8-f342-4d10-b940-08d742677ae3', N'G00041', N'lien.chung@groovetechnology.com', N'Lien Chung', N'Lien', N'Chung', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2016-12-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b3370f06-7556-4299-b941-08d742677ae3', N'G00042', N'mai.duong@groovetechnology.com', N'Mai Duong', N'Mai', N'Duong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'0812b971-79ec-4725-b942-08d742677ae3', N'G00043', N'phuoc.hoang@groovetechnology.com', N'Phuoc Hoang', N'Phuoc', N'Hoang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'193b5277-8bb2-4e32-b943-08d742677ae3', N'G00044', N'ba.nguyen@groovetechnology.com', N'Ba Nguyen', N'Ba', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2bec9f20-653b-4c47-b944-08d742677ae3', N'G00045', N'anh.nguyen@groovetechnology.com', N'Anh Nguyen', N'Anh', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-09T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'3e470405-3112-47e0-b945-08d742677ae3', N'G00046', N'tin.truong@groovetechnology.com', N'Tin Truong', N'Tin', N'Truong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-01-16T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'73f30349-b95c-4e9b-b946-08d742677ae3', N'G00049', N'hieu.tran@groovetechnology.com', N'Hieu Tran', N'Hieu', N'Tran', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'7fc8c53f-1148-4b90-b947-08d742677ae3', N'G00051', N'huyen.nguyent@groovetechnology.com', N'Huyen Nguyen', N'Huyen', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'baf6d9bc-0b9c-4074-b948-08d742677ae3', N'G00052', N'truc.ha@groovetechnology.com', N'Truc Ha', N'Truc', N'Ha', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'1900089a-745d-48ee-b949-08d742677ae3', N'G00053', N'mai.nguyen@groovetechnology.com', N'Mai Nguyen', N'Mai', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-03-06T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'a8d2732b-c464-4334-b94a-08d742677ae3', N'G00055', N'thao.nguyen@groovetechnology.com', N'Thao Nguyen', N'Thao', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-03-15T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2459ba9d-ecd1-4a86-b94b-08d742677ae3', N'G00056', N'tuan.nguyen@groovetechnology.com', N'Tuan Nguyen', N'Tuan', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-03-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b2162dd8-5129-4031-b94c-08d742677ae3', N'G00058', N'hai.le@groovetechnology.com', N'Hai Le', N'Hai', N'Le', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-03-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'4fd278d4-3f6b-470c-b94d-08d742677ae3', N'G00059', N'tri.le@groovetechnology.com', N'Tri Le', N'Tri', N'Le', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-04-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'de2ffa90-8dfc-4805-b94e-08d742677ae3', N'G00063', N'nhi.truong@groovetechnology.com', N'Nhi Truong', N'Nhi', N'Truong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-04-10T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c71bb72d-c919-4cb5-b94f-08d742677ae3', N'G00064', N'mai.nguyentn@groovetechnology.com', N'Mai Nguyen', N'Mai', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-04-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd83e85fd-03a2-4599-b950-08d742677ae3', N'G00066', N'khang.tran@groovetechnology.com', N'Khang Tran', N'Khang', N'Tran', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-04-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'e7eab004-d2e7-47c1-b951-08d742677ae3', N'G00069', N'dinh.vu@groovetechnology.com', N'Dinh Vu', N'Dinh', N'Vu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-05-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'823bdb40-e3ee-4afe-b952-08d742677ae3', N'G00070', N'huy.do@groovetechnology.com', N'Huy Do', N'Huy', N'Do', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-05-08T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'7951c3e0-fa7c-420f-b953-08d742677ae3', N'G00071', N'hung.nguyen@groovetechnology.com', N'Hung Nguyen', N'Hung', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-07-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c1db3843-1c2d-4e78-b954-08d742677ae3', N'G00072', N'hung.doan@groovetechnology.com', N'Hung Doan', N'Hung', N'Doan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-08-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'6522121e-e643-4cc6-b955-08d742677ae3', N'G00074', N'thinh.vo@groovetechnology.com', N'Thinh Vo', N'Thinh', N'Vo', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-09-05T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'63a7ba28-fd19-48ab-b956-08d742677ae3', N'G00079', N'dung.nguyentm@groovetechnology.com', N'Dung Nguyen', N'Dung', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-10-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'84385a96-5de1-4962-b957-08d742677ae3', N'G00083', N'vu.nguyen@groovetechnology.com', N'Vu Nguyen', N'Vu', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-10-16T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'91512936-93f2-41ca-b958-08d742677ae3', N'G00084', N'vinh.trang@groovetechnology.com', N'Vinh Trang', N'Vinh', N'Trang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-11-06T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'24d5d532-02cd-46a5-b959-08d742677ae3', N'G00087', N'trinh.nguyen@groovetechnology.com', N'Trinh Nguyen', N'Trinh', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-11-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd1f95517-d069-4d4c-b95a-08d742677ae3', N'G00088', N'vinh.trinh@groovetechnology.com', N'Vinh Trinh', N'Vinh', N'Trinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2017-11-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'5c97d12d-cdf9-4641-b95b-08d742677ae3', N'G00090', N'phuong.nguyen@groovetechnology.com', N'Phuong Nguyen', N'Phuong', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-01-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'622eadd9-8cb0-488f-b95c-08d742677ae3', N'G00093', N'dung.trann@groovetechnology.com', N'Dung Tran', N'Dung', N'Tran', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-02-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b6f9389b-2145-444e-b95d-08d742677ae3', N'G00095', N'hai.hoang@groovetechnology.com', N'Hai Hoang', N'Hai', N'Hoang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-02-21T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'52b16e87-3f59-4ffc-b95e-08d742677ae3', N'G00098', N'vuong.do@groovetechnology.com', N'Vuong Do', N'Vuong', N'Do', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-03-05T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c96b1c56-d9d0-4ae5-b95f-08d742677ae3', N'G00099', N'nam.ngo@groovetechnology.com', N'Nam Ngo', N'Nam', N'Ngo', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-03-05T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd04c3ff9-5130-48ef-b960-08d742677ae3', N'G00100', N'ngan.luong@groovetechnology.com', N'Ngan Luong', N'Ngan', N'Luong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-03-19T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'e0c28fc1-7465-4ee9-b961-08d742677ae3', N'G00103', N'chuong.le@groovetechnology.com', N'Chuong Le', N'Chuong', N'Le', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-04-09T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2557dbb8-053b-4f93-b962-08d742677ae3', N'G00108', N'minh.vu@groovetechnology.com', N'Minh Vu', N'Minh', N'Vu', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-05-15T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'f30c1565-8266-4bb7-b963-08d742677ae3', N'G00109', N'cuong.phan@groovetechnology.com', N'Cuong Phan', N'Cuong', N'Phan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-04T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'32241301-34ff-499c-b964-08d742677ae3', N'G00110', N'khoi.nguyen@groovetechnology.com', N'Khoi Nguyen', N'Khoi', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-04T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd84f6f37-6b96-460f-b965-08d742677ae3', N'G00111', N'thai.nguyen@groovetechnology.com', N'Thai Nguyen', N'Thai', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-04T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'e8cc739f-9954-43ee-b966-08d742677ae3', N'G00113', N'tin.pham@groovetechnology.com', N'Tin Pham', N'Tin', N'Pham', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-25T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'4e334b96-efca-484a-b967-08d742677ae3', N'G00114', N'van.tang@groovetechnology.com', N'Van Tang', N'Van', N'Tang', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-06-25T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b5aa397c-6571-422a-b968-08d742677ae3', N'G00116', N'truc.luc@groovetechnology.com', N'Truc Luc', N'Truc', N'Luc', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-07-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'1ce772e5-aa38-48e8-b969-08d742677ae3', N'G00117', N'phuong.nguyenduy@groovetechnology.com', N'Phuong Nguyen', N'Phuong', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-07-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'240a685b-9a8b-440a-b96a-08d742677ae3', N'G00121', N'hoa.nguyen@groovetechnology.com', N'Hoa Nguyen', N'Hoa', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-07-16T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'2a2ced90-1b9e-413b-b96b-08d742677ae3', N'G00122', N'cuc.nguyen@groovetechnology.com', N'Cuc Nguyen', N'Cuc', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-07-16T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'b6004516-a221-4c5a-b96c-08d742677ae3', N'G00123', N'quynh.ho@groovetechnology.com', N'Quynh Ho', N'Quynh', N'Ho', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-08-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'59398bb9-aa38-47db-b96d-08d742677ae3', N'G00124', N'cuong.duong@groovetechnology.com', N'Cuong Duong', N'Cuong', N'Duong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-08-06T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'8b1be196-1e9f-4f25-b96e-08d742677ae3', N'G00126', N'thien.nguyen@groovetechnology.com', N'Thien Nguyen', N'Thien', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-08-13T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'9a5f9b08-4413-4bec-b96f-08d742677ae3', N'G00127', N'tuan.thai@groovetechnology.com', N'Tuan Thai', N'Tuan', N'Thai', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-08-13T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'70d0949d-ff32-4651-b970-08d742677ae3', N'G00129', N'thanh.vong@groovetechnology.com', N'Thanh Vong', N'Thanh', N'Vong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-09-17T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c295fbd8-bc2f-4e91-b971-08d742677ae3', N'G00130', N'trung.nguyen@groovetechnology.com', N'Trung Nguyen', N'Trung', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-08T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'0f81bf85-f3ee-4744-b972-08d742677ae3', N'G00132', N'thuy.nguyenlm@groovetechnology.com', N'Thuy Nguyen', N'Thuy', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'7850add0-cb56-479e-b973-08d742677ae3', N'G00133', N'duc.doan@groovetechnology.com', N'Duc Doan', N'Duc', N'Doan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'ba124980-ac9e-4397-b974-08d742677ae3', N'G00134', N'vu.nguyenhuy@groovetechnology.com', N'Vu Nguyen Huy', N'Vu', N'Nguyen Huy', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'93ce3b9f-7674-4030-b975-08d742677ae3', N'G00135', N'dinh.nguyen@groovetechnology.com', N'Dinh Nguyen', N'Dinh', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'cdfcb208-ee6a-4288-b976-08d742677ae3', N'G00136', N'tan.trinh@groovetechnology.com', N'Tan Trinh', N'Tan', N'Trinh', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-10-23T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c47245ea-d747-4b43-b977-08d742677ae3', N'G00140', N'cuong.tran@groovetechnology.com', N'Cuong Tran', N'Cuong', N'Tran', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-11-12T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c2ad0081-7336-4b9d-b978-08d742677ae3', N'G00142', N'an.nguyen@groovetechnology.com', N'An Nguyen', N'An', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-11-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'3d2784d5-8ddc-494e-b979-08d742677ae3', N'G00143', N'thang.nguyen@groovetechnology.com', N'Thang Nguyen', N'Thang', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-11-27T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'9c31a8b8-a864-4bc7-b97a-08d742677ae3', N'G00144', N'tam.hua@groovetechnology.com', N'Tam Hua', N'Tam', N'Hua', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2018-12-24T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'c9411c24-caf9-480c-b97b-08d742677ae3', N'G00146', N'an.ho@groovetechnology.com', N'An Ho', N'An', N'Ho', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-01-02T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'1e630cd8-e908-4be9-b97c-08d742677ae3', N'G00147', N'khang.ton@groovetechnology.com', N'Khang Ton', N'Khang', N'Ton', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-01-28T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd9e473a8-134b-4c4b-b97d-08d742677ae3', N'G00157', N'hoang.ha@groovetechnology.com', N'Hoang Ha', N'Hoang', N'Ha', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-02-18T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'70652f4d-c823-4740-b97e-08d742677ae3', N'G00148', N'huy.nguyen@groovetechnology.com', N'Huy Nguyen', N'Huy', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'04c208c6-ec32-47e8-b97f-08d742677ae3', N'G00149', N'bach.nguyen@groovetechnology.com', N'Bach Nguyen', N'Bach', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-02-20T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'83b7fca1-00b3-451d-b980-08d742677ae3', N'G00150', N'thang.tran@groovetechnology.com', N'Thang Tran', N'Thang', N'Tran', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-02-25T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'68b5b328-a908-4799-b981-08d742677ae3', N'G00151', N'phong.nguyen@groovetechnology.com', N'Phong Nguyen', N'Phong', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-03-04T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'569933a5-4019-4ac1-b982-08d742677ae3', N'G00152', N'tuan.truong@groovetechnology.com', N'Tuan Truong', N'Tuan', N'Truong', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-04-01T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'd489111e-a390-4212-b983-08d742677ae3', N'G00153', N'phuoc.le@groovetechnology.com', N'Phuoc Le', N'Phuoc', N'Le', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-04-03T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'12f0f5f4-d2e6-41c7-b984-08d742677ae3', N'G00159', N'hoa.pham@groovetechnology.com', N'Hoa Pham', N'Hoa', N'Pham', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-05-20T00:00:00.0000000' AS DateTime2), 1)
GO
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'7ebc8676-073a-4801-a6fc-72737782aa6f', N'G00161', N'hien.nguyen@groovetechnology.com', N'Hien Nguyen Doan', N'Hien', N'Nguyen', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-06-19T00:00:00.0000000' AS DateTime2), 1)
INSERT [dbo].[Users] ([Id], [EmployeeId], [Email], [FullName], [FirstName], [LastName], [isMale], [Photo], [BirthDate], [StartDate], [ContractTypeId]) VALUES (N'bf4ca84b-f41c-492b-ba89-dffd8d8e8b0b', N'G00160', N'truc.phan@groovetechnology.com', N'Truc Phan', N'Truc', N'Phan', 0, NULL, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), CAST(N'2019-06-19T00:00:00.0000000' AS DateTime2), 1)
ALTER TABLE [dbo].[AppraisalParticipants] ADD  DEFAULT (CONVERT([bit],(0))) FOR [IsInvited]
GO
ALTER TABLE [dbo].[Goals] ADD  DEFAULT ((0)) FOR [PerformanceReviewCycleId]
GO
ALTER TABLE [dbo].[Objectives] ADD  DEFAULT ((0)) FOR [PerformanceReviewCycleId]
GO
ALTER TABLE [dbo].[ReviewTimelines] ADD  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[AppraisalParticipants]  WITH CHECK ADD  CONSTRAINT [FK_AppraisalParticipants_Appraisals_AppraisalId] FOREIGN KEY([AppraisalId])
REFERENCES [dbo].[Appraisals] ([Id])
GO
ALTER TABLE [dbo].[AppraisalParticipants] CHECK CONSTRAINT [FK_AppraisalParticipants_Appraisals_AppraisalId]
GO
ALTER TABLE [dbo].[AppraisalParticipants]  WITH CHECK ADD  CONSTRAINT [FK_AppraisalParticipants_Roles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[AppraisalParticipants] CHECK CONSTRAINT [FK_AppraisalParticipants_Roles_RoleId]
GO
ALTER TABLE [dbo].[AppraisalParticipants]  WITH CHECK ADD  CONSTRAINT [FK_AppraisalParticipants_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[AppraisalParticipants] CHECK CONSTRAINT [FK_AppraisalParticipants_Users_UserId]
GO
ALTER TABLE [dbo].[Appraisals]  WITH CHECK ADD  CONSTRAINT [FK_Appraisals_PerformanceReviewCycles_PerformanceReviewCycleId] FOREIGN KEY([PerformanceReviewCycleId])
REFERENCES [dbo].[PerformanceReviewCycles] ([Id])
GO
ALTER TABLE [dbo].[Appraisals] CHECK CONSTRAINT [FK_Appraisals_PerformanceReviewCycles_PerformanceReviewCycleId]
GO
ALTER TABLE [dbo].[Criterias]  WITH CHECK ADD  CONSTRAINT [FK_Criterias_CriteriaGroups_CriteriaGroupId] FOREIGN KEY([CriteriaGroupId])
REFERENCES [dbo].[CriteriaGroups] ([Id])
GO
ALTER TABLE [dbo].[Criterias] CHECK CONSTRAINT [FK_Criterias_CriteriaGroups_CriteriaGroupId]
GO
ALTER TABLE [dbo].[CriteriaWeights]  WITH CHECK ADD  CONSTRAINT [FK_CriteriaWeights_Criterias_CriteriaId] FOREIGN KEY([CriteriaId])
REFERENCES [dbo].[Criterias] ([Id])
GO
ALTER TABLE [dbo].[CriteriaWeights] CHECK CONSTRAINT [FK_CriteriaWeights_Criterias_CriteriaId]
GO
ALTER TABLE [dbo].[CriteriaWeights]  WITH CHECK ADD  CONSTRAINT [FK_CriteriaWeights_LevelCriteriaGroups_LevelCriteriaGroupId] FOREIGN KEY([LevelCriteriaGroupId])
REFERENCES [dbo].[LevelCriteriaGroups] ([Id])
GO
ALTER TABLE [dbo].[CriteriaWeights] CHECK CONSTRAINT [FK_CriteriaWeights_LevelCriteriaGroups_LevelCriteriaGroupId]
GO
ALTER TABLE [dbo].[EmployeeProjects]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeProjects_Projects_ProjectId] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Projects] ([Id])
GO
ALTER TABLE [dbo].[EmployeeProjects] CHECK CONSTRAINT [FK_EmployeeProjects_Projects_ProjectId]
GO
ALTER TABLE [dbo].[EmployeeProjects]  WITH CHECK ADD  CONSTRAINT [FK_EmployeeProjects_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[EmployeeProjects] CHECK CONSTRAINT [FK_EmployeeProjects_Users_UserId]
GO
ALTER TABLE [dbo].[Goals]  WITH CHECK ADD  CONSTRAINT [FK_Goals_PerformanceReviewCycles_PerformanceReviewCycleId] FOREIGN KEY([PerformanceReviewCycleId])
REFERENCES [dbo].[PerformanceReviewCycles] ([Id])
GO
ALTER TABLE [dbo].[Goals] CHECK CONSTRAINT [FK_Goals_PerformanceReviewCycles_PerformanceReviewCycleId]
GO
ALTER TABLE [dbo].[Goals]  WITH CHECK ADD  CONSTRAINT [FK_Goals_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Goals] CHECK CONSTRAINT [FK_Goals_Users_UserId]
GO
ALTER TABLE [dbo].[LevelCriteriaGroups]  WITH CHECK ADD  CONSTRAINT [FK_LevelCriteriaGroups_CriteriaGroups_CriteriaGroupId] FOREIGN KEY([CriteriaGroupId])
REFERENCES [dbo].[CriteriaGroups] ([Id])
GO
ALTER TABLE [dbo].[LevelCriteriaGroups] CHECK CONSTRAINT [FK_LevelCriteriaGroups_CriteriaGroups_CriteriaGroupId]
GO
ALTER TABLE [dbo].[LevelCriteriaGroups]  WITH CHECK ADD  CONSTRAINT [FK_LevelCriteriaGroups_Levels_LevelId] FOREIGN KEY([LevelId])
REFERENCES [dbo].[Levels] ([Id])
GO
ALTER TABLE [dbo].[LevelCriteriaGroups] CHECK CONSTRAINT [FK_LevelCriteriaGroups_Levels_LevelId]
GO
ALTER TABLE [dbo].[Objectives]  WITH CHECK ADD  CONSTRAINT [FK_Objectives_PerformanceReviewCycles_PerformanceReviewCycleId] FOREIGN KEY([PerformanceReviewCycleId])
REFERENCES [dbo].[PerformanceReviewCycles] ([Id])
GO
ALTER TABLE [dbo].[Objectives] CHECK CONSTRAINT [FK_Objectives_PerformanceReviewCycles_PerformanceReviewCycleId]
GO
ALTER TABLE [dbo].[Objectives]  WITH CHECK ADD  CONSTRAINT [FK_Objectives_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[Objectives] CHECK CONSTRAINT [FK_Objectives_Users_UserId]
GO
ALTER TABLE [dbo].[PerformanceReviewCycles]  WITH CHECK ADD  CONSTRAINT [FK_PerformanceReviewCycles_AppraisalModes_AppraisalModeId] FOREIGN KEY([AppraisalModeId])
REFERENCES [dbo].[AppraisalModes] ([Id])
GO
ALTER TABLE [dbo].[PerformanceReviewCycles] CHECK CONSTRAINT [FK_PerformanceReviewCycles_AppraisalModes_AppraisalModeId]
GO
ALTER TABLE [dbo].[PerformanceReviewCycles]  WITH CHECK ADD  CONSTRAINT [FK_PerformanceReviewCycles_Cycles_CycleId] FOREIGN KEY([CycleId])
REFERENCES [dbo].[Cycles] ([Id])
GO
ALTER TABLE [dbo].[PerformanceReviewCycles] CHECK CONSTRAINT [FK_PerformanceReviewCycles_Cycles_CycleId]
GO
ALTER TABLE [dbo].[Projects]  WITH CHECK ADD  CONSTRAINT [FK_Projects_Teams_TeamId] FOREIGN KEY([TeamId])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[Projects] CHECK CONSTRAINT [FK_Projects_Teams_TeamId]
GO
ALTER TABLE [dbo].[Ratings]  WITH CHECK ADD  CONSTRAINT [FK_Ratings_AppraisalParticipants_AppraisalParticipantId] FOREIGN KEY([AppraisalParticipantId])
REFERENCES [dbo].[AppraisalParticipants] ([Id])
GO
ALTER TABLE [dbo].[Ratings] CHECK CONSTRAINT [FK_Ratings_AppraisalParticipants_AppraisalParticipantId]
GO
ALTER TABLE [dbo].[Ratings]  WITH CHECK ADD  CONSTRAINT [FK_Ratings_CriteriaWeights_CriteriaWeightId] FOREIGN KEY([CriteriaWeightId])
REFERENCES [dbo].[CriteriaWeights] ([Id])
GO
ALTER TABLE [dbo].[Ratings] CHECK CONSTRAINT [FK_Ratings_CriteriaWeights_CriteriaWeightId]
GO
ALTER TABLE [dbo].[ReviewTimelines]  WITH CHECK ADD  CONSTRAINT [FK_ReviewTimelines_Appraisals_AppraisalId] FOREIGN KEY([AppraisalId])
REFERENCES [dbo].[Appraisals] ([Id])
GO
ALTER TABLE [dbo].[ReviewTimelines] CHECK CONSTRAINT [FK_ReviewTimelines_Appraisals_AppraisalId]
GO
ALTER TABLE [dbo].[ReviewTimelines]  WITH CHECK ADD  CONSTRAINT [FK_ReviewTimelines_Stages_StageId] FOREIGN KEY([StageId])
REFERENCES [dbo].[Stages] ([Id])
GO
ALTER TABLE [dbo].[ReviewTimelines] CHECK CONSTRAINT [FK_ReviewTimelines_Stages_StageId]
GO
ALTER TABLE [dbo].[Teams]  WITH CHECK ADD  CONSTRAINT [FK_Teams_BusinessUnits_BusinessUnitId] FOREIGN KEY([BusinessUnitId])
REFERENCES [dbo].[BusinessUnits] ([Id])
GO
ALTER TABLE [dbo].[Teams] CHECK CONSTRAINT [FK_Teams_BusinessUnits_BusinessUnitId]
GO
ALTER TABLE [dbo].[UserLevels]  WITH CHECK ADD  CONSTRAINT [FK_UserLevels_Levels_LevelId] FOREIGN KEY([LevelId])
REFERENCES [dbo].[Levels] ([Id])
GO
ALTER TABLE [dbo].[UserLevels] CHECK CONSTRAINT [FK_UserLevels_Levels_LevelId]
GO
ALTER TABLE [dbo].[UserLevels]  WITH CHECK ADD  CONSTRAINT [FK_UserLevels_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UserLevels] CHECK CONSTRAINT [FK_UserLevels_Users_UserId]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Roles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Roles_RoleId]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Users_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Users_UserId]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_ContractTypes_ContractTypeId] FOREIGN KEY([ContractTypeId])
REFERENCES [dbo].[ContractTypes] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_ContractTypes_ContractTypeId]
GO
